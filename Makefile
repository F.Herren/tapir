# To install tapir, the requirements are python3 in a version >= 3.6

# Use bash as it is standard on most machines
SHELL := /bin/bash

# Check for python3
# To use a different python3 executable, just replace your executable in the following line
PYTHON_CMD_SYS := $(shell which python3)
# Virtual python3 environment path
VENV_DIR := python3
# Virtual evironment executable
PYTHON_CMD_VENV := $(VENV_DIR)/bin/python3


# Create virtual environment and install the required packages
.ONESHELL:
install:
	@echo "Prepare dependecies for tapir..."
ifeq (,$(PYTHON_CMD_SYS))
	@echo "Please install Python in version 3.6 or above!"
else
	# Check for correct version
	if (($$($(PYTHON_CMD_SYS) -V | grep -Po '(?<=3\.)[^\.]+') < 4)); then\
		echo "Please install Python in version 3.6 or above!";\
		exit 1;\
	fi

	@echo "Check for pip module..."
	$(PYTHON_CMD_SYS) -m ensurepip

	@echo "Build virtual environment..."
	$(PYTHON_CMD_SYS) -m venv $(VENV_DIR)

	@echo "Install requirements..."
	${PYTHON_CMD_VENV} -m pip install -U pip
	${PYTHON_CMD_VENV} -m pip install -r requirements.txt
	touch ${VENV_DIR}/bin/activate

	@echo "Build Cython binaries..."
	cd modules/cython/ && ../../${PYTHON_CMD_VENV} setup.py build_ext --inplace
endif


# Clean installation
clean: FORCE
	@echo "Clean virtual python environment..."
	rm -rf $(VENV_DIR)
	rm -rf $$(find -name "__pycache__")
	@echo "Clean Cython backend..."
	rm -rf modules/cython/build/ modules/cython/*.cpp modules/cython/*.so
FORCE:


# Run all tests
test: unittests smoke_tests legacy_consistency_checks pylinttests


# Run unittests
unittests: FORCE
ifeq (,$(wildcard $(PYTHON_CMD_VENV)))
	@echo "Run 'make install' first!"
else
	${PYTHON_CMD_VENV} -m unittest discover -v
endif
FORCE:


# Run smoketests
smoke_tests: FORCE
ifeq (,$(wildcard $(PYTHON_CMD_VENV)))
	@echo "Run 'make install' first!"
else
	test/run_smoke_tests.sh
endif
FORCE:


# Run legacy_consistency_checks
legacy_consistency_checks: FORCE
ifeq (,$(wildcard $(PYTHON_CMD_VENV)))
	@echo "Run 'make install' first!"
else
	test/run_legacy_consistency_checks.sh
endif
FORCE:


# Run legacy_consistency_checks
pylinttests: FORCE
ifeq (,$(wildcard $(PYTHON_CMD_VENV)))
	@echo "Run 'make install' first!"
else
	test/run_pylinttests.sh
endif
FORCE:


# Create documentation using doxygen and pdflatex
doc: FORCE
ifeq (,$(shell which doxygen))
	@echo "Command 'doxygen' not found, consider installing it!"
else
	doxygen tapir.doxy
ifeq (,$(shell which pdflatex))
	@echo ""
	@echo "Command 'pdflatex' not found! pdf documentation could not be build."
	@echo "Technical documentation (HTML format) can be found in doc/development/html/"
else
	cd doc/development/latex && latex refman.tex
	@echo ""
	@echo "Technical documentation (HTML and LaTeX format) can be found in doc/development/"
endif
endif
FORCE:
