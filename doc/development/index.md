# Developer manual
To generate the technical documents with relevant internal information, run

```bash
make doc
```

in the project directory. The manual is then generated as `doc/development/latex/refman.pdf`.

Alternatively, a html version is available at `doc/development/html/index.html` (open via web browser).