# dia files

dia files define the analytic structure of individual diagrams, expressed through the [defined Feynman rules](rules.md).
The analytic expressions are split into different `FORM` folds according to the definition in vrtx and prop files.
Additionally, it is recommended to include the file `declare.inc` from the `tapir` base folder before including `dia` files since all ancillary symbols are declared there.

A typical dia file for a single diagram looks like this:

```
*--#[ d[#loops]l[diagram number] :

	[qlist prefactor]*[fermion-loop factors]

    ...

*--#] d[#loops]l[diagram number] :

*--#[ fqcd[#loops]l[diagram number] :

    ...

*--#] fqcd[#loops]l[diagram number] :

*--#[ fqed[#loops]l[diagram number] :

    ...

*--#] fqed[#loops]l[diagram number] :

*--#[ few[#loops]l[diagram number] :

    ...

*--#] few[#loops]l[diagram number] :
```

If there are more diagrams, the same fold structure follows in the file.

The main fold, named as "d[#loops]l[diagram number]" (e.g. d3l5), wraps the contents of the Lorentz-entries of the prop and vrtx files.
Also, the combinatorial prefactor found by `qgraf` and the fermion-loop factors (from the `closed_fermion_loop` option) are here situated.

## Example

```
*--#[ d1l1 :

	(+1)*1*nfu
	*FT1(nu6)
	*FT2(nu8)
	*FT3(nu7)
	*FT3(p4)
	*FT3(nu5)
	*FT3(p3)
	*d_(nu6,nu5)
	*d_(nu8,nu7)
	;

	#define TOPOLOGY "arb"
	#define INT1 "arb"

*--#] d1l1 :

*--#[ fqcd1l1 :

	1
	*d_(i2,i1)
	*d_(i3,i4)
	*d_(j9,j12)
	*d_(j12,j11)
	*d_(j11,j10)
	*d_(j10,j9)
	;

*--#] fqcd1l1 :

*--#[ fqed1l1 :
	1
*--#] fqed1l1 :

*--#[ few1l1 :
	1
*--#] few1l1 :
```

The "1"'s in the QED and EW folds are automatically inserted in case the Feynman rules for this fold are all empty.
