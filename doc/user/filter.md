# Filter
In addition to the standard diagram filtering of `qgraf` we also provide a few filters.
They can be specified in the [config file](config.md) via

```
* tapir.filter [filter name] : [true/t/1/false/f/0] {: [filter arguments]...}
```

Specifying a filter with `true` (or `t` or `1`) gives `tapir` the order to keep only the diagrams that match the filter condition.
Setting the filter with `false` (or `f` or `0`) will end up in the removal of the diagrams that match the filter condition.

Filters are applied one after another in the here specified order.

Currently implemented are:
- `self_energy_bridge_mixing`: Matches one particle reducible diagrams which contains bridges (no loop associated lines) that define a mixing of the particle. I.e. these are self energy diagrams which do not contribute to standard propagator corrections.
- `self_energy_bridge`: Matches one particle reducible diagrams which have bridges (no loop associated lines) which correspond to self energy corrections without mixing, i.e. similar to `self_energy_bridge_mixing` but in adjacent bridges the same particle must propagate.
- `external_self_energy_bridge_mixing`: Same as `self_energy_bridge_mixing` but only mixing directly at external lines are regarded. I.e. mixing from internal to internal lines are ignored.
- `external_self_energy_bridge`: Same as `self_energy_bridge` but only self-energies directly at external lines are regarded. I.e. bridges from internal to internal lines are ignored.
- `scaleless`: Matches diagrams that only contain scaleless integrals and thus vanish. If sigma particles are specified, the corresponding lines are contracted as they don't carry momentum.
- `cuts`: Matches if the diagram has cuts through a specific amount of particles. More about this [here](#cuts).

Cascading multiple filters (by specifying multiple ones) is a powerful tool to vastly reduce the number of regarded diagrams and to split the problem in smaller sub-classes.

## `cuts`
This filter is used to extract only the subclass of diagrams that have relevant cuts according to Cutkosky rules.
To specify the allowed cuts, the `cuts` option allows the following filter arguments:
```
* tapir.filter cuts : [true/t/1/false/f/0] : {[external "source" momenta]} : {[particle types]} : {[intervals of how many cuts]}
```

The latter three are optional and can be empty to match generically. The separating `:` must nevertheless be given (unless they're trailing).

- The first argument `[external "source" momenta]` is a comma separated list of external momenta which are treated as "sources" or incoming. Accordingly, the remaining external momenta are then treated as "sinks" or outgoing. The momenta here have their real names, i.e. only `q1`, `q2`,... are allowed. If this argument is not provided the incoming momenta, as defined in the `qgraf` file, are taken as default.
- The second argument `[particle types]` is a comma separated list of particles on which the cutting restriction is applied. By specifying more than one particle, their cut contributions are added up and compared against the intervals. Leaving this argument empty has the same effect as if one had specified every particle available.
- The third argument `[intervals of how many cuts]` is a comma separated list of how many cuts are allowed through the propagators of the given particle type. The intervals are always given as upper and lower bounds. Multiple intervals are allowed, too. Just add another two numbers to the list. Overlapping intervals are combined implicitly. Providing nothing here matches _any_ number of cuts bigger than 0.

For example, the following filter specify cuts for an inclusive `h1` or `h2` productions with at least two gluons in the final state. Additionally, cuts through `ft` are not permitted.
```
* tapir.filter cuts : true : q1,q2 : h1,h2 : 1,1
* tapir.filter cuts : false : q1,q2 : g : 0,1
* tapir.filter cuts : true : q1,q2 : ft : 0,0
```

## `vertex`
This filter can be used to extract or reject diagrams that contain a certain number of vertices with a given particle content. The syntax is the same as for the `cuts` filter, except that the first argument (source momenta) should be empty (it will be ignored in any case). `qgraf` allows to filter vertices already at the diagram-generation stage by assigning certain factors to vertices; however, in combination with other filters it may be useful to apply these filters at the `tapir` stage.

For example, the following filter can be used to extract diagrams that have exactly 0 or 2 `[ft,fT,h]` vertices and no `[fe,fE,h` vertex.
```
* tapir.filter vertex : true :  : ft,fT,h : 0,0,2,2
* tapir.filter vertex : true :  : fe,fE,h : 0,0
```
Another example: we want to keep only diagrams that have exactly 3 vertices taken from the set `{[ft,fT,h],[ft,fT,G]}`, and no `[fe,fE,h]` vertex:
```
* tapir.filter vertex : true :  : ft,fT,h;ft,fT,G : 3,3
* tapir.filter vertex : true :  : fe,fE,h : 0,0
```
The `;` is used to distinguish different vertices, while `,` refers to the particles within a vertex.

## Implementing custom filters
Contributors are heartily welcome to implement their own filers.
To do so, python knowledge is recommended.

The filters are applied in the `filter()` method of the class `DiagramFilter` in `modules/diagramFilter.py`.
To add your own filter, write your filtering function which takes a list of `Diagram` objects (defined in `modules/diagram.py`) as arguments and returns a list of indices of the diagrams of the input list that match your condition.
Relevant fields of the `Diagram` class for filtering are `self.topologicalVertexList` and `self.topologicalLineList`, which are lists of `Vertex` and `Line` class instances defined in `modules/diagram.py`.

After writing your function, implement it like the others in `filter()` in the order of your choice.
