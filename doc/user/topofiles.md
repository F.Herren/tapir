# Analytic topology files
"topology" files are one of the outputs of `tapir` that makes the computation using `exp` easier.

The standard way of using `exp` is to give a [dia](dia.md) and an [edia](diagramAndTopologyFiles.md) file as input.
Furthermore, a list of "known" topologies must be given, the [topsel file](diagramAndTopologyFiles.md#topsel-files).
The edia entries are then topologically mapped by `exp` onto the topsel entries.
Further computation that makes use of this mapping involve the substitution of bare momenta into a scalar integral family function.
This is done in the topology files.

Because each topsel entry must have a topology file representation, `tapir` allows reading topsel files as input to generate those.

Before one can make use of these files, a few steps must be taken for each individual diagram which is not covered by `tapir`:

- Bring the Lorentz-fold of the dia file to a standard form:
    * All line momenta must be renamed to match the naming of the topology which was defined by the topsel file
    * All momenta must be freely accessible in each term (i.e. not in function arguments etc.)
    * massive propagators (i.e. `1/(p1^2-M2^2)`) must be expressed by `s[momentum index]m[mass index]` which is equal to `1/(M[mass index]^2 - p[momentum index]^2)` (the sign depends on the option `tapir.euclidean`, see [config](config.md#Topology-options)). E.g. `1/(p1^2-M2^2)` is replaced by `-s1m2` if the option `tapir.euclidean` was not set.
- Make the expression scalar

The last point implies that some sort of tensor reduction must be applied before a scalar topology function can be used.
Additionally, it is recommended to include the file `declare.inc` from the `tapir` base folder before using analytic topology files since all ancillary symbols are declared there.

Topology files are structured like this:

1. Header:
    The header provides information about the scalar integral function, which line momenta are chosen as loop momenta and whether the `tapir.euclidean` was declared to define the massive propagators differently.
2. Numerator momentum substitutions:
    The line momenta of the topology are substituted by a superposition of loop momenta and external momenta. Some the line momenta are picked to act as loop momenta. Also, external momenta are replaced according to the [option](config.md#Topology-options)) `tapir.external_momentum`.
3. Numerator momentum product substitutions:
    Here, we try to substitute as many momentum products (between loop and external, as well as between loop momenta) as possible, because the ones we cannot be replaced further must be part of the scalar topology function. Here, we also substitute squared massive line momenta to `s[momentum index]m[mass index]` and identify squared momenta with other squared momenta (even in the denominators) if they carry the same momentum and the same mass. E.g. `id p1.p1^i? = p2.p2^i`.
4. Direct replacements of internal momenta:
    If the options `tapir.topo_eikonal_internal` and/or `tapir.topo_eikonal_external` are used, we replace internal momenta in denominators directly by others if possible. For example, if the relation between two internal momenta differ maximally by a sign, i.e. `p2 = -p1`, in this case `p2` can be completely replaced by `-p1` even in eikonal propagators.
5. Replacement of kinematic relations:
    If the option `tapir.kinematic_relation` is used, we apply this replacement at this point to an arbitrary power. E.g. for `q1.q1 : M1^2` we insert here `id q1.q1^n1? = (M1^2)^n1;`.
6. Combination to scalar function:
    The scalar function is defined as powers of the following terms:
    * Remaining momentum products in the numerators (here defined in denominator language). E.g. `-1/(p1.q)` (watch sign). If the `tapir.topo_complete_momentum_products` is used the momentum products are replaced by ancillary propagators as new momentum squares. E.g. `p1.q` is replaced by `1/2*(P1.P1 - p1^2 - q^2)` where `P1=p1+q` is the new momentum. In case one has a problem including eikonal propagators (like `-1/p1.q` being a real denominator function), one can use the options `tapir.topo_eikonal_internal` and/or `tapir.topo_eikonal_external`.
    * Squared momenta of massless lines (if it wasn't identified by another one in point 3.). E.g. `-1/p1.p1` (watch sign)
    * Momentum-mass combinations of massive lines. E.g. `s2m1`
    * Squared momenta of massive lines if the option `tapir.extend_analytical_topos` is set. This point is added for the case of non standard propagators (e.g. massive gluons) where the propagator looks like `1/(k^2-m^2)/k^2`. E.g. `-1/p2.p2`
    The appearance of both `1/(k^2-m^2)` and `1/k^2` is resolved in the next step.
7. Partial fraction decomposition:
    If the option for partial fraction decomposition is set (see [CLI options](index.md#Command-line-options)), the previously found scalar function is split into smaller functions with minimal sets of denominator terms. It is also possible to minimize the remaining analytic topologies that appear in all generated topology files. In that case, some topology files may insert reduced topologies from other topology files, too.

We insert also some sanity checks if all internal momenta (i.e. combinations including loop momenta) are inside the scalar topology function. The program will terminate if this is not the case.
The same applies for propagators after inserting the new partial fraction minimized topology functions.

Note that the minimization of partial fraction decomposed topologies is rather slow. Thus it is only recommended from an already minimized topology set.


## Example topology file
For the topsel entry of a 1-loop 2-point function with two massive lines

```
{d1l6;2;1;1;1;;(q1:1,2)(p1:2,1)(p2:1,2);11}
```

we get the following topology file when using partial fraction decomposition and the option `tapir.extend_analytical_topos`:

```
********************************************************************************
* Topology file d1l6 generated by TAPIR
* Using Minkowski momenta
*
*       d1l6:   {M1^2 - p2^2, -p2^2, M1^2 - p2^2 + 2*p2*q1 - q1^2, -p2^2 + 2*p2*q1 - q1^2}
*       loop momenta:   p2
********************************************************************************

* Reducible numerator momentum replacements
id p1 = p2 - q1;

.sort

* Numerator momentum product replacements
id p2.q1 = -p1.p1/2 + p2.p2/2 + q1.q1/2;

.sort

* Define massive propagators
id p1.p1 = -1/s1m1 + M1^2;
id p2.p2 = -1/s2m1 + M1^2;

.sort

* Combine to scalar topology function
id s2m1^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? = 
   (-1)^n1 * (-1)^n3 * 
   d1l6(n0,n1,n2,n3);

.sort

* Check for unmatched topology factors
if( occurs(p1, p2, s1m1, s2m1) );
   exit ">> Error: Unmatched topology factors found!";
endif;


* Perform partial fraction decomposition
id d1l6(n0?, n1?, n2?, n3?) = 1/D0^n0 * 1/D1^n1 * 1/D2^n2 * 1/D3^n3;

repeat id D3 = D2 - M1^2;
repeat id D2/D3 = M1^2/D3 + 1;
repeat id D1 = D0 - M1^2;
repeat id D0/D1 = M1^2/D1 + 1;
repeat id 1/D2/D3 = (-1/D2 + 1/D3)/M1^2;
repeat id 1/D0/D1 = (-1/D0 + 1/D1)/M1^2;

.sort

if( occurs(D0, D1, D2, D3) == 0 );
*  Integral has no scale
   multiply FLAGSCALELESS;
elseif( occurs(D0, D2) == 0 );
    id D1^n0? * D3^n1? = d1l6w1w3(-n0, -n1);
elseif( occurs(D1, D2) == 0 );
    id D0^n0? * D3^n1? = d1l6w0w3(-n0, -n1);
elseif( occurs(D0, D3) == 0 );
    id D1^n0? * D2^n1? = d1l6w0w3(-n1, -n0);
elseif( occurs(D1, D3) == 0 );
    id D0^n0? * D2^n1? = d1l6w0w2(-n0, -n1);
endif;

.sort

* Check if all topology factors are substituted back
if( occurs(D0, D1, D2, D3, D4, D5) );
   exit ">> Error: Topology entries after partial fraction decomposition found!";
endif;
```

## Topology list files

We also get two topology list files (generated altogether with the topology file). The `Mathematica` readable one `topologyList.m` looks like:

```
{{"d1l6w1w3", {-k1^2, -k1^2 - 2*k1*q1 - q1^2}, {k1}},
{"d1l6w0w3", {M1^2 - k1^2, -k1^2 + 2*k1*q1 - q1^2}, {k1}},
{"d1l6w0w2", {M1^2 - k1^2, M1^2 - k1^2 + 2*k1*q1 - q1^2}, {k1}}}
```

whereas the `YAML` list file `topologyList.yaml` is given by:
```
integralfamilies:
  - name: "d1l6w1w3"
    loop_momenta: [k1]
    propagators:
      - ["-k1^2", 0]
      - ["-(k1 - q1)^2", 0]
  - name: "d1l6w0w3"
    loop_momenta: [k1]
    propagators:
      - ["M1^2 - k1^2", 0]
      - ["-(k1 - q1)^2", 0]
  - name: "d1l6w0w2"
    loop_momenta: [k1]
    propagators:
      - ["M1^2 - k1^2", 0]
      - ["M1^2 - (k1 - q1)^2", 0]
```
