# B_s decays
This example shows the workflow with `tapir` for the process $`\bar{b} s \rightarrow b \bar{s}`$ with NNLO QCD corrections.
Here, we do not repeat the basics from the [3-loop gluon propagator example](../gg/README.md).

## Modifying the topologies
Let us assume, we are only interested in a special kinematic case for our problem, e.g. All three momenta are zero.
In addition, let's assume only the b quark is non massless.
In that case the momenta of the s quarks are negligible.
Thus, we can simplify the topologies by removing the external s quark lines.
To account for special kinematics in the topology files, we use the option `tapir.external_momentum` in the [config file](Bmix.conf). By setting external momenta to zero with this option, `tapir` will cut the lines away from the topology.

To see the result, let us plot the topologies:

```bash
../../tapir --conf Bmix.conf --qlist qlist.2 --topologyart topology-drawings.tex && lualatex topology-drawings.tex
```

## Topology files
Also here, let us create a minimal set of analytic topologies with all related files. In addition let us minimize the partial fraction decomposed analytic topologies with the `-pm, --partfrac-minimize` option:

```bash
$ ../../tapir -k -c Bmix.conf -q qlist.2 -m -t Bmix.topsel -f topologies -pm
```
