Symbol ufoee;
Symbol ufocw;
Symbol ufoG;
Symbol ufoI1a11;
Symbol ufoI1a12;
Symbol ufoI1a13;
Symbol ufoI1a21;
Symbol ufoI1a22;
Symbol ufoI1a23;
Symbol ufoI1a31;
Symbol ufoI1a32;
Symbol ufoI1a33;
Symbol ufoI2a11;
Symbol ufoI2a12;
Symbol ufoI2a13;
Symbol ufoI2a21;
Symbol ufoI2a22;
Symbol ufoI2a23;
Symbol ufoI2a31;
Symbol ufoI2a32;
Symbol ufoI2a33;
Symbol ufoI3a11;
Symbol ufoI3a12;
Symbol ufoI3a13;
Symbol ufoI3a21;
Symbol ufoI3a22;
Symbol ufoI3a23;
Symbol ufoI3a31;
Symbol ufoI3a32;
Symbol ufoI3a33;
Symbol ufoI4a11;
Symbol ufoI4a12;
Symbol ufoI4a13;
Symbol ufoI4a21;
Symbol ufoI4a22;
Symbol ufoI4a23;
Symbol ufoI4a31;
Symbol ufoI4a32;
Symbol ufoI4a33;
Symbol ufolam;
Symbol ufosw;
Symbol ufoCKM1x1;
Symbol ufoCKM1x2;
Symbol ufoCKM1x3;
Symbol ufoCKM2x1;
Symbol ufoCKM2x2;
Symbol ufoCKM2x3;
Symbol ufoCKM3x1;
Symbol ufoCKM3x2;
Symbol ufoCKM3x3;
Symbol ufovev;
Symbol ufoyb;
Symbol ufoyc;
Symbol ufoydo;
Symbol ufoye;
Symbol ufoym;
Symbol ufoys;
Symbol ufoyt;
Symbol ufoytau;
Symbol ufoyup;
Symbol ufoaEWM1;
Symbol ufoVud;
Symbol ufoVus;
Symbol ufoVub;
Symbol ufoVcd;
Symbol ufoVcs;
Symbol ufoVcb;
Symbol ufoVtd;
Symbol ufoVts;
Symbol ufoVtb;
Symbol ufoaEW;
Symbol ufopi;
Symbol ufoMZ;
Symbol ufoGf;
Symbol ufoMW;
Symbol ufosw2;
Symbol ufoMH;
Symbol ufoymb;
Symbol ufoymc;
Symbol ufoymdo;
Symbol ufoyme;
Symbol ufoymm;
Symbol ufoyms;
Symbol ufoymt;
Symbol ufoymtau;
Symbol ufoymup;


CFunction nonfactag;
CFunction ufocomplex;
CFunction ufosqrt;
CFunction ufocomplexconjugate;


id ufoGC1 = -(ufoee*ufocomplex(0,1))/3;
id ufoGC2 = (2*ufoee*ufocomplex(0,1))/3;
id ufoGC3 = -(ufoee*ufocomplex(0,1));
id ufoGC4 = ufoee*ufocomplex(0,1);
id ufoGC5 = ufoee^2*ufocomplex(0,1);
id ufoGC6 = 2*ufoee^2*ufocomplex(0,1);
id ufoGC7 = -ufoee^2/(2*ufocw);
id ufoGC8 = -(ufoee^2*ufocomplex(0,1))/(2*ufocw);
id ufoGC9 = ufoee^2/(2*ufocw);
id ufoGC10 = -ufoG;
id ufoGC11 = ufocomplex(0,1)*ufoG;
id ufoGC12 = ufocomplex(0,1)*ufoG^2;
id ufoGC13 = -(ufocomplex(0,1)*ufoI1a11);
id ufoGC14 = -(ufocomplex(0,1)*ufoI1a12);
id ufoGC15 = -(ufocomplex(0,1)*ufoI1a13);
id ufoGC16 = -(ufocomplex(0,1)*ufoI1a21);
id ufoGC17 = -(ufocomplex(0,1)*ufoI1a22);
id ufoGC18 = -(ufocomplex(0,1)*ufoI1a23);
id ufoGC19 = -(ufocomplex(0,1)*ufoI1a31);
id ufoGC20 = -(ufocomplex(0,1)*ufoI1a32);
id ufoGC21 = -(ufocomplex(0,1)*ufoI1a33);
id ufoGC22 = ufocomplex(0,1)*ufoI2a11;
id ufoGC23 = ufocomplex(0,1)*ufoI2a12;
id ufoGC24 = ufocomplex(0,1)*ufoI2a13;
id ufoGC25 = ufocomplex(0,1)*ufoI2a21;
id ufoGC26 = ufocomplex(0,1)*ufoI2a22;
id ufoGC27 = ufocomplex(0,1)*ufoI2a23;
id ufoGC28 = ufocomplex(0,1)*ufoI2a31;
id ufoGC29 = ufocomplex(0,1)*ufoI2a32;
id ufoGC30 = ufocomplex(0,1)*ufoI2a33;
id ufoGC31 = ufocomplex(0,1)*ufoI3a11;
id ufoGC32 = ufocomplex(0,1)*ufoI3a12;
id ufoGC33 = ufocomplex(0,1)*ufoI3a13;
id ufoGC34 = ufocomplex(0,1)*ufoI3a21;
id ufoGC35 = ufocomplex(0,1)*ufoI3a22;
id ufoGC36 = ufocomplex(0,1)*ufoI3a23;
id ufoGC37 = ufocomplex(0,1)*ufoI3a31;
id ufoGC38 = ufocomplex(0,1)*ufoI3a32;
id ufoGC39 = ufocomplex(0,1)*ufoI3a33;
id ufoGC40 = -(ufocomplex(0,1)*ufoI4a11);
id ufoGC41 = -(ufocomplex(0,1)*ufoI4a12);
id ufoGC42 = -(ufocomplex(0,1)*ufoI4a13);
id ufoGC43 = -(ufocomplex(0,1)*ufoI4a21);
id ufoGC44 = -(ufocomplex(0,1)*ufoI4a22);
id ufoGC45 = -(ufocomplex(0,1)*ufoI4a23);
id ufoGC46 = -(ufocomplex(0,1)*ufoI4a31);
id ufoGC47 = -(ufocomplex(0,1)*ufoI4a32);
id ufoGC48 = -(ufocomplex(0,1)*ufoI4a33);
id ufoGC49 = -2*ufocomplex(0,1)*ufolam;
id ufoGC50 = -4*ufocomplex(0,1)*ufolam;
id ufoGC51 = -6*ufocomplex(0,1)*ufolam;
id ufoGC52 = (ufoee^2*ufocomplex(0,1))/(2*ufosw^2);
id ufoGC53 = -((ufoee^2*ufocomplex(0,1))/ufosw^2);
id ufoGC54 = (ufocw^2*ufoee^2*ufocomplex(0,1))/ufosw^2;
id ufoGC55 = -(ufoee*ufocomplex(0,1))/(2*ufosw);
id ufoGC56 = (ufoee*ufocomplex(0,1))/(2*ufosw);
id ufoGC57 = ufoee/(2*ufosw);
id ufoGC58 = (ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC59 = (ufoCKM1x1*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC60 = (ufoCKM1x2*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC61 = (ufoCKM1x3*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC62 = (ufoCKM2x1*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC63 = (ufoCKM2x2*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC64 = (ufoCKM2x3*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC65 = (ufoCKM3x1*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC66 = (ufoCKM3x2*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC67 = (ufoCKM3x3*ufoee*ufocomplex(0,1))/(ufosw*ufosqrt(2));
id ufoGC68 = -((ufocw*ufoee*ufocomplex(0,1))/ufosw);
id ufoGC69 = (ufocw*ufoee*ufocomplex(0,1))/ufosw;
id ufoGC70 = -ufoee^2/(2*ufosw);
id ufoGC71 = (ufoee^2*ufocomplex(0,1))/(2*ufosw);
id ufoGC72 = ufoee^2/(2*ufosw);
id ufoGC73 = (-2*ufocw*ufoee^2*ufocomplex(0,1))/ufosw;
id ufoGC74 = (ufoee*ufocomplex(0,1)*ufosw)/(3*ufocw);
id ufoGC75 = (-2*ufoee*ufocomplex(0,1)*ufosw)/(3*ufocw);
id ufoGC76 = (ufoee*ufocomplex(0,1)*ufosw)/ufocw;
id ufoGC77 = -(ufocw*ufoee)/(2*ufosw) - (ufoee*ufosw)/(2*ufocw);
id ufoGC78 = -(ufocw*ufoee*ufocomplex(0,1))/(2*ufosw) - (ufoee*ufocomplex(0,1)*ufosw)/(6*ufocw);
id ufoGC79 = (ufocw*ufoee*ufocomplex(0,1))/(2*ufosw) - (ufoee*ufocomplex(0,1)*ufosw)/(6*ufocw);
id ufoGC80 = -(ufocw*ufoee*ufocomplex(0,1))/(2*ufosw) + (ufoee*ufocomplex(0,1)*ufosw)/(2*ufocw);
id ufoGC81 = (ufocw*ufoee*ufocomplex(0,1))/(2*ufosw) + (ufoee*ufocomplex(0,1)*ufosw)/(2*ufocw);
id ufoGC82 = (ufocw*ufoee^2*ufocomplex(0,1))/ufosw - (ufoee^2*ufocomplex(0,1)*ufosw)/ufocw;
id ufoGC83 = -(ufoee^2*ufocomplex(0,1)) + (ufocw^2*ufoee^2*ufocomplex(0,1))/(2*ufosw^2) + (ufoee^2*ufocomplex(0,1)*ufosw^2)/(2*ufocw^2);
id ufoGC84 = ufoee^2*ufocomplex(0,1) + (ufocw^2*ufoee^2*ufocomplex(0,1))/(2*ufosw^2) + (ufoee^2*ufocomplex(0,1)*ufosw^2)/(2*ufocw^2);
id ufoGC85 = -(ufoee^2*ufocomplex(0,1)*ufovev)/(2*ufocw);
id ufoGC86 = -2*ufocomplex(0,1)*ufolam*ufovev;
id ufoGC87 = -6*ufocomplex(0,1)*ufolam*ufovev;
id ufoGC88 = -(ufoee^2*ufovev)/(4*ufosw^2);
id ufoGC89 = -(ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufosw^2);
id ufoGC90 = (ufoee^2*ufocomplex(0,1)*ufovev)/(2*ufosw^2);
id ufoGC91 = (ufoee^2*ufovev)/(4*ufosw^2);
id ufoGC92 = -(ufoee^2*ufocomplex(0,1)*ufovev)/(2*ufosw);
id ufoGC93 = (ufoee^2*ufocomplex(0,1)*ufovev)/(2*ufosw);
id ufoGC94 = (ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufocw) - (ufocw*ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufosw^2);
id ufoGC95 = (ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufocw) + (ufocw*ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufosw^2);
id ufoGC96 = -(ufoee^2*ufocomplex(0,1)*ufovev)/2 - (ufocw^2*ufoee^2*ufocomplex(0,1)*ufovev)/(4*ufosw^2) - (ufoee^2*ufocomplex(0,1)*ufosw^2*ufovev)/(4*ufocw^2);
id ufoGC97 = ufoee^2*ufocomplex(0,1)*ufovev + (ufocw^2*ufoee^2*ufocomplex(0,1)*ufovev)/(2*ufosw^2) + (ufoee^2*ufocomplex(0,1)*ufosw^2*ufovev)/(2*ufocw^2);
id ufoGC98 = -(ufoyb/ufosqrt(2));
id ufoGC99 = -((ufocomplex(0,1)*ufoyb)/ufosqrt(2));
id ufoGC100 = -((ufocomplex(0,1)*ufoyc)/ufosqrt(2));
id ufoGC101 = ufoyc/ufosqrt(2);
id ufoGC102 = -(ufoydo/ufosqrt(2));
id ufoGC103 = -((ufocomplex(0,1)*ufoydo)/ufosqrt(2));
id ufoGC104 = -(ufocomplex(0,1)*ufoye);
id ufoGC105 = -(ufoye/ufosqrt(2));
id ufoGC106 = -((ufocomplex(0,1)*ufoye)/ufosqrt(2));
id ufoGC107 = -(ufocomplex(0,1)*ufoym);
id ufoGC108 = -(ufoym/ufosqrt(2));
id ufoGC109 = -((ufocomplex(0,1)*ufoym)/ufosqrt(2));
id ufoGC110 = -(ufoys/ufosqrt(2));
id ufoGC111 = -((ufocomplex(0,1)*ufoys)/ufosqrt(2));
id ufoGC112 = -((ufocomplex(0,1)*ufoyt)/ufosqrt(2));
id ufoGC113 = ufoyt/ufosqrt(2);
id ufoGC114 = -(ufocomplex(0,1)*ufoytau);
id ufoGC115 = -(ufoytau/ufosqrt(2));
id ufoGC116 = -((ufocomplex(0,1)*ufoytau)/ufosqrt(2));
id ufoGC117 = -((ufocomplex(0,1)*ufoyup)/ufosqrt(2));
id ufoGC118 = ufoyup/ufosqrt(2);
id ufoGC119 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM1x1))/(ufosw*ufosqrt(2));
id ufoGC120 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM1x2))/(ufosw*ufosqrt(2));
id ufoGC121 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM1x3))/(ufosw*ufosqrt(2));
id ufoGC122 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM2x1))/(ufosw*ufosqrt(2));
id ufoGC123 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM2x2))/(ufosw*ufosqrt(2));
id ufoGC124 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM2x3))/(ufosw*ufosqrt(2));
id ufoGC125 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM3x1))/(ufosw*ufosqrt(2));
id ufoGC126 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM3x2))/(ufosw*ufosqrt(2));
id ufoGC127 = (ufoee*ufocomplex(0,1)*ufocomplexconjugate(ufoCKM3x3))/(ufosw*ufosqrt(2));
id ZERO = 0;
id ufoaEW = 1/ufoaEWM1;
id ufoG = 2*ufosqrt(ufoaS)*ufosqrt(ufopi);
id ufoCKM1x1 = ufoVud;
id ufoCKM1x2 = ufoVus;
id ufoCKM1x3 = ufoVub;
id ufoCKM2x1 = ufoVcd;
id ufoCKM2x2 = ufoVcs;
id ufoCKM2x3 = ufoVcb;
id ufoCKM3x1 = ufoVtd;
id ufoCKM3x2 = ufoVts;
id ufoCKM3x3 = ufoVtb;
id ufoMW = ufosqrt(ufoMZ^2/2 + ufosqrt(ufoMZ^4/4 - (ufoaEW*ufopi*ufoMZ^2)/(ufoGf*ufosqrt(2))));
id ufoee = 2*ufosqrt(ufoaEW)*ufosqrt(ufopi);
id ufosw2 = 1 - ufoMW^2/ufoMZ^2;
id ufocw = ufosqrt(1 - ufosw2);
id ufosw = ufosqrt(ufosw2);
id ufog1 = ufoee/ufocw;
id ufogw = ufoee/ufosw;
id ufovev = (2*ufoMW*ufosw)/ufoee;
id ufolam = ufoMH^2/(2*ufovev^2);
id ufoyb = (ufoymb*ufosqrt(2))/ufovev;
id ufoyc = (ufoymc*ufosqrt(2))/ufovev;
id ufoydo = (ufoymdo*ufosqrt(2))/ufovev;
id ufoye = (ufoyme*ufosqrt(2))/ufovev;
id ufoym = (ufoymm*ufosqrt(2))/ufovev;
id ufoys = (ufoyms*ufosqrt(2))/ufovev;
id ufoyt = (ufoymt*ufosqrt(2))/ufovev;
id ufoytau = (ufoymtau*ufosqrt(2))/ufovev;
id ufoyup = (ufoymup*ufosqrt(2))/ufovev;
id ufomuH = ufosqrt(ufolam*ufovev^2);
id ufoI1a11 = ufoydo*ufocomplexconjugate(ufoCKM1x1);
id ufoI1a12 = ufoydo*ufocomplexconjugate(ufoCKM2x1);
id ufoI1a13 = ufoydo*ufocomplexconjugate(ufoCKM3x1);
id ufoI1a21 = ufoys*ufocomplexconjugate(ufoCKM1x2);
id ufoI1a22 = ufoys*ufocomplexconjugate(ufoCKM2x2);
id ufoI1a23 = ufoys*ufocomplexconjugate(ufoCKM3x2);
id ufoI1a31 = ufoyb*ufocomplexconjugate(ufoCKM1x3);
id ufoI1a32 = ufoyb*ufocomplexconjugate(ufoCKM2x3);
id ufoI1a33 = ufoyb*ufocomplexconjugate(ufoCKM3x3);
id ufoI2a11 = ufoyup*ufocomplexconjugate(ufoCKM1x1);
id ufoI2a12 = ufoyc*ufocomplexconjugate(ufoCKM2x1);
id ufoI2a13 = ufoyt*ufocomplexconjugate(ufoCKM3x1);
id ufoI2a21 = ufoyup*ufocomplexconjugate(ufoCKM1x2);
id ufoI2a22 = ufoyc*ufocomplexconjugate(ufoCKM2x2);
id ufoI2a23 = ufoyt*ufocomplexconjugate(ufoCKM3x2);
id ufoI2a31 = ufoyup*ufocomplexconjugate(ufoCKM1x3);
id ufoI2a32 = ufoyc*ufocomplexconjugate(ufoCKM2x3);
id ufoI2a33 = ufoyt*ufocomplexconjugate(ufoCKM3x3);
id ufoI3a11 = ufoCKM1x1*ufoyup;
id ufoI3a12 = ufoCKM1x2*ufoyup;
id ufoI3a13 = ufoCKM1x3*ufoyup;
id ufoI3a21 = ufoCKM2x1*ufoyc;
id ufoI3a22 = ufoCKM2x2*ufoyc;
id ufoI3a23 = ufoCKM2x3*ufoyc;
id ufoI3a31 = ufoCKM3x1*ufoyt;
id ufoI3a32 = ufoCKM3x2*ufoyt;
id ufoI3a33 = ufoCKM3x3*ufoyt;
id ufoI4a11 = ufoCKM1x1*ufoydo;
id ufoI4a12 = ufoCKM1x2*ufoys;
id ufoI4a13 = ufoCKM1x3*ufoyb;
id ufoI4a21 = ufoCKM2x1*ufoydo;
id ufoI4a22 = ufoCKM2x2*ufoys;
id ufoI4a23 = ufoCKM2x3*ufoyb;
id ufoI4a31 = ufoCKM3x1*ufoydo;
id ufoI4a32 = ufoCKM3x2*ufoys;
id ufoI4a33 = ufoCKM3x3*ufoyb;
