# Example: HQET fermion self-energy at one-loop order

This example shows how eikonal propagators can be used in `tapir`. It follows closely along the lines of [QCD self-energy](../masslessQCD), and the reader should have a look at the QCD example before reading on. As in the QCD case, we will carry out all the steps needed to obtain the final result at the one-loop order.

## 1-loop
### Diagrams
At one-loop, there is only one diagram, the gluonic correction to the propagator of a heavy (HQET) quark (that we call `fbqHQET`. Inspecting the input file `qlistyaml_1lQSE.`, it looks exactly the same as the one-loop `qlist` file from the [QCD self-energy](../masslessQCD) example except for the name of the (now massive) quark.

In order to use `tapir`, we have to define the Feynman rules in the two files `hqet_ufo.prop` and `hqet_ufo.vrtx`. They differ substantially from the QCD case:
HQET describes the case where the total momentum of a heavy quark is close to its rest mass, and therefore it is advantageous to subtract the rest mass contribution according to the momentum decomposition $`p_{\mu} = m_Q v_{\mu} + \tilde{p}_{\mu}`$, where $`v_{\mu} v^{\mu} = 1`$ and $`\tilde{p}`$ is small compared to the heavy-quark mass. The heavy-quark propagator is then given by
```math
\frac{1 + v_{\mu} \gamma^{\mu}}{2} \frac{1}{\tilde{p} \cdot v + \mathrm{i} \varepsilon}
```
and the heavy-quark-gluon vertex changes to $`\mathrm{i} g_s v^{\mu} T^a`$. The four vector $`v`$ is a constant external label.

### tapir
In `tapir`, we can incorporate these Feynman rules as follows:
```
{fbqHQET,fBqHQET:*1/2*(d_(<spinor_index_vertex_2>,<spinor_index_vertex_1>) + auxSlashv(v,<spinor_index_vertex_2>,<spinor_index_vertex_1>))*invSP(v,<momentum>)|
		*d_(<spinor_index_vertex_2>,<spinor_index_vertex_1>)||}
{fBqHQET,fbqHQET,g:*( ufoGC11 * ( auxVecv(<lorentz_index_particle_3>) * d_(<spinor_index_particle_2>,<spinor_index_particle_1>) ))|
		*(GM(<colour_index_particle_3>,<spinor_index_particle_1>,<spinor_index_particle_2>))||}
```
We have introduced three functions, `auxSlashv`, `auxVecv` and `invSP`, that we need to take care of. The function `invSP` contains the heavy-quark propagator denominator, while the other two auxiliary functions are rather self-explanatory. Another object, the symbol `ufoGC11` is still around: this corresponds to $`\mathrm{i} g_s`$.

The `tapir` configuration file `hqet1l.yaml` looks pretty similar to the [QCD self-energy](../masslessQCD) case, with one notable difference: We introduce an external eikonal momentum `v` that is treated as a four vector internally.
Now we can run `tapir` (from its main directory):
```
./tapir -c example/selfenergies/HQET/hqet1l.yaml
```
Inspecting the output `1lQSE.dia` file, we can see that all vertex and propagator definitions have been properly inserted.


### Proceeding with the output (Lorentz fold)
We can further process the `tapir` output similarly to the [QCD self-energy](../masslessQCD) case (all of the following is contained in the FORM file `calculate_1loop.frm`).
A notable difference is the Dirac/Lorentz structure of the result. Since the HQET propagator is proportional to $`(1 + v_{\mu} \gamma^{\mu} )/2`$, we need to apply the corresponding projector, which in this case is given by
```
* Project out the component proportional to (1 + vslash)/2
multiply 1/4 * (fvertS(i2,i1)*scalarpart + auxGamma(-v,i2,i1)*vectorpart);
```
and taking the trace, which we will do a few steps later. The two labels `scalarpart` and `vectorpart` fulfil the sole purpose of tagging the contributions of the different parts of the projectors to the final result, and will eventually be set to `1`.

We also need to deal with the auxiliary functions we introduced earlier:
```
* Here we also need a minus sign because we run opposite to the fermion line
id auxSlashv(?x,j1?,j2?) = auxGamma(-v,j1,j2);
id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);
id invSP(v?,p?) = p.v^-1;
id auxVecv(nu?) = v(nu);
```

In order to take the trace, we will have to deal with gamma matrices in the Dirac chain as well as the unit matrix in Dirac space, so we need to introduce a few lines:
```
* Take the trace
id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
id fvertS(?a,j1?,j2?) = fermionchain(fvertS(?a), j1, j2);
repeat;
    id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
    id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
endrepeat;

repeat;
    id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
    id fermiontrace(fvertS(),?a) = gi_(1) * fermiontrace(?a);
endrepeat;
id fermiontrace = 1;
.sort

id Vec(nu?,p?) = p(nu);
.sort
Tracen 1;
id v.v = 1;
.sort
```
The remaining bits of `calculate_1loop.frm` are standard again: we include the relevant topology file `1ltopologies/d1l1`. Note that the definition of the scalar function `d1l1(n0,n1,n2,n3,n4)` contains the scalar products of loop momenta with the external labelling four vector `v`. This is why we needed to introduce the external eikonal "momentum" `v` in the `hqet1l.yaml` config file.

Using equation (2.27) from [Grozin's book](https://link.springer.com/book/10.1007/b79301) "Heavy quark effective theory", we can finally obtain our result and expand in epsilon.
```
* Integrate
id d1l1(2,0,-2,1) = Eq1^2 * d1l1(2,0,0,1);
id d1l1(n1?,0,0,n2?) = i_ * (4*pi)^(-d/2) * (-2*Eq1)^(d-2*n1) * Eq1^(-n2) * (-1)^n2 * Gamma(-d + n2 + 2*n1) * Gamma(d/2 - n1) / Gamma(n2) / Gamma(n1);
.sort
```
The result can be compared against equation (3.61) of the same book.

All routines related to the color part of the diagram are identical to the case of massless QCD and we refer to the corresponding [README file](../masslessQCD/README.md).


## 2-loop
At two-loop, all the steps are very similar to either the one-loop HQET case or the two-loop QCD case. In contrast to the QCD case, we consider the bottom quark as heavy, and do not allow closed bottom quark loops, hence we have only 9 diagrams instead of 10.

We can invoke `tapir` using
```
./tapir -c example/selfenergies/HQET/hqet2l.yaml
```
and end up with two distinct, but linearly dependent topologies. The topology files contain code to perform a partial fraction decomposition on these topologies.

The next steps in `calculate_2loop.frm` are very similar again to the [two-loop QCD](../masslessQCD/README.md#2-loop) case, and we won't repeat them here. Again, we do not perform the integration-by-parts reduction of the scalar functions here, but stop instead at this point.

The colour part of this calculation proceeds in the same way as in the one-loop case. We load all routines related to COLOR, load all diagrams and then invoke COLOR.

The script can be run using (you might have to `mkdir -p 2lresults` first)
```
form calculate_2loop.frm
```
