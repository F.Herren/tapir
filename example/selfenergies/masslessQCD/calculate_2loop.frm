#-
#include UFOdecl.inc
#define NUMDIAS "10"
#define NUMLOOPS "2"
#define DIAFILE "2lQSE.dia"

Symbol d;
Dimension d;
Off statistics;

CFunctions Mom, Dghost, Dgl, auxSlash, auxGamma, fprop, fpropnum, fvertV, propden, Vec, fermionchain, fermiontrace;
Symbol xiG;
Autodeclare Indices nu, i, j;
Vectors p, p1,...,p20, q1;
Symbol vectorpart;
Symbols n0, n1, ..., n5;
CFunction Gamma, Log, d`NUMLOOPS'l1,..., d`NUMLOOPS'l`NUMDIAS', Gfunc, gfunc, pgamma;
Symbols pi, ep, eulergamma, orderep3, x;
Symbols gs, mutilde;
Symbols logmq1sq, log4pisq, log4pi, logpisq, logpi, logmusq;
CFunction diagramlabel;

.global
.sort


* Define the procedure to handle individual diagrams
#procedure dia2l(i)

* Load the i-th diagram
    Global d`NUMLOOPS'l`i'amp = 
        #include `DIAFILE' #d`NUMLOOPS'l`i'
    .sort

* Project out the q-slash component
    multiply, 1/4 * vectorpart * auxGamma(q1,i2,i1) * q1.q1^-1;

* Rewrite helper functions
    id Mom(?x) = Vec(?x);
    id Dghost(?x,xiG?) = i_ * propden(?x);
    id Dgl(i1?,i2?,?x,xiG?) = -i_ * (d_(i1,i2) - (1 - xiG) * Vec(i1,?x) * Vec(i2,?x) * propden(?x) ) * propden(?x);
    id auxSlash(?x,j1?,j2?) = i_ * fprop(?x,j1,j2);
    id auxGamma(?x,j1?,j2?) = fvertV(?x,j1,j2);
    id fprop(p?, j1?, j2?) = propden(p) * fpropnum(-p, j1, j2);
    id fprop(-p?, j1?, j2?) = propden(p) * fpropnum(p, j1, j2);
    id propden(p?) = p.p^-1;
* id fpropnum(p2,j1?,j2?) = fpropnum(p1,j1,j2) + fpropnum(q1,j1,j2);
* id fpropnum(-p2,j1?,j2?) = fpropnum(-p1,j1,j2) + fpropnum(-q1,j1,j2);

* Take the trace
    id fpropnum(?a,j1?,j2?) = fermionchain(fpropnum(?a), j1, j2);
    id fvertV(?a,j1?,j2?) = fermionchain(fvertV(?a), j1, j2);
    repeat;
        id fermionchain(?a,j1?,j2?) * fermionchain(?b,j2?,j3?) = fermionchain(?a, ?b, j1, j3);
        id fermionchain(?a,j1?,j1?) = fermiontrace(?a);
    endrepeat;

    repeat;
        id fermiontrace(fpropnum(p?),?a) = g_(1,p) * fermiontrace(?a);
        id fermiontrace(fvertV(nu?),?a) = g_(1,nu) * fermiontrace(?a);
    endrepeat;
    .sort
    id fermiontrace = 1;

    id Vec(nu?,p?) = p(nu);
    Tracen 1;
    .sort


    #include mapping2l.inc #d`NUMLOOPS'l`i'
    `MOMREPLACEMENT'
    #include `NUMLOOPS'ltopologies/`INT1'
    .sort
    .store

    save 2lresults/d`NUMLOOPS'l`i'amp.res d`NUMLOOPS'l`i'amp;
    .sort

* Calculate the colour factor here...
#endprocedure




* Calculate each diagram separately using the procedure defined above
#do i=1, `NUMDIAS'
    #call dia2l(`i')
#enddo
.store


* Definitions
* PATHTOCOLORH needs to point to color.h
#define UFOCOLOUR "0"
#define PATHTOCOLORH "color.h"
#define RANK "14"
#define NUMINDEX "40"

#include `PATHTOCOLORH' #Declarations
Dimension NR;
autodeclare Index funi, funj;
Dimension NA;
autodeclare Index adja, adjb;


#include `PATHTOCOLORH' #color
#include `PATHTOCOLORH' #SORT
#include `PATHTOCOLORH' #adjoint

cf a,b,GM,V3g,prop,ufocomplex;

* Load diagrams (QCD fold)
#do i = 1, `NUMDIAS'
    Global fqcd`NUMLOOPS'l`i'amp = 
        #include `DIAFILE' #fqcd`NUMLOOPS'l`i'
#enddo
.sort
* Apply projector
multiply d_(i1,i2)/NR;
.sort

* Rename indices
#if ( `UFOCOLOUR' == 1)
  repeat id ufocomplex(0,1)^2 = -1;
#endif
#do N = 1, `NUMINDEX'
  argument;
    id a(`N') = adja`N';
    id b(`N') = adjb`N';
    id i`N' = funi`N';
    id j`N' = funj`N';
    #if ( `UFOCOLOUR' == 1)
      id aufo(`N') = adja{`N'+`NUMINDEX'+1};
      id bufo(`N') = adjb{`N'+`NUMINDEX'+1};
    #endif
  endargument;
#enddo

* Replace functions
id prop(?x) = d_(?x);
id GM(i1?,i2?,i3?) = T(i2,i3,i1);
id V3g(?x) = (-i_)*f(?x);
.sort

* Call color
#call color
#call SORT(tloop-1)

* Re-express color factor
id NA/NR*I2R = cR;
Print +s;
.store

* Load all results and add them up
Global d`NUMLOOPS'all = 
    #do i = 1, `NUMDIAS'
        + fqcd`NUMLOOPS'l`i'amp*d`NUMLOOPS'l`i'amp * diagramlabel(`i')
    #enddo
    ;
.sort

* Some final simplifications
id vectorpart = 1;
id ufoGC10 = -gs;
id ufoGC11 = i_ * gs;
id ufocomplex(0,1) = i_;
id diagramlabel(?x) = 1;
.sort

b gs,I2R,cA,cR;
print +s;
.end
