## @package diagram
# This module combines the graph information of each Feynman diagram with the classes modules.diagram.Diagram, modules.diagram.Vertex, modules.diagram.Line and modules.diagram.FermionLine.
#
# The Diagram class holds all information of the Feynman diagram such as a topological vertex list. This is just a list of Vertex objects which hold references to Line objects.
# It is thus possible to construct every graph theoretical relevant quantity (like a adjacency matrix) from it, as used modules.topoAnalyzer.TopologyAnalyzer.
#
# See  modules.topoAnalyzer.Edge for an alternative datatype to save graph edges.


import copy
import logging
import re
from modules.momentumAssigner import MomentumAssigner


## The basic data format for all Feynman diagrams/topologies
#
# Here, all information is stored about the particle flow through the diagram, as well as meta-data such as names, diagram prefactors and which momenta are flowing through each line.
class Diagram:
    ## The constructor
    #  is not used to set all fields directly, because there are too many fields involved.
    def __init__(self, conf=None):
        ## The configuration
        self.conf = conf
        ## The unique number of the diagram. If the diagram is read from a qlist file, the number is taken accordingly from there.
        self.diagramNumber = 0
        ## The combinatorical prefactor of the Feynman diagram stemming from qgraf
        self.preFactor = 0
        ## Amount of loops (circles + self-loops in graph-theoretical language)
        self.numLoops = 0
        ## Amount of ingoing legs
        self.numLegsIn = 0
        ## Amount of outgoing legs
        self.numLegsOut = 0
        ## A list combining all information about the external lines
        #
        # External momenta are always chosen incoming, signs of the momentum name have to be changed when handling different definitions.
        #
        # Its structure is as follows:
        # [A, B, C, D]
        # with
        # * A: momentum name with sign adjusted that the momentum is treated as incoming
        # * B: vertex number at which the leg is attached
        # * C: incoming particle name
        # * D: spinor index of the incoming particle
        #
        # Example: <code>[["q1", 1, "fT1", i1], ["q2", 3, "fT2", i2], ["-q3", 4, "h", i2]] </code>
        self.externalMomenta = []
        ## A list combining all information about the external lines
        #
        # Its structure is as follows:
        # [A, B, C, D, E, F, G, H]
        # with
        # * A: name of the momentum flowing through the edge
        # * B: start vertex
        # * C: end vertex
        # * D: name of the incoming particle (at start vertex)
        # * E: name of the outgoing particle (at end vertex)
        # * F: spinor index of the incoming particle (at start vertex)
        # * G: spinor index of the outgoing particle (at end vertex)
        # * H: name of the mass of the particle
        #
        # Example:  <code>[["p1", 1, 2, "fT1", "ft1", 1, 3, "M1"], ["p2", 3, 4, "fT2", "ft2", 4, 5, ""],  ...] </code>
        self.internalMomenta = []
        ## A list of Vertex objects, combining all vertex information of the diagram
        #
        # It is assigned by calling #createTopologicalVertexAndLineLists().
        self.topologicalVertexList = []
        ## A list of Line objects, expressing the information of #internalMomenta and #externalMomenta
        #
        # It is assigned by calling #createTopologicalVertexAndLineLists().
        # The Line instances of #Vertex::lines are defined as flowing out of the vertex, when this list is created by #createTopologicalVertexAndLineLists().
        self.topologicalLineList = []
        ## The same as #topologicalLineList but used to store the original one before mutation.
        # See #cacheTopologicalLineList()
        self.cachedTopologicalLineList = []
        ## A list of FermionLine objects, needed for the correct routing of spinor lines to aggregate correct \f$\gamma\f$-traces.
        # It is assign via #groupFermions().
        # The lines can be either open or closed (loops).
        self.fermionLines = []
        ## The unique name of the diagram. This is uniquely set via #setName() after initializing all Diagram fields.
        self.name = ""
        ## Comment to the diagram
        # It is printed in the dia files
        self.diaComment = ""
        ## The Nickel index of the underlying topology of the diagram
        # This field is only filled by modules.topoMinimizer.TopoMinimizer if the coarse minimization is used
        self.nickelIndex = ""
        ## The momenta which are bridges, i.e. linear combinations of only external momenta.
        # Will be filled by #createTopologicalVertexAndLineLists if bridges are removed from the topology file.
        self.bridgeMomenta = dict()


    ## Override the string representation and return all configurations of the current instance
    def __str__(self):
        string = "{\n"
        string += "    name: " + str(self.name) + "\n\n"
        string += "    diagramNumber: " + str(self.diagramNumber) + "\n\n"
        string += "    preFactor: " + str(self.preFactor) + "\n\n"
        string += "    numLoops: " + str(self.numLoops) + "\n\n"
        string += "    externalMomenta: {\n      " + "\n      ".join([str(v) for v in self.externalMomenta]) + "\n    }\n\n"
        string += "    internalMomenta: {\n      " + "\n      ".join([str(v) for v in self.internalMomenta]) + "\n    }\n\n"
        string += "    topologicalVertexList: {\n      " + "\n      ".join([str(v) for v in self.topologicalVertexList]) + "\n    }\n\n"
        string += "    topologicalLineList: {\n      " + "\n      ".join([str(l) for l in self.topologicalLineList]) + "\n    }\n\n"
        string += "    fermionLines: " + str(self.fermionLines) + "\n"
        string += "    diaComment: " + str(self.diaComment) + "\n"
        string += "    bridge momenta: {\n      " + "\n      ".join([str(key) + "  --->  " + str(self.bridgeMomenta[key]) for key in self.bridgeMomenta]) + "\n    }\n\n"
        string += "}"

        return string


    ## This gives the diagram a unique name.
    # It must be called after initializing the fields of Diagram.
    # The name is either set to the given value or of the type d<#numLoops>l<#diagramNumber>, e.g. d2l42.
    # @param name A custom name
    # @param appendNumbering Set to true and the name gets an additional `<loops>l<diagram number>` appended
    def setName(self, name="", appendNumbering=True):
        if name != "":
            self.name = name
        else:
            self.name = "d"
        if appendNumbering:
            self.name += str(self.numLoops) + "l" + str(self.diagramNumber)


    ## Create a list of Vertex instances and a list of Line instances corresponding to the entries of #internalMomenta and #externalMomenta.
    # The results are written to the #topologicalVertexList and #topologicalLineList fields.
    def createTopologicalVertexAndLineLists(self):
        self.topologicalVertexList = []
        self.topologicalLineList = []
        # pre-calculate all lines
        lines = []
        extLines = []
        maxVertexNumber = -1

        # internal
        for line in self.internalMomenta:
            # save the maximal vertex number
            if line[1] > maxVertexNumber:
                maxVertexNumber = line[1]
            if line[2] > maxVertexNumber:
                maxVertexNumber = line[2]

            l = Line()
            l.external = False
            l.momentum = line[0]
            # strictly accept only momenta of the form p1,...,pN
            try:
                assert re.match(r'\-*p[0-9]+', l.momentum), "Momentum ’" + l.momentum + "’ must have the form p3 etc."
            except Exception as e:
                logging.error(e)
                exit(1)
            l.number = int(re.sub(r'\-*p', r'', l.momentum))
            l.start = line[1]
            l.end = line[2]
            l.incomingParticle = line[3]
            l.outgoingParticle = line[4]
            l.mass = line[7]
            lines.append(l)

        # Sort list of lines by their momentum
            lines.sort(key=lambda entry: int(entry.momentum[1::]))

        externalLineNumber = -1

        # external
        for line in self.externalMomenta:
            # save the maximal vertex number
            if line[1] > maxVertexNumber:
                maxVertexNumber = line[1]
            # differentiate incoming and outgoing lines for topologicalLineList and topologicalVertexList, respectively
            l = Line()
            l.external = True
            # make external momentum (which is by default incoming) outgoing by changing sign
            l.momentum = line[0]
            l.start = externalLineNumber
            l.end = line[1]
            l.incomingParticle = line[2]
            l.outgoingParticle = ""
            l.number = externalLineNumber
            externalLineNumber -= 1
            # take external lines to be massless
            l.mass = ""
            extLines.append(l)

        # fill line list
        self.topologicalLineList = extLines + lines

        # fill vertex list
        for vertexNumber in range(1,maxVertexNumber+1):
            v = Vertex()
            # add number
            v.number = vertexNumber
            # add lines
            for oLine in (extLines + lines):
                line = copy.deepcopy(oLine)
                # line is fermionic, choose outgoing particle, since this one is never empty
                if (len(line.outgoingParticle) > 0 and line.outgoingParticle[0] == "f") or \
                    (len(line.incomingParticle) > 0 and line.incomingParticle[0] == "f"):
                    line.fermionLine = True
                if line.start == vertexNumber:
                    # line is outgoing
                    v.lines.append(line)
                elif line.end == vertexNumber:
                    # line is incoming, make it outgoing
                    line.start, line.end = line.end, line.start
                    line.outgoingParticle, line.incomingParticle = line.incomingParticle, line.outgoingParticle
                    # flip sign of momentum
                    if line.momentum[0] == "-":
                        line.momentum = line.momentum[1:]
                    else:
                        line.momentum = "-"+line.momentum
                    v.lines.append(line)
            v.checkIfFermionLineAttached()
            if len(v.lines) > 0:
                self.topologicalVertexList.append(v)

        if self.conf != None and (self.conf.topoIgnoreBridges or self.conf.ediaNoSigma):
            ## If we ignore bridges in the topology file, we should store the information how they depend on external momenta.
            # For this, we work on a copy of the diagram.
            copydia = copy.deepcopy(self)
            mA = MomentumAssigner(self.conf, copydia)
            mA.generateLineMassesDict()
            # Decompose all momenta in terms of L loop momenta and external momenta
            mA.calculateMomentumRelations()
            # Check if any momentum depends only on external momenta -> bridge.
            for mom in mA.momentumDict:
                foundBridge = True
                for mInt in [m[0] for m in self.internalMomenta]:
                    if mInt in mA.momentumDict[mom]:
                        foundBridge = False
                        break
                if foundBridge:
                    # Append all bridge momenta and their decomposition to self.bridgeMomenta.
                    self.bridgeMomenta[mom] = mA.momentumDict[mom]


    ## This function groups vertices by the fermionic lines they belong to.
    # It scans through #topologicalVertexList and generates #fermionLines.
    #
    # Note: 4 and more fermion vertices are not supported and would require a different implementation
    def groupFermions(self):
        # Filter vertices that adjacent fermion lines
        tmpList = [v for v in self.topologicalVertexList if v.fermionic]

        # Stop execution if a vertex does not contain two fermionic lines, and thus maskes the fermion routing ambivalent or not possible
        for v in tmpList:
            assert len([l for l in v.lines if l.fermionLine]) <= 2, "Fermion line contraction only possible with vertices with two attached fermions"

        if len(tmpList) == 0:
            return

        # Now that we have all vertices involving fermions in one list, check which ones are connected
        tmpFermionLine = FermionLine()

        startv = 0
        startl = -999
        # Here we select the start of an external line, if existing
        for i,v in enumerate(tmpList):
            for l in v.lines:
                if l.external and l.fermionLine:
                    if l.number > startl:
                        startl = l.number
                        startv = i

        tmpFermionLine.vertices = [tmpList.pop(startv)]
        nextVert = tmpFermionLine.vertices[-1].number

        # Go through the tmpList vertex list and follow adjacent fermionic lines by removing them iteratively from tmpList.
        # If one cannot reach another vertex using fermion lines only, close the fermion loop.
        # This procedure works only if vertices have exactly zero or two fermionic lines attached.
        # Fermionic self-loops are tested to work as well.

        lastVert = 0
        while len(tmpList) > 0:
            nextOne = False
            for l in tmpFermionLine.vertices[-1].lines:
                if l.fermionLine == True and l.external == False and l.start != l.end:
                    if nextVert == l.start and l.end != lastVert:
                        lastVert = nextVert
                        nextVert = l.end
                        if nextVert != tmpFermionLine.vertices[0].number:
                            nextOne = True
                        tmpFermionLine.lines.append(l)
                    elif nextVert == l.end and l.start != lastVert:
                        lastVert = nextVert
                        nextVert = l.start
                        if nextVert != tmpFermionLine.vertices[0].number:
                            nextOne = True
                        tmpFermionLine.lines.append(l)

            if nextOne == True:
                nextIndex = next(i for i,v in enumerate(tmpList) if v.number == nextVert)
                tmpFermionLine.vertices.append(tmpList.pop(nextIndex))
            else:
                tmpFermionLine.isLoop()
                tmpFermionLine.reOrderLine()
                self.fermionLines.append(tmpFermionLine)
                # New Line
                tmpFermionLine = FermionLine()

                startv = 0
                startl = -999
                # Here we select the start of an external line, if existing
                for i,v in enumerate(tmpList):
                    for l in v.lines:
                        if l.external and l.fermionLine:
                            if l.number > startl:
                                startl = l.number
                                startv = i
                tmpFermionLine.vertices = [tmpList.pop(startv)]
                nextVert = tmpFermionLine.vertices[-1].number
                # lastVert is needed to veto the line leading back to where we already were
                lastVert = -1

        # Append last fermion line
        tmpFermionLine.isLoop()
        tmpFermionLine.reOrderLine()
        self.fermionLines.append(tmpFermionLine)
        # Reorder external lines by the external momenta
        fermionLoops = [l for l in self.fermionLines if l.loop == True]
        fermionLines = [l for l in self.fermionLines if l.loop == False]
        # Temporary, can only handle 9 external fermion lines
        fermionLines.sort(key = lambda x: int(x.externalMomenta[0][-1]))
        self.fermionLines = fermionLines + fermionLoops


    ## Contract lines of the diagram and renumerate the vertices
    # Here, we remove the given lines, combine their adjacent vertices and rename all vertices alltogether.
    # Note: Spinor indices are not contracted!
    # @param lineNumbers number that corresponds to the name of the momentum in #internalMomenta, e.g. 6 for "p6"
    def contractLines(self, lineNumbers):
        # set the bridge vertices as equal, bridge start -> bridge end
        contractVertices = [ [p[1], p[2]] for p in self.internalMomenta if p[0] in [F"p{num}" for num in lineNumbers] ]
        logging.debug(F"Combine vertices: {contractVertices}")

        # remove the lines
        self.internalMomenta = [ p for p in self.internalMomenta if p[0] not in [F"p{num}" for num in lineNumbers] ]
        logging.debug(F"internal momenta left: {self.internalMomenta}")

        while len(contractVertices) > 0:
            currentRename = contractVertices.pop()
            # discard the case of self-loop contraction
            if currentRename[0] == currentRename[1]:
                continue

            # rename the vertices
            for p in self.internalMomenta:
                if currentRename[0] == p[1]:
                    p[1] = currentRename[1]
                if currentRename[0] == p[2]:
                    p[2] = currentRename[1]

            for q in self.externalMomenta:
                if currentRename[0] == q[1]:
                    q[1] = currentRename[1]

            # rename vertices in contractVertices also
            for v in contractVertices:
                if currentRename[0] == v[0]:
                    v[0] = currentRename[1]
                if currentRename[0] == v[1]:
                    v[1] = currentRename[1]

        # re-label vertices in ascending order
        vertexLabels = set([p[1] for p in self.internalMomenta] + [p[2] for p in self.internalMomenta] + [q[1] for q in self.externalMomenta])
        vertexLabels = list(vertexLabels)
        # [2,4,7,8] means, re-label 2->1, 4->2, 7->3, 8->4
        vertexLabels.sort()

        # also re-label line momenta in ascending order
        for i,p in enumerate(self.internalMomenta):
            # Do not rename the momentum in order to have the same names in the topology file
            # p[0] = re.sub(r'p[0-9]+$', F"p{i+1}", p[0])
            p[1] = vertexLabels.index(p[1]) + 1
            p[2] = vertexLabels.index(p[2]) + 1

        # relabel the vertices in the external momenta field
        for q in self.externalMomenta:
            q[1] = vertexLabels.index(q[1]) + 1

        # re-calculate diagram.Diagram.topologicalLineList and diagram.Diagram.topologicalVertexList
        self.createTopologicalVertexAndLineLists()


    ## Save a copy of the current #topologicalLineList in a new variable #cachedTopologicalLineList.
    # The cached field can be used later if the original one was mutated
    def cacheTopologicalLineList(self):
        self.cachedTopologicalLineList = self.topologicalLineList.copy()


## Basic class to wrap every information about a specific internal vertex.
class Vertex:
    ## Basic constructor
    def __init__(self):
        ## Unique vertex number
        self.number = 0
        ## Flag that indicates if there are fermionic lines attached to the vertex.
        # Assigned with #checkIfFermionLineAttached()
        self.fermionic = False
        ## List of Line objects which are attached to the vertex
        # Note that the lines have to be defined as going out of the vertex!
        #
        # I.e. the momenta have to be inverted if thats not the case.
        self.lines = []


    ## Override the string representation and return all configurations of the current instance
    def __str__(self):
        return "vertex " + str(self.number) + " : [" + ", ".join([str(l) for l in self.lines]) + "]"


    ## Look up if an attached Line is fermionic and save result in #fermionic
    #
    # Note that particle names that start with a "f" are treated as fermions!
    def checkIfFermionLineAttached(self):
        for l in self.lines:
            if l.fermionLine == True:
                self.fermionic = True
                return


    ## Get a list of names of outgoing particle
    # \return A list particle strings of outgoing Line's
    def getOutgoingParticles(self):
        ret = []
        for l in self.lines:
            # Append self-loops twice
            if l.start == self.number and l.external == False:
                ret.append(l.incomingParticle)
            if l.start == self.number and l.external == True:
                ret.append(l.outgoingParticle)
            if l.end == self.number:
                ret.append(l.outgoingParticle)
        return ret


    ## Get a list of momenta, defined as outgoing, that are attached to the vertex
    # \return A list of strings with (signed) momenta of the attached Line's
    def getOutgoingMomenta(self):
        ret = []
        for l in self.lines:
            # Append self-loops twice
            if l.isSelfLoop():
                ret.append("-" + l.momentum)
            ret.append(l.momentum)
        return ret


    ## Define and return special spinor indices of attached Line's.
    #
    # To get the same result as the legacy code (q2e), the indices \f$i\f$ are defined as follows:
    # Take the number in the momentum \f$n\f$, e.g. \f$n=5\f$ for "p5" or \f$n=1\f$ from "q1".
    # If the line is external, the index is given by \f$i = n\f$.
    #
    # If the line is internal, take the index as
    # \f{eqnarray*}{
    # i =
    # \begin{cases}
    # 2 \cdot (n+\#\text{External legs}-1)+1,& \text{if outgoing line momentum is positive} \\
    # 2 \cdot (n+\#\text{External legs}-1)+2,& \text{if outgoing line momentum is negative}
    # \end{cases}
    # \f}
    #
    # For the case a line is a self-loop (i.e. it starts at the vertex where it ends),
    # we add both variants
    #
    # @param numLegs Number of external legs of the whole diagram
    # \return A list of spinor indices attached to the vertex
    def getIndices(self, numLegs):
        indices = []
        for l in self.lines:
            index = int(re.sub(r'^-*[qp]', r'', l.momentum))

            if l.external:
                indices.append(index)
            else:
                # Add for self-loops both indices
                if l.isSelfLoop():
                    if l.momentum[0] != "-":
                        indices.append(2*(index+numLegs-1)+1)
                        indices.append(2*(index+numLegs-1)+2)
                    else:
                        indices.append(2*(index+numLegs-1)+2)
                        indices.append(2*(index+numLegs-1)+1)
                # Apply the above mentioned rule for normal lines
                else:
                    if l.momentum[0] != "-":
                        indices.append(2*(index+numLegs-1)+1)
                    else:
                        indices.append(2*(index+numLegs-1)+2)

        return indices



## A class combining all information of a single edge needed to uniquely identify the Diagram
class Line:
    ## Constructor
    def __init__(self, number = 0, start = 0, end = 0):
        ## True if line is no inner edge
        self.external = False
        ## Index of start vertex
        self.start = start
        ## Index of end vertex
        self.end = end
        ## Name of the momentum (possibly with sign)
        self.momentum = ""
        ## number of the Line (must be the same as the momentum tag, e.g. #momentum = "p5" => #number = 5)
        self.number = number
        ## Identifier string of the incoming particle (see modules.config.Conf.antiFermion)
        self.incomingParticle = ""
        ## Identifier string of the outgoing particle (see modules.config.Conf.antiFermion)
        self.outgoingParticle = ""
        ## True if line is fermionic
        self.fermionLine = False
        ## Name of the mass. If string is empty the line is massless
        self.mass = ""


    ## Override the string representation and return all configurations of the current instance
    def __str__(self):
        mass = ""
        if self.mass != "":
            mass = ", " + self.mass
        external = ""
        if self.external:
            external = ", external"
        fermion = ""
        if self.fermionLine:
            fermion = ", fermionic"
        return F"[Line {self.momentum} : From {self.start} to {self.end}{mass}{fermion}{external}]"


    ## Check if Line is a self-loop, i.e. start and end vertex are equal
    # \return True if lne is self-loop
    def isSelfLoop(self):
        return self.start == self.end


    ## Get the other end of the line
    # @param fromVertexNumber Vertex::number from which we want to find the other end
    # \return Vertex::number of the Vertex that is connected by this Line with \p fromVertexNumber
    def to(self, fromVertexNumber):
        if fromVertexNumber not in [self.start, self.end]:
            return 0
        if fromVertexNumber == self.start:
            return self.end
        return self.start



## A special class for fermionic line paths needed for the spinor routing.
#
# E.g. for the correct \f$\gamma\f$-traces.
class FermionLine:
    ## Contructor
    def __init__(self):
        ## List of Vertex instances along the path
        self.vertices = []
        ## List of Line instances that build the path
        self.lines = []
        ## true if path describes a circle
        self.loop = False
        ## Indicates if FermionLine is flipped.
        self.isFlipped = False
        ## List of flavours of the fermion line: fq, ft etc.
        self.flavours = []
        ## List of external momenta attached to this line (only relevant if #loop is False)
        self.externalMomenta = []


    ## Override the string representation and return all configurations of the current instance
    def __str__(self):
        return F"[FermionLine: Lines=[{', '.join([str(l) for l in self.lines])}], Loop={self.loop}, Flavors={self.flavours}, Flipped={self.isFlipped}]"


    ## Determines if this FermionLine is a closed loop.
    #  If the FermionLine is a loop #loop is set to true and
    #  the flavours of the loop are determined.
    def isLoop(self):
        for v in self.vertices:
            for l in v.lines:
                if l.external and l.fermionLine:
                    return False
        self.loop = True

        # Handle self-loops
        if len(self.vertices) == 1 and len(self.lines) == 0:
            for l in self.vertices[0].lines:
                if l not in self.lines and l.fermionLine and l.isSelfLoop():
                    self.lines.append(l)
        # Since Diagram.groupFermions reads the vertices it misses the last line of a closed loop, we fix this here.
        elif(len(self.vertices) > len(self.lines)):
            # Check all lines attached to the last vertex
            for l in self.vertices[-1].lines:
                if l.fermionLine:
                    # Since we only implement vertices with up to 2 fermions, one of the lines carries the same momentum as the last line in self.lines.
                    # We are looking for the other one and append it.
                    if l.number != self.lines[-1].number:
                        self.lines.append(l)
                        break
        self.determineFlavours()


    ## Determines which flavours correspond to this FermionLine and fills #flavours accordingly.
    #  For each propagator, both the in- and outgoing particle are checked.
    def determineFlavours(self):
        for l in self.lines:
            self.flavours.append(l.incomingParticle)
            self.flavours.append(l.outgoingParticle)
        self.flavours = list(set(self.flavours))
        self.flavours.sort()


    ## This function determines if the fermion line is flipped.
    #  For loops this means, the momentum flows in the other direction than the fermion flow.
    #  This is determined by the sign of the momenta, if all of them have a -, the loop is flipped.
    #  If not all momenta have the same sign, we print and error.
    #  For external lines
    def reOrderLine(self):
        if self.loop == True:
            # For self-loops this is no problem
            if len(self.lines) == 1 and self.lines[0].isSelfLoop():
                self.isFlipped = False
            elif all(line.momentum[0] == "-" for line in self.lines):
                self.isFlipped = True
            elif all(line.momentum[0] == "p" for line in self.lines):
                self.isFlipped = False
            else:
                logging.error("Fermion line has varying flow!")
        else:
            for l in self.vertices[0].lines:
                if l.fermionLine == True and l.external == True:
                    # Here we check if the external particle the line starts with is an ingoing fermion, or an anti-fermion.
                    # This is indicated by a lowercase letter (i.e. fq is a fermion, fQ is an anti-fermion).
                    # This is strictly necessary to match q2e.
                    if l.outgoingParticle[1].islower():
                        # If we deal with an incoming fermion, the line is flipped and we reverse #vertices and #lines
                        self.vertices.reverse()
                        self.lines.reverse()
                        self.isFlipped = True
                    # In any case, add the external momentum to #externalMomenta
                    self.externalMomenta.append(l.momentum)
