## @package ediaGenerator
# Here, we provide the necessary classes modules.ediaGenerator.EdiaEntry and modules.ediaGenerator.EdiaGenerator needed to produce .edia and topsel files from modules.diagram.Diagram instances.


import logging
import os
import re
from modules.utils import Utils


## Class used to create single edia/topsel entries from single modules.diagram.Diagram instances. It is called within EdiaGenerator.
class EdiaEntry:
    ## Constructor
    # @param conf modules.config.Conf instance that describes the meta data of current diagrams
    # @param dia modules.diagram.Diagram instance from which we want to create a edia/topsel entry
    def __init__(self, conf, dia):
        ## diagram name of \p dia
        self.name = "d" + str(dia.numLoops) + "l" + str(dia.diagramNumber)
        ## number of propagators of \p dia
        self.numPropagators = len(dia.internalMomenta)
        ## number of loops of \p dia
        self.numLoops = dia.numLoops
        ## number of independent external momenta of \p dia
        self.numIndependentMomenta = max(len(dia.externalMomenta) - 1, 0)
        ## highest mass index present or number of masses defined in \p conf, depending on which is higher.
        # `exp` treats this as "number of masses". Otherwise this would lead to incorrect results if the defined masses are not all present, because `exp` starts its mass counting always at 1. E.g. only `M2` is defined in \p conf
        self.maxMassNumber = len(set(conf.mass.values()))
        ## produce all combinations with an absent massive line (used in combination with exp)
        self.allowAbsentMassiveLines = conf.topoAllowAbsentMassiveLines
        # check all mass indices and set maxMassNumber to the highest one if higher than amount of different masses
        for m in set(conf.mass.values()):
            index = re.findall(r'[0-9]+$', m)
            if len(index) == 0:
                index = 0
            else:
                index = int(index[0])

            if index > self.maxMassNumber:
                self.maxMassNumber = index

        ## number of relevant scales of \p dia
        self.scales = conf.scales
        if len(dia.externalMomenta) > 0:
            externalMomOut = dia.externalMomenta[-1][1]
        ## A list of all independent external momenta
        #
        # (I.e. the number off all external momenta - 1)
        #
        # Including
        # * momentum name (without sign)
        # * an expansion tag ("e" or "") telling if this momentum is expanded naively or not
        # * vertex number to which the external line is attached
        # * vertex number to which the last (non-independent) external momentum is attached
        #
        # Example <code> [["q1", "e", 1, 4], ["q2", "", 2, 4], ["q3", "", 3, 4]]  </code>
            self.externalMomFlow = [[mom[0],self.extMomExpand(mom[0],conf.expandNaive),mom[1],externalMomOut] for mom in dia.externalMomenta[:-1]]
        else:
            self.externalMomFlow = []
        ## A list of all internal momenta
        #
        # Including
        # * momentum name (without sign)
        # * name of mass corresponding to the line of the momentum (with expansion identifier "e" if in the mass is expanded naively)
        # * vertex number of the starting point of the line (defines direction of momentum)
        # * vertex number of the end point of the line
        #
        # Example <code> [["p1", "M1", 1,2], ["p2", "M2,e", 1, 4], ["p3", "", 2, 4], ...]  </code>
        self.internalMomFlow = [[mom[0],self.addMasses(mom[3:],conf.mass,conf.expandNaive),mom[1],mom[2]] for mom in dia.internalMomenta]
        ## Reference to the modules.diagram.Diagram instances we focus on
        self.diaRef = dia


    ## Create a edia entry in the appropriate format.
    # This can be called directly after instantiation.
    # \return Formated edia entry string
    def toEdiaEntry(self):
        outString = "{" + self.name + ";" + str(self.numPropagators) + ";" + str(self.numLoops) + ";"
        outString += str(self.numIndependentMomenta) + ";" + str(self.maxMassNumber) + ";" + ",".join(self.scales) + ";"
        outString += "".join([self.momFlowtoString(m) for m in self.externalMomFlow])
        outString += "".join([self.momFlowtoString(m) for m in self.internalMomFlow])
        outString += "}\n"
        return outString


    ## Create a topsel entry in the appropriate format.
    # This can be called directly after instantiation.
    # \return Formated topsel entry string
    def toTopselEntry(self):
        outString = "{" + self.diaRef.name + ";" + str(self.numPropagators) + ";" + str(self.numLoops) + ";"
        outString += str(self.numIndependentMomenta) + ";" + str(self.maxMassNumber) + ";;"
        outString += "".join([self.momFlowtoString(m) for m in self.externalMomFlow])
        outString += "".join([self.momFlowtoString(m,True) for m in self.internalMomFlow]) + ";"
        if not self.allowAbsentMassiveLines:
            outString += "".join([str(self.getMassIndexFromMomFlow(m)) for m in self.internalMomFlow])
        else:
            massDistribution = "".join([str(self.getMassIndexFromMomFlow(m)) for m in self.internalMomFlow])
            # Create all possible replacements of one or several line masses by 'x', but only if the mass of the line was not zero
            possibleCharactersDicts = [{str(i): [str(massDistribution[i]), 'x']} if str(massDistribution[i]) != "0" else {str(i): ["0"]} for i in range(len(massDistribution))]
            possibleCharacters = dict(map(dict.popitem, possibleCharactersDicts))
            massStrings = []
            massStringsUnique = []
            for n in range(2**len(massDistribution) - 1): # the last number 2**n corresponds to the mass assignment 'xxxx', which we do not need
                binaryString = '0b' + bin(n)[2:].zfill(len(massDistribution)) # binary notation, padded to fixed length
                # Append all combinations where one or more massive lines are absent
                # If the line is massless then len(possibleCharacters[i] == 1, so the "0" will never be replaced by 'x'
                massStrings.append("".join([possibleCharacters[str(i)][int(binaryString[i+2])] if len(possibleCharacters[str(i)]) == 2 else "0" for i in range(len(binaryString) - 2)]))
            # Now remove duplicate entries (they arise if at least one line is massless)
            massStringsUnique = sorted(list(set(massStrings)))
            outString += ";".join(massStringsUnique)
        outString += "}\n"
        return outString


    ## Extract mass index from an element of #internalMomFlow
    #
    # The mass index is defined as a number from 1,...,9. To have a strict hierarchy of masses, the allowed mass names are M1,....,M9.
    # The program stops if there is a non valid mass name involved.
    # @param momFlow Element of #internalMomFlow
    # \return Mass index
    def getMassIndexFromMomFlow(self, momFlow):
        # read the index of a mass (e.g. M2 -> 2) from a mom flow object
        if momFlow[1] == "":
            return 0
        else:
            massNumber = re.sub(r'^M([1-9]).*', r'\1', momFlow[1])
            try:
                assert massNumber in [str(i) for i in range(1,10)], "Masses have to have a form like M1,...,M9! For {} this is not the case".format(momFlow[1])
            except Exception as e:
                logging.error(e)
                exit(1)
            return int(massNumber)


    ## Translate an element of #internalMomFlow to a formated string
    #
    # Example: <code> ["p1", "M1", 1, 4] </code> gets <code> "(p1,M1:1,4)" </code> or <code> "(p1:1,4)" </code> for <code> \p skipMass = True </code>
    # @param momFlow Element of #internalMomFlow
    # @param skipMass Setting this to True ignores the mass of \p momFlow
    # \return Formated momFlow string
    def momFlowtoString(self, momFlow, skipMass=False):
        if momFlow[1] == "" or skipMass:
            return "("+str(momFlow[0])+":"+str(momFlow[2])+","+str(momFlow[3])+")"
        else:
            return "("+str(momFlow[0])+","+str(momFlow[1])+":"+str(momFlow[2])+","+str(momFlow[3])+")"


    ## Tell if momentum must be expanded naively and give expansion identifier if that's indeed the case
    # @param mom External momentum name
    # @param naiveList List of naive expanded scales, see modules.config.Conf::expandNaive
    # \return "e" if \p mom is expanded naively, empty string otherwise
    def extMomExpand(self, mom, naiveList):
        if mom in naiveList:
            return "e"
        else:
            return ""


    ## Tell from a given particle list ([particle name, anti-particle name]) name the mass and add a naive expansion identifier if its mass is in \p naiveList
    # @param particles A list of particle name and anti-particle name we want to check for masses
    # @param massDict A dictionary of particles and masses as modules.config.Conf::mass
    # @param naiveList A list containing the masses a naive taylor expansion should be done with
    # \return A String with the mass of \p particles with expansion identifier, e.g. <code>"M1,e" </code>
    def addMasses(self, particles, massDict, naiveList):
        if particles[0] in massDict:
            ret = massDict[particles[0]]
            if ret in naiveList:
                ret += ",e"
            return ret
        elif particles[1] in massDict:
            ret = massDict[particles[1]]
            if ret in naiveList:
                ret += ",e"
            return ret
        else:
            return ""



## Edia and topsel file generator class
#
# After the constructor, one has to call #generateEntries() with the modules.diagram.Diagram objects first and afterwarts #writeEdia() or #writeTopsel() to generate the appropriate files.
class EdiaGenerator:
    ## Constructor
    # @param conf modules.config.Conf instance that describes the meta data of current diagrams
    def __init__(self, conf, verbose=False):
        ## modules.config.Conf instance that describes the meta data of current diagrams
        self.conf = conf
        ## List of EdiaEntry objects generated by #generateEntries()
        self.entryList = []
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose

        self.utils = Utils(conf, self.verbose)


    ## Generate #entryList from modules.diagram.Diagram instances
    # @param dias List of modules.diagram.Diagram objects
    def generateEntries(self, dias):
        self.utils.vprint(" Generate edia/topsel entries: \r", end = "")
        self.entryList = [EdiaEntry(self.conf,dia) for dia in dias]
        self.utils.vprint(" Generate edia/topsel entries: Done")


    ## Write #entryList to a .edia file
    # @param outputFileName The file name of the output destination
    def writeEdia(self, outputFileName):
        # write entries to .edia file
        self.generatePath(outputFileName)

        if self.conf.blockSize < 1:
            with open(outputFileName,'w') as ediaFile:
                for entry in self.entryList:
                    ediaFile.write(entry.toEdiaEntry())
        else:
            listOfEntryLists = [self.entryList[i:i+self.conf.blockSize] for i in range(0, len(self.entryList), self.conf.blockSize)]
            for i in range(0,len(listOfEntryLists)):
                with open(outputFileName + "." + str(i+1),'w') as ediaFile:
                    for entry in listOfEntryLists[i]:
                        ediaFile.write(entry.toEdiaEntry())


    ## Write #entryList to a topsel file
    # @param outputFileName The file name of the output destination
    # @param lengthOrdered True if output should be written to file ordered from most to least number of propagators
    def writeTopsel(self, outputFileName, lengthOrdered=False):
        # order topsel entries by number of propagators from highest to lowest
        if lengthOrdered:
            self.entryList.sort(key=self.getNumberOfPropagators, reverse=True)

        self.generatePath(outputFileName)

        with open(outputFileName,'w') as topselFile:
            for entry in self.entryList:
                topselFile.write(entry.toTopselEntry())


    ## Extract number of propagators from an EdiaEntry
    # @param entry The Edia Entry object from which one wants to know the number of propagators
    # \return Amount of Propagators
    def getNumberOfPropagators(self, entry):
        return entry.numPropagators


    ## Generate the directories to the given file path
    # @param file Full or relative path to the file one wants to write to
    @staticmethod
    def generatePath(file):
        # create path if it does not exist already
        if not os.path.exists(os.path.dirname(file)) and os.path.dirname(file) != "":
            try:
                os.makedirs(os.path.dirname(file))
            except:
                logging.error("Cannot create path of " + str(file))
                exit(1)
