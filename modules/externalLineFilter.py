## @package externalLineFilter
#
# The here described class modules.externalLineFilter.ExternalLineFilter is used to change the topological features
# of a bunch of modules.diagram.Diagram instances due to the options set in modules.config.Conf::externalMomenta.


from typing import List
from modules.diagram import Diagram
from modules.filter import Filter
from modules.utils import Utils
import re
import pathos.multiprocessing as mp
import time
import signal


## This class removes external lines of modules.config.Conf::externalMomenta, if they are specified to be zero.
# E.g. `externalMomenta = {"q2": 0}` will remove the line corresponding to `q2` of all diagrams that are piped through #filter().
class ExternalLineFilter(Filter):
    ## Constructor
    # @param conf modules.config.Conf instance where modules.config.Conf::externalMomenta is (possibly) specified
    def __init__(self, conf, verbose=False, kernels=1):
        ## List of external momentum names, which are set to zero in modules.config.Conf::externalMomenta
        self.zeroMomenta = [key for key,val in conf.externalMomenta.items() if val == 0 or val == "0" or val == ""]
        ## verbose flag to print progress during minimization.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels

        self.utils = Utils(conf, self.verbose)


    ## Basic pipe method that changes the topological structure of an input list of modules.diagram.Diagram's to the specifications of modules.config.Conf::externalMomenta.
    # It is run in parallel for best performance result.
    # The diagrams with external momenta `q1,q2,q3` are changed for the following cases of modules.config.Conf::externalMomenta:
    # * `"q1"=0`, `"q1"="0"` or `"q1"=""` : The corresponding external line is removed from diagram.Diagram.externalMomenta and thus removed from all its derivatives (diagram.Diagram::topologicalLineList etc.)
    # Be aware, that this mehtod is destructive! I.e. The diagrams are intrinsically changed.
    # @param diagrams List of modules.diagram.Diagram instances we want to filter
    # \return \p diagrams where the lines with zero momenta are removed
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        # continue with main program when there's nothing to remove
        if len(self.zeroMomenta) == 0:
            return diagrams


        # remove unwanted lines from the diagrams in main thread
        if self.kernels == 1:
            self.utils.vprint(" Remove external zero momenta lines...\r", end = "")
            dias = [self.removeLinesOfDiagram(d) for d in diagrams]
            self.utils.vprint(" Remove external zero momenta lines... Done                  ")
            return dias

        # remove unwanted lines from the diagrams in parallel
        else:
             # initializer function for each worker in the multithread pool
            def initializer():
                # ignore CTRL+C in the worker process
                signal.signal(signal.SIGINT, signal.SIG_IGN)

            if self.kernels == 0:
                pool = mp.Pool(mp.cpu_count(),initializer=initializer)
            else:
                pool = mp.Pool(self.kernels,initializer=initializer)

            # start asynchronous workers
            result = pool.map_async(self.removeLinesOfDiagram, diagrams, chunksize=1)

            # print progress with actualization rate of 10 Hz
            while not result.ready():
                self.utils.vprint(" Remove external zero momenta lines: {0:^7}/{1:^7}\r".format(len(diagrams)-result._number_left, len(diagrams)), end = "")
                time.sleep(0.1)
            self.utils.vprint(" Remove external zero momenta lines: Done                  ")
            pool.close()
            pool.join()

            return result.get()


    ## Remove external lines corresponding to zero momentum insertions from a list of modules.diagram.Diagram instances
    # @param diagrams List of modules.diagram.Diagram instances of which we want to remove the lines. Note that this list is altered!
    # \return List of diagrams with changed topology
    def removeLinesOfDiagram(self, diagram):
        # save the vertex numbers of the removed external lines
        numbersOfRemovedVertices = []
        for externalMomentum in list(diagram.externalMomenta):
            # find matches with zeroMomenta and remove them from externalMomenta
            if re.sub(r'^\-*', r'', externalMomentum[0]) in self.zeroMomenta:
                numbersOfRemovedVertices.append(externalMomentum[1])
                diagram.externalMomenta.remove(externalMomentum)

        # re-calculate diagram.Diagram.topologicalLineList and diagram.Diagram.topologicalVertexList
        diagram.createTopologicalVertexAndLineLists()
        return diagram

