## @package filter
# This is the basis for all filter related features of TAPIR.
# A filter is a class derived from modules.filter.Filter.
# The derivation must incorporate the modules.filter.Filter::filter() mehtod to be fully functional.
#
# A filter is nothing else than a modules.filter.Filter derivation instance that takes a list modules.Diagram.diagram objects and returns a copy with altered entries.

from typing import List
from modules.diagram import Diagram


## Basic filter class template
# To use it, one has to inherit the #filter() mehtod
class Filter:
    ## This central method provides the basic functionality of each filter derivative
    # @param diagrams A list of modules.diagram.Diagram objects objects that shall be filtered
    # \return Changed list of modules.diagram.Diagram objects. This list should be either a copy of the old input \p diagrams or just remove the amount of modules.Diagram.diagram's
    # without changing the initial input list.
    def filter(self, diagrams: List[Diagram]) -> List[Diagram]:
        pass
