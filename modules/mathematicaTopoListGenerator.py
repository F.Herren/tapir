## @package mathematicaTopoListGenerator
# This module reads Mathematica topology list files and generates according modules.integralFamily.IntegralFamily instances from it.


import logging


## Main class to read and translate topology list files in Mathematica format to modules.integralFamily.IntegralFamily instances
class MathematicaTopoListGenerator:
    ## Constructor
    #  @param conf An initialized config object
    #  @param verbose Increase verbosity level if set to true
    #  @param kernels Run certain parts of the module in parallel using the given number of cores
    def __init__(self, conf, verbose=False, kernels=1):
        ## config.Conf object reference.
        self.conf = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose
        ## Option to run in parallel
        # 0 means use all available cores
        # n (>0) gives the amount of cores to run on
        self.kernels = kernels


    ## Export a list of modules.integralFamily.IntegralFamily instances to a Mathematica readable file
    # This method calls #integralFamilyToMathematica() to translate every integral family into a Mathematica readable string and writes it to a file.
    # For the input three lists of the same length should be provided, where the i'th entry of each list describes the i'th integral family.
    # @param outputFileName The file name of the output destination
    # @param integralFamilies List of modules.integralFamily.IntegralFamily instances which serve as output
    def writeIntegralFamilies(self, outputFileName, integralFamilies):
        try:
            with open(outputFileName,'w') as mmFile:
                mmFile.write("{")
                # Print individual integral families one after another
                mmFile.write(
                    ",\n".join([self.integralFamilyToMathematica(family)  for family in integralFamilies])
                )
                mmFile.write("}\n")

        except FileNotFoundError:
            logging.error("Not able to write " + outputFileName)
            exit(1)


    ## Translate integral family information to a Mathematica readable string
    # For the input lists of the same length should be provided, where the i'th entry of each list describes the i'th integral family.
    # For example:
    #    {"Int1", (k1 + q1)^2, (k1 + q1 + q2)^2, k1^2, k2^2-M1^2, (k1-k2)^2}, {k1, k2}
    # @param outputFileName The file name of the output destination
    # @param familyNames List of strings representing the names of the integral families
    # @param propagatorLists List of lists decribing the the analytic structure of the denominator functions of the integral family. One entry has the form of e.g. ["-p1^2", "M1^2 - (p1+q)^2"]
    # @param loopMomentaLists List of lists of strings with the momenta used as loop momenta in \p propagators
    def integralFamilyToMathematica(self, integralFamily):
        mmEntry = '{"' + integralFamily.topoName + '", '
        mmEntry += "{" + ", ".join([str(s).replace('**','^') for s in integralFamily.analyticTopologyExpression]) + "}, "
        mmEntry += "{" + ", ".join([str(s) for s in integralFamily.loopMomenta]) + "}"
        mmEntry += "}"

        return mmEntry
