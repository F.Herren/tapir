## @package momentumAssigner
#
# Here we provide a (more or less) basic toolset which is needed to find relations between momenta along a Feynman diagram (modules.diagram.Diagram).
# For this purpose, the class modules.momentumAssigner.MomentumAssigner offers some static methods and data formats (such as a "momentum matrix", see modules.momentumAssigner.MomentumAssigner::createMomentumMatrix())
# that can be also used for other applications (for example minimization of diagrams).

import logging
import re
import sympy as sy

## Main class to find momentum relations of Feynman diagrams from their topological structure
# Here, all the momentum distributions of a graph is determined.
# Data types such as the momentumMatrix also enables finding topological properties of the graph.
#
# Many methods can be used independently (they are defined as static) with the correct input but for a complete generation of
# the momentumMatrix and replacement rules of momenta and momentum products, one has to initialize a MomentumAssigner instance and run the methods:
#
# * #generateLineMassesDict() to fill the #lineMasses field
# * #calculateMomentumRelations() to calculate #momentumDict and #momentumProducts
# * #calculateMomentumProducts() to calculate #momentumProducts and #topoFactors, #topoFactorSigns
class MomentumAssigner:
    ## Constructor
    # @param conf modules.config.Conf instance that describes the meta data of current diagrams
    # @param diagram modules.diagram.Diagram instance of which we want to determine the momentum routing
    def __init__(self, conf, diagram):
        ## modules.config.Conf instance that describes the meta data of current diagrams
        self.conf = conf
        ## diagram modules.diagram.Diagram instance of which we want to determine the momentum routing
        self.diagram = diagram
        ## Dictionary of internal momenta and their corresponding masses.
        # Example: `{"p1": "M1", "p2":0, "p3": "M2"}`, where '0' stands for massless lines
        # It is provided by #generateLineMassesDict().
        self.lineMasses = {}
        ## List of external momentum names. Example: <code>["q1", "q2", "qSomething"]</code>
        self.externalMomenta = []
        ## List of linearly independent external momentum names. Example: <code>["q1", "q2"]</code>
        self.irreducibleExternalMomenta = []
        ## List of internal momentum names which were chosen to be loop momenta
        self.loopMomenta = []
        ## Dict of momentum multiplications in terms of not further reducible momenta. Syntax: <code>{"p1.q2" : "p2.p2 + q2.q2", ...}</code> meaning \f$p_1 \cdot q_2 = p_2 \cdot p_2 + q_2 \cdot q_2\f$
        self.momentumProducts = {}
        ## Subset of #momentumProducts whose entries can be directly substituted by an other momentum product.
        # E.g. `{"p1.p1": "p6.p6", "p7.p7": "p6.p6", "p3.p3": "p2.p2"}`
        self.equalMomentumSquaredProducts = {}
        ## List that collects all non-further reducible scalar terms as an argument for the topology function. Example: <code>["1/p1.p1", "s2m1", "1/q1.p1"]</code>.
        # This field is calculated in #calculateMomentumProducts().
        self.topoFactors = []
        ## List of signs ("-" or "") of the #topoFactors.
        # Together with #topoFactors this defines the arguments of the scalar topology function.\n
        # We use the Euclidean convention, which means that the topology function has arguments of the type <code>-1/p1.p1</code> and </code>s2m1</code>\f$=\frac{1}{M_1^2-p_2^2}\f$.\n
        # Note: everything is handled in this convention, if conf.Conf.euclideanMomenta is set, only topoFileGenerator.getTopoString() takes care of this for the output
        # This field is calculated in #calculateMomentumProducts().
        self.topoFactorSigns = []
        ## List of Strings that describe the (denominator) content of #topoFactors in terms of loop momenta, external momenta and masses with correct signs (see #topoFactorSigns).
        # This field is calculated in #calculateMomentumProducts().\n
        # Example: \f$\texttt{d1l1}(a,b) = \frac{1}{(-p1^2+M_1^2)^a (-q_1 \cdot p_1)^b} \rightarrow\f$ `topoFactors = ["s1m1", "1/q1.p1"]` and `analyticTopologyExpression = ["-p1^2+M1^2", "-q1*p1"]`
        self.analyticTopologyExpression = []
        ## Dict of momenta and how they're composed of in terms of other momenta.
        # This field is assigned in #calculateMomentumRelations() via #findEqualMomenta().\n
        # Example: <code>{"p3" : "p1-q1"}</code>
        self.momentumDict = {}
        ## Dict of momenta whose absolute value is the same as another momentum.
        # This field is assigned in #calculateMomentumProducts() via #extractMomentumDictFromEchelonMatrix().\n
        # Example: <code>{"p3" : "-q", "p4" : "p2"}</code>
        self.equalMomentumDict = {}


    ## Map all line momenta to the corresponding line mass.
    # The result is written to #lineMasses
    def generateLineMassesDict(self):
        # check if an internal line is massive and save its corresponding momentum
        for momentum in self.diagram.internalMomenta:
            try:
                if momentum[3] in self.conf.mass.keys():
                    self.lineMasses[momentum[0]] = self.conf.mass[momentum[3]]
                elif momentum[4] in self.conf.mass.keys():
                    self.lineMasses[momentum[0]] = self.conf.mass[momentum[4]]
                else:
                    self.lineMasses[momentum[0]] = 0
            except AttributeError:
                logging.warning("Cannot look up mass for line {momentum}: Assuming line to be massless.")
                self.lineMasses[momentum[0]] = 0

        logging.debug("Line masses: " + str(self.lineMasses))


    ## Compute a momentum matrix which combines all momentum relations coming from momentum conservation at each vertex
    #
    # The momentum matrix is a linear operator acting on a \p momentumNames vector.
    # A row resembles the coefficients of the momenta adding to zero which is true for every vertex of a diagram.
    #
    # If we take the following diagram into account:
    # \image html box-topo.png "Topology description of box diagram" height=300px
    # \image latex box-topo.eps "Topology description of box diagram"
    # we find the following momentum conservation relations at each vertex:
    # \f{align*}
    #   q_1-p_1-p_2 &= 0\\
    #   q_3+p_1-p_3 &= 0\\
    #   q2+p_2+p_4 &= 0\\
    #   q_4+p_3-p_4 &= 0\,.
    # \f}
    # This can be abbreviated in the following linear equation system:
    # \f{align*}
    #   \begin{pmatrix}
    #   -1 & -1 & 0 & 0 & 1 & 0 & 0 & 0\\
    #   1 & 0 & -1 & 0 & 0 & 0 & 1 & 0\\
    #   0 & 1 & 0 & 1 & 0 & 1 & 0 & 0\\
    #   0 & 0 & 1 & -1 & 0 & 0 & 0 & 1\\
    #   0 & 0 & 0 & 0 & 1 & 1 & 1 & 1\\
    #   \end{pmatrix} \cdot \begin{pmatrix}
    #  p_1 \\
    #  p_2 \\
    #  p_3 \\
    #  p_4 \\
    #  q_1 \\
    #  q_2 \\
    #  q_3 \\
    #  q_4
    # \end{pmatrix} = 0
    # \f}
    # To account for overall momentum conservation, we add a additional row to the matrix.
    # The matrix is what we call the "momentumMatrix" and the vector is simply related as "momentumNames".
    #
    # @param topologicalVertexList A list of modules.diagram.Vertex instances that describe the topological structure of a graph, as given in modules.diagram:Diagram::topologicalVertexList
    # @param momentumNames A list of momentum name strings defining the columns of the matrix
    # @param externalMomenta A possible dict of external momentum replacements, as given in modules.config.Conf::externalMomenta
    # \return momentumMatrix: A list of lists (i.e. a matrix) that describes the momentum relations at every vertex of the diagram
    @staticmethod
    def createMomentumMatrix(topologicalVertexList, momentumNames, externalMomenta = {}):
        momentumMatrix = []
        for vertex in topologicalVertexList:
            # initialize row
            v = [0 for i in momentumNames]
            # fill row with entries
            for line in vertex.lines:
                p = re.sub(r'-*', '', line.momentum)
                # if the vertex has an associated loop, remove loop momentum from matrix (the only entry) and save the propagator for later
                if line.start != line.end:
                    # rename p in case its defined in the conf file
                    if line.external and p in externalMomenta.keys():
                        p = externalMomenta[p]
                    prefactor = eval(re.sub(r'[^-]*$', '', p) + re.sub(r'[^-]*$', '', line.momentum) + "1")
                    # change sign if momentum is incoming
                    if vertex.number == line.end:
                        prefactor = - prefactor
                    v[momentumNames.index(re.sub(r'-*', '', p))] += prefactor

            momentumMatrix.append(v)
        return momentumMatrix


    ## This method extracts a bunch of relations between the momenta of a graph and thus reduces the number of independent momenta
    #
    # @param momentumMatrix The momentumMatrix generated by #createMomentumMatrix() in a reduced row echelon form (use gaussian elimination as far as possible on the matrix)
    # @param momentumNames List of momentum names defining the columns of \p momentumMatrix
    # \return A dict of replacement rules for non-minimal momenta in terms of minimal momenta. E.g. <code> {"p2" : "p3", "p1" : "q1-q2+p3"}</code>
    @staticmethod
    def extractMomentumDictFromEchelonMatrix(momentumMatrix, momentumNames):
        # read out the linearly independent entries of the matrix and write them as linear combination of others to a dict
        momentumDict = {}
        for row in momentumMatrix.tolist():
            # extract first non-zero entry of the row
            if 1 in row:
                reducibleMomentumIndex = row.index(1)
                # kick-out the specific entry
                row[reducibleMomentumIndex] = 0

                row = sy.Matrix([row,])
                # invert rest
                row = -row

                # get linear combination
                row = row * sy.Matrix(sy.symbols(" ".join(momentumNames)))
                momentumDict[momentumNames[reducibleMomentumIndex]] = str(row.tolist()[0][0])
                logging.debug(momentumNames[reducibleMomentumIndex] + " = " + str(row.tolist()[0][0]))
            else:
                logging.error("Momentum matrix not properly reduced!")
                exit(1)
        return momentumDict


    ## This method extracts all loop momenta of a momentumMatrix in a reduces row echelon form.
    # @param momentumMatrix The momentumMatrix generated by #createMomentumMatrix() in a reduced row echelon form (use gaussian elimination as far as possible on the matrix)
    # @param momentumNames List of momentum names defining the columns of \p momentumMatrix
    # @param externalMomentumNames List of names of all external momenta
    # \return A list of names of all loop momenta
    @staticmethod
    def findLoopMomenta(momentumMatrix, momentumNames, externalMomentumNames):
        # extract loop momenta from missing steps in the row ecolon form, i.e. entries of momentumNames which cannot resolved
        loopMomenta = momentumNames.copy()
        for row in momentumMatrix.rowspace():
            loopMomenta.remove(momentumNames[row.tolist()[0].index(1)])
        # remove also external momenta
        return [x for x in loopMomenta if x not in externalMomentumNames]


    ## To minimize the number of momentum products, we have to know all relevant ones that could arise.
    # Calculate the relevant momentum products of loop times loop, loop times external and external times external momenta
    # The ordering is relevant for the later reduction to a minimal set of invariants.
    # Products of internal momenta are discarded.
    # @param loopMomenta List of names of loop momenta, generated by #findLoopMomenta()
    # @param externalMomentumNames List of names of all external momenta
    # \return A tuple consisting of
    # * pMix: List of products of loop momentum times external momentum, for example <code>[["p3", "q1"], ["p3", "q2"]]</code> (meaning \f$p_3\cdot q_1\f$ and \f$p_3\cdot q_2\f$)
    # * pLoop: List of products of loop momentum times loop momentum, for example <code>[["p3", "p5"], ["p3", "p3"], ["p5", "p5"]]</code> (meaning \f$p_3\cdot p_5\f$, \f$p_3^2\f$ and \f$p_5^2\f$)
    # * pExternal: List of products of external momentum times external momentum, for example <code>[["q1", "q1"], ["q1", "q2"], ["q2", "q2"]]</code> (meaning \f$q_1^2\f$, \f$q_1\cdot q_2\f$ and \f$q_2^2\f$)
    @staticmethod
    def findRelevantMomentumProducts(loopMomenta, externalMomenta):
        pRelevant = loopMomenta + externalMomenta

        pMix = []
        pExternal = []
        pLoop = []

        for i in range(0,len(pRelevant)):
            for j in range(i,len(pRelevant)):
                p1 = pRelevant[i]
                p2 = pRelevant[j]
                # put loop momentum times external momentum at the beginning
                if (p1 in loopMomenta and p2 in externalMomenta):
                    pMix.insert(0, [p1,p2])
                elif (p1 in externalMomenta and p2 in loopMomenta):
                    pMix.insert(0, [p2,p1])
                # put loop momentum times loop momentum right after
                elif p1 in loopMomenta and p2 in loopMomenta:
                    if p1 != p2:
                        pMix.append(sorted([p1,p2], key=lambda x: (len(x),x)))
                    else:
                        pLoop.append(sorted([p1,p2], key=lambda x: (len(x),x)))
                # save external times external momenta separately
                elif p1 in externalMomenta and p2 in externalMomenta:
                    pExternal.append(sorted([p1,p2], key=lambda x: (len(x),x)))
                else:
                    logging.error(p1 + " and/or " + p2 + "are neither loop nor external momenta!")
                    exit(1)

        return pMix, pLoop, pExternal


    ## Calculate the matrix of momentum times momentum relations altogether with the appropriate basis vector.
    # We calculate both at the same time to improve performance.
    # The momentum product matrix can be understand as the momentumMatrix from #createMomentumMatrix() where all entries in rows are squared with respect to the momentum conservation relations.
    # The linear equations for this matrix are given by resolving the linear equations of the momentumMatrix for a reducible internal momentum and square bot sides.
    # For example (p2 is a loop momentum):
    # \f{equation*}
    #   q_1 - p_1 - p_2 = 0 \qquad \Rightarrow \qquad p_1^2 = (q_1 - p_2)^2 = q_1^2 - 2 q_1 \cdot p_2 + p_2^2
    # \f}
    #
    # An example is the first row of the linear equation system of the box diagram from #createMomentumMatrix():
    # \f{align*}
    # \left(\begin{tabular}{ccccccccccccc}
    #  -2 & 0 & 0 & 0 & -1 & 0 & 1 & 1 & 0 & 0 & 0 & 0 & 0
    # \end{tabular}\right) \cdot \begin{pmatrix}
    # p_2 \cdot q_1 \\
    # p_2 \cdot q_2 \\
    # p_2 \cdot q_3 \\
    # \hline
    # p_3^2 \\
    # p_1^2 \\
    # p_4^2 \\
    # \hline
    # p_2^2 \\
    # \hline
    # q_1^2 \\
    # q_2^2 \\
    # q_3^2 \\
    # q_1 \cdot q_2 \\
    # q_1 \cdot q_3 \\
    # q_2 \cdot q_3
    # \end{pmatrix} = 0
    # \f}
    # The momentum product vector consists of the return values of #findRelevantMomentumProducts() together with internal (non-loop) momentum A squared,
    # but internal (non-loop) momentum A times internal (non-loop) momentum B (A != B) are excluded.
    #
    # The higher a product is in the vector, the higher is its priority to find a replacement for it in terms of the other products.
    # Here, we decide to prioritize external momenta times internal momenta the most, then come internal times internal momenta.
    #
    # @param A momentumMatrix in reduced row echelon form without zero rows and duplicate rows (a row with only two entries, relating both momenta 1:1)
    # @param p List of all momentum names (\p momentumNames)
    # @param pMix List of products of loop momentum times external momentum, for example <code>[["p3", "q1"], ["p3", "q2"]]</code> (meaning \f$p_3\cdot q_1\f$ and \f$p_3\cdot q_2\f$). Found by #findRelevantMomentumProducts().
    # @param pLoop List of products of loop momentum times loop momentum, for example <code>[["p3", "p5"], ["p3", "p3"], ["p5", "p5"]]</code> (meaning \f$p_3\cdot p_5\f$, \f$p_3^2\f$ and \f$p_5^2\f$). Found by #findRelevantMomentumProducts().
    # @param pExternal List of products of external momentum times external momentum, for example <code>[["q1", "q1"], ["q1", "q2"], ["q2", "q2"]]</code> (meaning \f$q_1^2\f$, \f$q_1\cdot q_2\f$ and \f$q_2^2\f$). Found by #findRelevantMomentumProducts().
    # @param lineMasses Dict of internal momentum names corresponding to the mass of the line, as generated by #generateLineMassesDict()
    # \return A tuple of
    # * Momentum product matrix ("squared momentum matrix")
    # * List of momentum products, spanning the linear equation system of the momentum product matrix. E.g. <code>[["p2", "q1"], ["p2", "q2"], ["p2", "q3"],...]</code>
    @staticmethod
    def squaredMomentumMatrixAndBasisVector(A, p, pMix, pLoop, pExternal, lineMasses):
        # handle internal momenta separately, because we want to replace them
        B = []
        P = pMix + pLoop + pExternal

        # list of internal, non-loop momenta
        pInternal = []

        for i, row in enumerate(A):
            Brow = []
            if 1 in row:
                reducibleMomentumIndex = row.index(1)
                # save the internal momentum squared which belongs to this row of B
                pInternal += [[p[reducibleMomentumIndex],p[reducibleMomentumIndex]]]

                # build rows of B by multiplying the two entries of A of list P
                for Pentry in P:
                    # multiply the square of different entries by 2 to achieve (a+b)^2 = a^2 + 2ab + b^2
                    if Pentry[0] != Pentry[1]:
                        Brow.append(2 * row[p.index(Pentry[0])] * row[p.index(Pentry[1])])
                    else:
                        Brow.append(row[p.index(Pentry[0])] * row[p.index(Pentry[1])])
                B.append(Brow)
            else:
                logging.error("Cannot resolve line momentum in row" + str(i) + "!")
                exit(1)

        # switch rows and entries of B and P such that massive internal momenta are in the back.
        # This ensures that massless lines are more likely completely replaced by massive lines,
        # which are always included to scalar topologies (except two massive lines are completely equal).
        switchIndex = 0
        for i, pi in enumerate(pInternal.copy()):
            if lineMasses[pi[0]] == 0:
                pInternal[switchIndex], pInternal[i] = pInternal[i], pInternal[switchIndex]
                B[switchIndex], B[i] = B[i], B[switchIndex]
                switchIndex += 1

        # now we add internal momenta separately
        P = pMix + pInternal + pLoop + pExternal

        # insert negative unity matrix in B for resolution of the internal momenta
        for i in range(len(pInternal)):
            basisVec = [0 for n in range(len(pInternal))]
            basisVec[i] = -1
            # insert entries of basisVec between entries pMix and pLoop + pExternal
            B[i] = B[i][:len(pMix)] + basisVec + B[i][len(pMix):]

        return sy.Matrix(B), P



    ## Calculate a dictionary of momentum products that have to be replaced by a linear combination of independent momentum products.
    # The relations are extracted from the momentum product matrix, calculated in #squaredMomentumMatrixAndBasisVector().
    #
    # Thus, this method is similar to #findRelevantMomentumProducts().
    #
    # Here, we do not differentiate between massive and massless lines yet.
    #
    # @param B momentum product matrix, calculated in #squaredMomentumMatrixAndBasisVector()
    # @param P List of momentum products, also calculated in #squaredMomentumMatrixAndBasisVector()
    # @param externalMomenta List of all externalMomentum names
    # \return A dict of replacement rules for non-minimal momentum products in terms of minimal momentum products. E.g. <code> {"p2.q1" : "q1.q1 - 2*q1.p2 + p2.p2"}</code>
    @staticmethod
    def getMomentumProductsDict(B, P, externalMomenta):
        # Since we have already the matrix B (which gives exactly these relations), this task reduces to resolving the entries in B to an equation
        # convert P entries [p1, p2] to string p1*p2
        PP = [Pentry[0] + "*" + Pentry[1] for Pentry in P]

        # bring B in reduced ecolon form
        B = B.rref()[0]
        logging.debug(B._format_str())

        momentumProducts = {}
        for rowIndex in range(B.rows):
            row = B.row(rowIndex).tolist()
            if not row == [0 for i in row]:
                # extract first non-zero entry of the row
                if 1 in row[0]:
                    reducibleMomentumIndex = row[0].index(1)

                    # suppress substitutions of external momenta only
                    if P[reducibleMomentumIndex][0] in externalMomenta and P[reducibleMomentumIndex][1] in externalMomenta:
                        logging.error("The columns in the momentum matrix must be changed!")
                        continue

                    # kick-out the specific entry
                    row[0][reducibleMomentumIndex] = 0
                    row = sy.Matrix(row)
                    # invert rest
                    row = -row

                    # get linear combinations
                    row = row * sy.Matrix(sy.symbols(" ".join(PP)))

                    # make momentum product dict FORM-readable
                    expr = re.sub(r'([a-zA-Z0-9]+)\*\*2', r'\1.\1', str(row.tolist()[0][0]))
                    expr = re.sub(r'(?=[a-z])([^+/ \*]+)\s*\*\s*(?=[a-z])([^+/ \*])', r'\1.\2', expr)

                    pp = re.sub(r'\*', r'.', PP[reducibleMomentumIndex])

                    logging.debug(" " + pp + " = " + expr)

                    momentumProducts[pp] = expr
                else:
                    logging.error("Momentum matrix not properly reduced (no 1's in rref)!")
                    exit(1)

        return momentumProducts


    ## Construct momentum products that may appear as eikonal propagators
    # Eikonal propagators consist of products of different momenta of the topology. This function finds all of them which
    # a) are not momenta squared and
    # b) cannot directly be replaced by another momentum product
    #
    # @param internalMomenta List of all independent internal momenta as strings (i.e. momenta which cannot be replaced by another one)
    # @param externalMomenta List of all irreducible external momenta as strings
    # @param externalEikonal Boolean value, if True return only products of internal and external momenta, if False return only products of internal momenta
    # \return List of momentum products as sublist of two momentum names. E.g. [["p1","p2"], ["p1","p3"], ["p2","p3"]]
    @staticmethod
    def getEikonalProducts(independentMomenta, externalMomenta, externalEikonal):
        # Build the products which have at least one internal momentum
        momentumProducts = []
        for i in range(0,len(independentMomenta)):
            # For external eikonal option, multiply only external momenta
            if externalEikonal:
                for q in sorted(externalMomenta):
                    momentumProducts.append( [independentMomenta[i], q] )
            # For internal eikonal option, multiply other internal momenta without double-counting
            else:
                for j in range(i+1,len(independentMomenta)):
                    momentumProducts.append( [independentMomenta[i], independentMomenta[j]] )

        return momentumProducts


    ## Routine to find replacement of (irreducible) internal momenta multiplied with external momenta in a single call.
    # First, build a vector of momentum products P = [["p1","p1"], ["p2","p2"], ["p3","q1"], ["p3","q2"] ...]
    # together with an appropriate matrix B which gives the equations between those, such that
    #   B P = 0
    # with the method #squaredMomentumMatrixAndBasisVector().
    #
    # Then, extract from B the momentum relations using #getMomentumProductsDict().
    # Here is also defined how the scalar integral is composed, i.e. the rules of what are the entries of #topoFactors, #topoFactors and #topoFactorSigns.
    # We also apply modules.config.Conf::kinematicRelations to #analyticTopologyExpression at the end.
    #
    # To #topoFactors we add:
    # * Scalar propagators of massive lines that are not substituted by #equalMomentumSquaredProducts: `s1m1` for \f$ s1m1 = \pm \frac{1}{M_1^2 - p_1^2} \f$
    # * The inverse squares of massive line momenta that are not substituted by #equalMomentumSquaredProducts (only with the option #extendAnalyticalTopos): `1/p1.p1`
    # * Squares of massless line momenta that are not substituted by #equalMomentumSquaredProducts: `1/p2.p2`
    # * Products of momenta that cannot be substituted by other momentum products (i.e. right-hand side elements of #momentumProducts): `1/p2.p3` unless they do not appear in the numerator but as real denominators. If these eikonal propagators are allowed in the conf all combinations of momentum products (which include internal momenta) are kept.
    # The signs depends on the choice of Config.conf.euclideanMomenta.
    # @param A momentumMatrix in reduced row echelon form without zero rows and duplicate rows (a row with only two entries, relating both momenta 1:1)
    # @param p List of all momentum names (\p momentumNames)
    def calculateMomentumProducts(self, A, p):
        logging.debug("----------------------  start of calculateMomentumProducts()  ----------------------")

        # first construct multiplications of loop and external momenta
        pMix, pLoop, pExternal = self.findRelevantMomentumProducts(self.loopMomenta, self.irreducibleExternalMomenta)

        # square the rows of A (without non-loop and external momenta) to build B
        B, P = self.squaredMomentumMatrixAndBasisVector(A, p, pMix, pLoop, pExternal, self.lineMasses)

        logging.debug("Momentum product basis:\n" + str(P))
        logging.debug("Matrix B with internal momenta:\n" + B._format_str())

        # now make the replacements
        self.momentumProducts = self.getMomentumProductsDict(B, P, self.externalMomenta)

        # find products that can be directly replaced by others
        self.equalMomentumSquaredProducts = self.findEqualMomentumSquaredProducts(self.momentumProducts, self.lineMasses)

        # counting variable to give new propagators (made from momentum products) a new index
        ancillaryMomentumIndex = 0

        # Find momenta which can be replaced by other ones
        self.equalMomentumDict = self.findEqualMomenta(self.momentumDict, self.lineMasses)

        # Relevant external momenta for eikonal propagators are either a specific list of momenta or all irreducible external momenta
        if len(self.conf.topoEikonalExternal) > 1:
            relevantEikonalExtMom = self.conf.topoEikonalExternal[1:]
        else:
            relevantEikonalExtMom = self.irreducibleExternalMomenta

        # Add eikonal momentum products to topology
        eikonalProducts = []
        if self.conf.topoEikonalInternal or self.conf.topoEikonalExternal[0]:
            # Get all internal momenta which are not direct replacable by another momentum (ignoring signs).
            independentMomenta = [mom for mom in p if mom not in  self.equalMomentumDict and mom not in self.externalMomenta]
            # Sort momenta numerically correct
            independentMomenta.sort(key=lambda x: (len(x),x))
            # Internal times internal momenta
            if self.conf.topoEikonalInternal:
                eikonalProducts += self.getEikonalProducts(independentMomenta, self.irreducibleExternalMomenta, externalEikonal=False)
            # Internal times external momenta
            if self.conf.topoEikonalExternal[0]:
                eikonalProducts += self.getEikonalProducts(independentMomenta, relevantEikonalExtMom, externalEikonal=True)

        # take only eikonal products which are are not yet in P
        eikonalProducts = [pp for pp in eikonalProducts if set(pp) not in [set(el) for el in P]]

        # extract non-further reducible scalar expressions from matrix B as arguments for the topology function
        for pp in P[::-1] + eikonalProducts:
            # discard pp if...
            # a) it can be one-to-one replaced by another product (#equalMomentumSquaredProducts), or
            isReplacedDirectly = (pp[0] + "." + pp[1]) in self.equalMomentumSquaredProducts.keys()
            # b) it is a real numerator that can be substituted, i.e. it's not a square product and not in #equalMomentumSquaredProducts (eikonal propgators are added separately later), or
            isReplacedNumerator = ((pp[0] + "." + pp[1]) in self.momentumProducts.keys() and (pp[0] != pp[1]))
            # Keep all products of internal and external momenta if the respective eikonal propagators are allowed
            isReplacedNumerator = isReplacedNumerator and not (self.conf.topoEikonalExternal[0] and (pp[0] in relevantEikonalExtMom or pp[1] in relevantEikonalExtMom))
            # Keep all products of internal momenta if the respective eikonal propagators are allowed
            isReplacedNumerator = isReplacedNumerator and not (self.conf.topoEikonalInternal and (pp[0] not in self.irreducibleExternalMomenta and pp[1] not in self.irreducibleExternalMomenta))
            # c) it is external times external momentum
            isPureExternal = pp[0] in self.irreducibleExternalMomenta and pp[1] in self.irreducibleExternalMomenta

            if not isReplacedDirectly and not isReplacedNumerator and not isPureExternal:
                # massive momenta must be inserted as simj = 1/(Mj^2 - pi^2)
                if pp[0] == pp[1] and self.lineMasses[pp[0]] != 0:
                    self.topoFactors.append(self.simj(pp[0]))
                    self.topoFactorSigns.append("")

                    # insert momentum definitions for analyticTopologyExpression
                    if pp[0] in self.momentumDict:
                        analyticProduct = "(" + self.momentumDict[pp[0]] + ")^2"
                    else:
                        analyticProduct = pp[0] + "^2"

                    if self.conf.euclideanMomenta: # 1/(pi^2 + Mj^2)
                        self.analyticTopologyExpression.append(
                            str(sy.sympify(analyticProduct + "+" + self.lineMasses[pp[0]] + "^2")).replace("**", "^")
                        )
                    else: # 1/(Mj^2 - pi^2)
                        self.analyticTopologyExpression.append(
                            str(sy.sympify("-(" + analyticProduct + ")+" + self.lineMasses[pp[0]] + "^2")).replace("**", "^")
                        )

                    # Check if another massless momentum product is directly replaced by the current momentum product, which is massive.
                    masslessMomentumMapsDirectlyOnMassive = False
                    if (pp[0] + "." + pp[1]) in self.equalMomentumSquaredProducts.values():
                        for mom, momMass in self.lineMasses.items():
                            if momMass == 0 and self.equalMomentumSquaredProducts.get(mom + "." + mom, False):
                                masslessMomentumMapsDirectlyOnMassive = True

                    # Break up if no copy of the massive momentum shall appear in the analytical topology.
                    # This excepts the case that a massless momentum is directly mapped on this massive one. Then, the momentum should occur as well.
                    if (not self.conf.extendAnalyticalTopos) and (not masslessMomentumMapsDirectlyOnMassive):
                        continue

                # In case we do not want simple products of momenta, replace them by new denominators
                if self.conf.topoCompleteMomentumProducts and pp[0] != pp[1]:
                    # create new momentum that equals the sum of both momenta
                    ancillaryMomentumIndex += 1
                    pNew = F"P{ancillaryMomentumIndex}"
                    self.momentumProducts[F"{pp[0]}.{pp[1]}"] = F"({pNew}.{pNew} - {pp[0]}.{pp[0]} - {pp[1]}.{pp[1]})/2"
                    # add the new momentum squared to the scalar topology function instead of the original momentum product
                    self.momentumDict[pNew] = F"{pp[0]} + {pp[1]}"
                    pp[0], pp[1] = pNew, pNew


                # the rest can be appended as simple multiplication
                # Add also the squares of momenta of massive lines to handle special propagator cases (e.g. gluon mass terms)
                self.topoFactors.append("1/" + pp[0] + "." + pp[1])
                p1 = pp[0]
                p2 = pp[1]

                if p1 in self.momentumDict:
                    p1 = self.momentumDict[p1]
                if p2 in self.momentumDict:
                    p2 = self.momentumDict[p2]

                if self.conf.euclideanMomenta: # 1/(p1*p2)
                    self.topoFactorSigns.append("")
                    self.analyticTopologyExpression.append(
                        str(sy.sympify("(" + p1 + ")*(" + p2 + ")").factor()).replace("**", "^")
                    )
                else: # 1/(-p1*p2)
                    self.topoFactorSigns.append("-")
                    self.analyticTopologyExpression.append(
                        str(sy.sympify("-(" + p1 + ")*(" + p2 + ")").factor()).replace("**", "^")
                    )

        # Apply the kinematic relations to the analyticTopologyExpression
        if len(self.conf.kinematicRelations) != 0:
            for i,expr in enumerate(self.analyticTopologyExpression):
                # Expand all terms
                term = sy.sympify(expr).expand()
                # Try to translate the relation to sympy and apply the kinematic relations one by one
                for lhs,rhs in self.conf.kinematicRelations.items():
                    try:
                        lhs = lhs.replace(".","*")
                        lhs = sy.sympify(lhs)
                    except:
                        logging.warning(F"{lhs} in the kinematic relation cannot be interpreted by sympy!")
                        exit(1)
                    try:
                        rhs = rhs.replace(".","*")
                        rhs = sy.sympify(rhs)
                    except:
                        logging.warning(F"{rhs} in the kinematic relation cannot be interpreted by sympy!")
                        exit(1)

                    term = term.subs(lhs,rhs)

                # Replace substituted term
                term = str(term).replace("**","^")
                self.analyticTopologyExpression[i] = str(term)

        logging.debug("----------------------  end of calculateMomentumProducts()  ----------------------\n")


    ## Calculate all momentum relations of diagram and find loop momenta.
    # First, calculate the momentumMatrix using #createMomentumMatrix().
    # With this, call #extractMomentumDictFromEchelonMatrix() and fill #momentumDict, #loopMomenta, #irreducibleExternalMomenta, and #externalMomenta.
    #
    # This method can be called directly after instantiation and #generateLineMassesDict()
    def calculateMomentumRelations(self):
        logging.debug("----------------------  start of calculateMomentumRelations()  ----------------------")

        # create a list of momentum names
        momentumNames = []
        # sort by massive lines to make sure the massive momenta are identified as loop momenta
        for momentum in self.diagram.internalMomenta:
            # is particle (or antiparticle) massless?
            if self.lineMasses[momentum[0]] == 0:
                momentumNames = [momentum[0]] + momentumNames
            # massive? put it to the end
            else:
                momentumNames = momentumNames + [momentum[0]]

        # add external momenta in inverted order to ensure that the last momentum is always in terms of the first ones
        externalMomenta = self.diagram.externalMomenta.copy()
        externalMomenta.reverse()
        externalMomenta = [re.sub(r'-*', r'', momentum[0]) for momentum in externalMomenta]
        # use momentum renaming from the conf file for substitution and restriction of external momenta
        for q,qSub in self.conf.externalMomenta.items():
            if q in externalMomenta:
                externalMomenta.append(qSub)
                del externalMomenta[externalMomenta.index(q)]


        # remove duplicates or kill last momentum for momentum conversation
        shortExternalMomentumNames = list(dict.fromkeys([re.sub(r'-*', '', q) for q in externalMomenta]))

        momentumNames = momentumNames + shortExternalMomentumNames

        logging.debug("Momentum names: " + str(momentumNames))

        # now build the vertex-momentum matrix
        momentumMatrix = self.createMomentumMatrix(self.diagram.topologicalVertexList, momentumNames, self.conf.externalMomenta)

        # add momentum conservation to last row
        momConservationRow = [0 for i in momentumNames]
        for q in externalMomenta:
            momConservationRow[momentumNames.index(re.sub(r'-*', '', q))] += eval(re.sub(r'[^-]*$', '', q) + "1")
        momentumMatrix.append(momConservationRow)
        # transform matrix into a sympy.Matrix. This framework is pretty handy since it has a large range of tools
        momentumMatrix = sy.Matrix(momentumMatrix)

        logging.debug("Momentum matrix before rref : " + momentumMatrix._format_str())
        # bring matrix into reduced echelon form. This is the simplest, minimal form for momentum replacement
        momentumMatrix = momentumMatrix.rref()[0]
        logging.debug("Momentum matrix after rref : " + momentumMatrix._format_str())

        momentumMatrix = momentumMatrix.tolist()
        # remove momentum conservation row
        if momConservationRow in momentumMatrix:
            momentumMatrix.remove(momConservationRow)

        # delete empty lines
        zeroRow = [0 for i in momentumNames]
        while zeroRow in momentumMatrix:
            momentumMatrix.remove(zeroRow)

        momentumMatrix = sy.Matrix(momentumMatrix)

        # now extract the replacements
        self.momentumDict = self.extractMomentumDictFromEchelonMatrix(momentumMatrix, momentumNames)

        # extract loop momenta from the matrix
        self.loopMomenta = self.findLoopMomenta(momentumMatrix, momentumNames, shortExternalMomentumNames)

        logging.debug("loop momenta: " + str(self.loopMomenta))

        # save irreducible external momenta
        self.irreducibleExternalMomenta = shortExternalMomentumNames[1:]

        self.externalMomenta = shortExternalMomentumNames

        logging.debug("Momentum names at the end of calculateMomentumRelations():\n" + str(momentumNames))
        logging.debug("Momentum matrix at the end of calculateMomentumRelations():\n" + str(momentumMatrix._format_str()))
        logging.debug("----------------------  end of calculateMomentumRelations()  ----------------------\n")

        # calculate momentum multiplications
        self.calculateMomentumProducts(A=momentumMatrix.tolist(), p=momentumNames)


    ## Find direct relations between momentum squares. Used to compute #equalMomentumSquaredProducts.
    #
    # This method enables to directly identify squared momenta for specific conditions.
    # If two massive lines are identical, one of them is thus assigned to the other one.
    # Equal squares between momenta of massless and massive lines are mapped on the momenta of the massive line, since the squares of momenta of massive lines are added to the scalar topology (see #calculateMomentumProducts()) in addition to the real massive propagator. This however is not dramatic since the partial fractioning reduces them afterwards anyway.
    # Massless lines that get completely mapped on a single external momentum are also replaced by the latter.
    #
    # @param momentumProductDict A dict defining the composition of products of internal, external and loop momenta in terms of irreducible ones as found by #getMomentumProductsDict()
    # @param lineMasses Dict of internal momentum names corresponding to the mass of the line, as generated by #generateLineMassesDict()
    # \return Dict of momentum squares that are equal. E.g. `{"p1.p1": "p2.p2", "p4.p4": "p2.p2"}`
    def findEqualMomentumSquaredProducts(self, momentumProductDict, lineMasses):
        squarematch = re.compile(r'([a-zA-Z]+[0-9]*)\.\1')

        # extract squares of momentumProductDict keys
        squares = {ppOrigin: ppSub for ppOrigin,ppSub in momentumProductDict.items() if len(squarematch.findall(ppOrigin))}

        substitutions = {}

        # add squares that are directly linked to a single other square
        for ppOrigin,ppSub in squares.items():
            if re.match(r'^\s*[a-zA-Z]+[0-9]*\s*\.\s*[a-zA-Z]+[0-9]*\s*$', ppSub):
                # external momentum is replaced by an external momentum
                if (squarematch.findall(ppSub)[0] not in lineMasses.keys()) and (squarematch.findall(ppOrigin)[0] not in lineMasses.keys()):
                    substitutions[ppOrigin] = ppSub
                # massless internal momentum corresponds to external one
                if (squarematch.findall(ppSub)[0] not in lineMasses.keys()) and (squarematch.findall(ppOrigin)[0] in lineMasses.keys() and lineMasses[squarematch.findall(ppOrigin)[0]] == 0):
                    substitutions[ppOrigin] = ppSub
                # both are internal momenta
                elif (squarematch.findall(ppSub)[0] in lineMasses.keys()) and (squarematch.findall(ppOrigin)[0] in lineMasses.keys()):
                    # both have the same mass
                    if lineMasses[squarematch.findall(ppSub)[0]] == lineMasses[squarematch.findall(ppOrigin)[0]]:
                        substitutions[ppOrigin] = ppSub
                    # The original momentum is massless
                    elif lineMasses[squarematch.findall(ppOrigin)[0]] == 0:
                        substitutions[ppOrigin] = ppSub

        # check for equality between other internal momenta
        for pp1, ppSub1 in sorted(squares.items(), key=lambda x: squarematch.findall(x[0])[0],reverse=True):
            if pp1 not in substitutions.keys() and pp1 not in substitutions.values():
                for pp2, ppSub2 in squares.items():
                    # check if pp2 can be mapped on pp1
                    if (pp1 != pp2) \
                    and (pp2 not in substitutions.keys()) \
                    and (squarematch.findall(pp1)[0] in lineMasses.keys()) and (squarematch.findall(pp2)[0] in lineMasses.keys()) \
                    and (lineMasses[squarematch.findall(pp1)[0]] == lineMasses[squarematch.findall(pp2)[0]] or lineMasses[squarematch.findall(pp1)[0]] == 0) \
                    and sy.sympify(ppSub1.replace(".", "*")) == sy.sympify(ppSub2.replace(".", "*")):
                        substitutions[pp1] = pp2
                        break

        return substitutions


    ## Find all momenta which can be replaced by another one with same or differing sign.
    # The momenta we are looking for can be directly replaced by another one. E.g. |p1| = |p3| (external momenta are also allowed on the rhs).
    # To choose the right-hand side, we pick a representative momentum.
    # This representative is the first massive momentum we find, or the first momentum whatsoever if all momentum lines are massless.
    #
    # @param momentumDict Dict that gives the expression of the momentum in terms of other ones. E.g. {"p1": "-p2+q1"}. This is the same as #momentumDict
    # @param lineMasses Dict of internal momentum names corresponding to the mass of the line. Same as #lineMasses
    # \return Dict of momenta replacing others, E.g. {"p1":"p2", "p3":"-p4", "p7":"-q2"}
    @staticmethod
    def findEqualMomenta(momentumDict, lineMasses):
        # Pre-compiled regex function that matches single replacable momenta. I.e. momenta which can be directly replaced by another one (ignoring signs)
        momMatch = re.compile(r'^\s*(-*)\s*([a-zA-Z0-9]+)\s*$')

        # Final dict with matched momenta
        equalMomentaDict = {}

        # Dict with [momenta, relative sign] values which have the same absolute values with their replacement value as keys. I.e. {"p5-q1": [["p1",1]], "p5+q1-q2": [["p2",1],["p3",-1],["p4",1]]} means p2 = -p3 = p4
        sameMomentaPool = {}

        # Check for all momenta in momentumDict
        for lhs, rhs in momentumDict.items():
            # First, check if the momentum can be directly replaced by another one (ignoring signs)
            matchRes = momMatch.findall(rhs)
            if len(matchRes)>0:
                equalMomentaDict[lhs] = "".join(matchRes[0])
                continue

            # If the direct replacement does not work, put the momentum in a pool according to its righthand-side
            # +rhs
            rhsP = str(sy.sympify(rhs).expand())
            # -rhs
            rhsM = str((-sy.sympify(rhs)).expand())
            # Check if either +rhs or -rhs was used as a key before, if true, add momentum and relative sign to pool
            if rhsP in sameMomentaPool:
                sameMomentaPool[rhsP].append([lhs,1])
            elif rhsM in sameMomentaPool:
                sameMomentaPool[rhsM].append([lhs,-1])
            # If rhs is new, open new pool
            else:
                sameMomentaPool[rhsP] = [[lhs,1]]

        # Checkout all momentum pools and set the replacement to a representative
        for equalMomenta in sameMomentaPool.values():
            # Sort momenta numerically
            equalMomenta.sort(key=lambda x: (len(x[0]),x[0]))
            # List of massive momenta in the pool
            massiveMomenta = [ p for p in equalMomenta if p[0] in lineMasses and lineMasses[p[0]] != 0 ]
            # Choose representative momentum to be the first one, if all momentum lines are massless
            if len(massiveMomenta) == 0:
                for p in equalMomenta[1:]:
                    # Same relative sign
                    if p[1] == equalMomenta[0][1]:
                        equalMomentaDict[p[0]] = equalMomenta[0][0]
                    # Different relative sign
                    else:
                        equalMomentaDict[p[0]] = "-" + equalMomenta[0][0]

            # Choose representative momentum to be the first massive one, otherwise
            else:
                for p in [ mom for mom in equalMomenta if mom != massiveMomenta[0] ]:
                    # Same relative sign
                    if p[1] == massiveMomenta[0][1]:
                        equalMomentaDict[p[0]] = massiveMomenta[0][0]
                    # Different relative sign
                    else:
                        equalMomentaDict[p[0]] = "-" + massiveMomenta[0][0]

        return equalMomentaDict


    ## Format a massive line momentum p3 with the mass M2 to "s3m2"
    # @param p momentum name pi with corresponding mass Mj
    # \return String of the form "simj"
    def simj(self, p):
        # index of mass
        if self.lineMasses != {}:
            self.generateLineMassesDict()
        mIndex = self.lineMasses[p][1:]

        return "s" + p[1:] + "m" + mIndex
