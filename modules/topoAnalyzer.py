## @package topoAnalyzer
# The class [TopologyAnalyzer](@ref modules.topoAnalyzer.TopologyAnalyzer) bundles many topology analysis tools.
# It provides a set of methods needed for the topology minimizations (see topoMinimizer) and other interesting properties from computational point of view.
#
# An additional ancillary class modules.topoAnalyzer.BridgeFinder can extract all bridges (Line's of a modules.diagram.Diagram that when cut, one would get two separated diagrams) of a modules.diagram.Diagram.
#
# With modules.topoAnalyzer.TopologyAnalyzer, we have the possibility to extract the following properties of a modules.diagram.Diagram instance:
# * create the Laplacian matrix of the topology (#modules.topoAnalyzer.TopologyAnalyzer::constructLaplacian())
# * calculate the Kirchhoff-polynomial (#modules.topoAnalyzer.TopologyAnalyzer::constructKirchhoffPolynom())
# * calculate the Symanzik-polynonials U and F (#modules.topoAnalyzer.TopologyAnalyzer::constructSymanzikPolynomials() and #modules.topoAnalyzer.TopologyAnalyzer::constructSymanzikPolynomials2())
# * canonically order the internal lines of the Symanzik-Polynomials (#modules.topoAnalyzer.TopologyAnalyzer::canonicallyOrderSymanzikPolynomials())
# * contract modules.diagram.Line's of the diagram and adjust every other property accordingly (especially U and F without re-calculating them) (#modules.topoAnalyzer.TopologyAnalyzer::deleteLines())
# * find bridges (#modules.topoAnalyzer.TopologyAnalyzer::findBridges() using modules.topoAnalyzer.BridgeFinder)
# * find the so-called Nickel-index, as described in [[3]](@ref refs) (#modules.topoAnalyzer.TopologyAnalyzer::findNickelIndex())
#
#
# To use this class, one has to initialize it with a diagram that might potentially be changed! So use a copy of it if you want to keep the original diagram.
# To do a full analysis and  (like done in #modules.topoMinimizer.TopoMinimizer::analyzeDiagram()), the following methods must be called after initialization:
# <code>
# #modules.topoAnalyzer.TopologyAnalyzer::constructSymanzikPolynomials() or topoAnalyzer.constructSymanzikPolynomials2()\n
# #modules.topoAnalyzer.TopologyAnalyzer::simplifyExternalMomentaInF()\n
# #modules.topoAnalyzer.TopologyAnalyzer::canonicallyOrderSymanzikPolynomials()
# </code>
#
# Alternatively, one can compute the Nickel-index and the corresponding line momentum order without this initiation (i.e. without #modules.topoMinimizer.TopoMinimizer::analyzeDiagram())
# Because comparing this beforehand is way more efficient than computing the Symanzik polynomials for all diagrams
#
# The methods to calculate the Symanzik- and Kirchhoff-Polynomials are given by [[1]](@ref refs).
# The canonical ordering and the Nickel index are based on [[2]](@ref refs) and [[3]](@ref refs).
#
# To illustrate the working of all described methods, we use the following diagram as a working example:
# \anchor box-topo
# \image html box-topo.png "Example box diagram" height=300px
# \image latex box-topo.eps "Example box diagram"
#
# References: \anchor refs
# -----------
# * [[1]](@ref refs) C. Bogner and S. Weinzierl, Int. J. of mod. Phys. A, Vol. 25 No. 13 (2010) 2585-2618 (DOI: 10.1142/S0217751X10049438)
# * [[2]](@ref refs) A. Pak, hep-ph/1111.0868v1
# * [[3]](@ref refs) D. Batkovich, Yu. Kirienko, M. Kompaniets and S. Novikov, hep-ph/1409.8227v1
# * [[4]](@ref refs) https://cp-algorithms.com/graph/bridge-searching.html (accessed 13.09.2019)
# * [[5]](@ref refs) https://cp-algorithms.com/graph/depth-first-search.html (accessed 13.09.2019)
# * [[6]](@ref refs) J. Hoff, Methods for multiloop calculations and Higgs boson production at the LHC, Karlsruhe (2015)


import logging
from modules.diagram import Vertex, Line
from modules.momentumAssigner import MomentumAssigner
import sympy as sy
import re
import copy
import modules.cython.nickel as nickel


## Main class of the Diagram analysis toolkit.
# For an overview of all functionalities, see topoAnalyzer.
class TopologyAnalyzer:
    ## Constructor
    # @param diagram The modules.diagram.Diagram instance, we want to analyze
    # @param particleMasses #modules.config.Conf::mass dict describing the masses of the particles
    # @param externalMomentaBoundaries #modules.config.Conf::externalMomenta dict that dictates how the external momenta shall be substituted
    def __init__(self, diagram, externalMomentaBoundaries = {}):
        ## Reference to the diagram we want to analyze.
        # Most methods are non-destructive (i.e. they do not change the \p diagram instance), but some of them are, namely #wrapExternalMomenta() and #deleteLines() (as well as #contractLines())
        self.diagram = diagram
        ## An ancillary field needed for #constructSymanzikPolynomials()
        self.externalEdgeCoeffs = []
        ## An ancillary field needed for #constructSymanzikPolynomials()
        self.internalEdgeCoeffs = []
        ## A dict describing the mass of each massive line (its ordered index) of the form <code>{1: "M1", 6: "M2"}</code>.
        # Calculated in #constructLaplacian() as an ancillary object
        self.lineMasses = {}
        ## First Symanzik polynomial from #constructSymanzikPolynomials() or #constructSymanzikPolynomials2()
        self.U = ""
        ## Second Symanzik polynomial from #constructSymanzikPolynomials() or #constructSymanzikPolynomials2()
        self.F = ""
        ## Dict of all masses and which lines (more precise: their ordered index) are assigned that mass. Can be calculated with #getMassInLines()
        # Example: <code>{"M1": [1,4], "M2": [[2]](@ref refs), 0: [3,5,6,7]}</code>
        self.massInLines = {}
        ## From #modules.config.Conf::externalMomenta : {"q1" : "q", "q2" : "-q", "q3" : "q", "q4" : "-q"}
        self.externalMomentaBoundaries = externalMomentaBoundaries
        ## This List describes the lexicographic ordering of the Line's due to #canonicallyOrderSymanzikPolynomials().
        # The parameter x1,x2,...,xN (corresponding to each non-self-loop Line) in #F are renamed in such a way, that F has the lexicographic maximal weight.
        # The list gives a renaming rule of to get to this ordering.
        # Example: <code>[1,4,3,2] => x1->x1, x2->x4, x3->x3, x4->x2</code>\n
        # (i.e. x1 is associated with Line::number 1, x4 with Line 2, x3 with Line 3 and x2 with Line 4)\n
        # To get not confused with the Line::number, we call the xi index corresponding to a Line its "ordered index".
        self.permutations = list(range(1,len(self.diagram.internalMomenta)+1))
        ## List of lexicographic Line-symmetries.
        # If in #canonicallyOrderSymanzikPolynomials() multiple Line (or xi)-renamings have the same lexicographic weight, it is one non-deterministic chosen.
        # To keep track of the other possible namings that lead to the same maximal lexicographic weight, we save them here.
        # Example: [[1,2], [1,3]] means the renaming of x1 <=> x2 and/or x1 <=> x3 in #U and #F leads to equal lexicographic weight.\n
        # Note: One could also extract real symmetries similarly with stronger constrains than just lexicographical equality, e.g. #F must not change under the renaming
        self.lineSymmetries = []
        ## Dict of renamings after the cuts of #deleteLines(). This is needed to retrieve the ordered indices without gaps.
        # E.g. If we have 4 Lines and cut the Line with the ordered index 2 (i.e. the Line corresponds to x2), the line that used to be associated with x4 becomes x2 (rename x4 to x2).
        # We have then <code>{4:2}</code>.
        self.cutReplacements = {}
        ## List the Line::numbers (index+1 of #permutations, not ordered indices!) that were cut by #deleteLines()
        self.cuts = []
        ## The Nickel-index is a unique marker for a diagram topology [[3]](@ref refs)
        # It is computed in #findNickelIndex()
        self.nickelIndex = ""
        ## Define the numeration of the Nickel-index in #findNickelIndex()
        self.nickelNumeration = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                                 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
        ## Renaming list of vertices that is minimal according to #findNickelIndex()
        # This List is similar to #permutations but with vertices.
        # I.e. [2,3,1] means the minimal #nickelIndex is achieved by renaming vertex 1 to 2, vertex 2 to 3 and vertex 3 to 1.
        self.nickelVertexPermutations = list(range(1,len(self.diagram.topologicalVertexList)+1))
        ## Canonical order of internal line momenta according to the Nickel-index
        # This list is generated together with #nickelIndex and #nickelOrderedExternalMomenta in #findNickelIndex()
        self.nickelOrderedMomenta = []
        ## Canonical order of external momenta according to the Nickel-index
        # This list is generated together with #nickelIndex and #nickelOrderedMomenta in #findNickelIndex()
        self.nickelOrderedExternalMomenta = []
        ## The line numbers of the diagram before computing the Nickel index are not necessarily of the form 1,...,n.
        # However, the findNickelIndex routine in cython needs them in the form 1,...,n.
        # self.goodLineNumbers is a list of the form [[2,1],[4,2],[5,3],[7,4]] if the original line numbers were [2,4,5,7].
        # It is computed in #findNickelIndex() and is needed to properly map the sets of momenta of two diagrams mapped with the Nickel index onto each other.
        self.goodLineNumbers = []


    ## Simple debug method that prints the actual states of all fields of TopologyAnalyzer
    def __str__(self):
        string = "TopoAnalyzer{"
        string += "\tnumber: " + str(self.diagram.diagramNumber)
        string += "\ttopologicalVertexList: " + str([{v.number : [(l.momentum, l.start, l.end) for l in v.lines]} for v in self.diagram.topologicalVertexList])
        string += "\texternalEdgeCoeffs: " + str(self.externalEdgeCoeffs)
        string += "\tinternalEdgeCoeffs: " + str(self.internalEdgeCoeffs)
        string += "\tlineMasses: " + str(self.lineMasses)
        string += "\tU: " + str(self.U)
        string += "\tF: " + str(self.F)
        string += "\tmassInLines: " + str(self.massInLines)
        string += "\tlineSymmetries: " + str(self.lineSymmetries)
        string += "\tpermutations: " + str(self.permutations)
        string += "\tcutReplacements: " + str(self.cutReplacements)
        string += "\tcuts: " + str(self.cuts)
        string += "}"

        return string


    ## Fill #massInLines with the Diagram::topologicalLineList of TopologyAnalyzer::diagram
    def getMassInLines(self):
        self.massInLines = {}

        # check the mass of every internal line
        for l in self.diagram.topologicalLineList:
            if not l.external:
                if l.mass not in self.massInLines:
                    self.massInLines[l.mass] = []
                # save not the line number, but the x-index
                self.massInLines[l.mass].append(self.permutations[l.number-1])

        # replace the massless lines by a 0-key
        if "" in self.massInLines:
            self.massInLines[0] = self.massInLines[""]
            del self.massInLines[""]

        return self.massInLines


    ## Calculate the Laplacian matrix of the TopologyAnalyzer::diagram either with external Line's connected to each other or not.
    # The special case in which all external lines are connected is needed to calculate the W polynomial from which we can extract #U and #F in #constructSymanzikPolynomials().
    # The Laplacian \f$L\f$ of a graph \f$G\f$ is defined in eq. (35) of [[1]](@ref refs) as a specific Adjacency matrix describing relations between vertices (rows and columns) in the folowing way:
    #
    # \f{align*}
    # L_{ij} =
    # \begin{cases}
    # \sum_k x_k, & \text{if \ $i=j$ \ and Line with number \ $k$ \ is attached to vertex with number \ $i$ \ and no self-loop} \\
    # -\sum_k x_k, & \text{if \ $i\neq j$ \ and Line with number \ $k$ \ connects the vertex with number \ $i$ \ with the vertex of number \ $j$}
    # \end{cases}
    # \f}
    #
    # For our [example diagram](@ref box-topo), we get:
    #
    # \f{align*}
    # L(G) = \begin{pmatrix}
    #   x_1 + x_2 & -x_1 & -x_2 & 0 \\
    #   -x_1 & x_1+x_3 & 0 & -x_3 \\
    #   -x_2 & 0 & x_2 + x_4 & -x_4 \\
    #   0 & -x_3 & -x_4 & x_3+x_4
    # \end{pmatrix}
    # \f}
    #
    # @param externalLinesConnected If set to true add all external lines to the Laplacian definition and connect them in a single additional vertex.
    # \return Sympy.Matrix representation of the Laplacian of TopologyAnalyzer::diagram
    def constructLaplacian(self, externalLinesConnected=False):
        # without the external lines connected, the laplacian has one dimension less
        if externalLinesConnected:
            matrixDim = len(self.diagram.topologicalVertexList)+1
        else:
            matrixDim = len(self.diagram.topologicalVertexList)

        laplacian = [0 for i in range(matrixDim)]
        externals = []
        # every vertex has its own row
        for vertex in self.diagram.topologicalVertexList:
            row = ["0" for i in range(matrixDim)]
            # every column stands for an adjacent vertex
            for line in vertex.lines:
                lineCopy = copy.deepcopy(line)

                # discard self-loops
                if lineCopy.start == lineCopy.end:
                    continue

                # external lines are treated specifically
                if lineCopy.external:
                    if externalLinesConnected:
                        # -q1 -> z1
                        x = "z" + re.sub(r'\-*q', r'', lineCopy.momentum)
                        lineCopy.end = matrixDim
                        externals.append([lineCopy.start, x])
                        self.externalEdgeCoeffs.append(x)
                    else:
                        continue
                else:
                    # -p2 -> x2
                    number = int(re.sub(r'-*p', r'', lineCopy.momentum))
                    x = "x" + str(number)
                    self.internalEdgeCoeffs.append(x)

                    # save mass information of the line
                    if lineCopy.mass != "":
                         self.lineMasses[number] = lineCopy.mass

                # diagonal elements are the sum of all attached edges
                row[vertex.number-1] += "+" + x
                # non-diagonal elements are sum of edges connecting the two vertices
                if lineCopy.start == vertex.number:
                    row[lineCopy.end-1] += "-" + x
                elif lineCopy.end == vertex.number:
                    row[lineCopy.start-1] += "-" + x

            laplacian[vertex.number-1] = row

        # remove duplicating xi
        self.internalEdgeCoeffs = list(set(self.internalEdgeCoeffs))

        # add additional row for external vertex
        if externalLinesConnected:
            row = ["0" for i in range(matrixDim)]
            for ext in externals:
                row[ext[0]-1] = "-" + ext[1]
                row[matrixDim-1] += "+" + ext[1]
            laplacian[matrixDim-1] = row

        # transform the string list into a sympy matrix
        return sy.Matrix(sy.sympify(laplacian))


    ## Calculate the Kirchhoff-polynomial, a polynomial representation of a (non-colored) graph, that characterizes its topological structure [[1]](@ref refs).
    # The Kirchhoff-polynomial \f$\mathcal{K}\f$ of a graph \f$G\f$ is just the determinant of the Laplacian (see #constructLaplacian()) with the \f$i\f$'th row and column removed,
    # where \f$i\f$ is arbitrary.
    # I.e.
    # \f{align*}
    #   \mathcal{K}(G) = \det\left(L\left(G/ \text{Vertex}_i\right)\right)\,, \quad i = 1,...,\text{\#Vertices of G}
    # \f}
    # \f$\mathcal{K}\f$ is in direct relation with the first Symanzik-polynomial #U:
    # \f{align*}
    #   \mathcal{U}(x_1,\dots,x_n) = \mathcal{K}\left(\frac{1}{x_1},\dots,\frac{1}{x_n}\right) \cdot \prod_{k=1}^n x_k \\
    #   \mathcal{K}(x_1,\dots,x_n) = \mathcal{U}\left(\frac{1}{x_1},\dots,\frac{1}{x_n}\right) \cdot \prod_{k=1}^n x_k
    # \f}
    #
    # For our [example diagram](@ref box-topo), we chose to remove the Vertex 4 and thus get
    # \f{align*}
    # \mathcal{K}(G) = \begin{vmatrix}
    #   x_1 + x_2&     -x_1&     -x_2\\
    #    -x_1& x_1 + x_3&       0\\
    #    -x_2&       0& x_2 + x_4
    # \end{vmatrix} = x_1x_2x_3 + x_1x_2x_4 + x_1x_3x_4 + x_2x_3x_4\,.
    # \f}
    def constructKirchhoffPolynom(self):
        logging.debug("Calculate the Kirchhoff polynomial of the graph...")
        laplacian = self.constructLaplacian()
        laplacian.row_del(0)
        laplacian.col_del(0)
        kirchhoffPolynom = laplacian.det()
        return sy.expand(kirchhoffPolynom)


    ## The \f$\mathcal{W}\f$-polynomial is the Kirchhoff-polynomial of the Laplacian matrix with external lines taken into account, and connected to a single additional vertex, which is removed from the matrix.
    # Thus, the calculation is similar to #constructKirchhoffPolynom().
    #
    # For the [example graph](@ref box-topo) \f$G\f$, we thus define a new graph \f$\hat{G}\f$ as
    # \image html box-W.png "The example graph with external lines connected to additional vertex" height=200px
    # \image latex box-W.eps "The example graph with external lines connected to additional vertex"
    # where \f$z_i\f$ is the line weight for the external Line according to \f$q_i\f$.
    #
    # The Laplacian has now the following structure:
    #
    # \f{align*}
    # L(\hat{G}) = \begin{pmatrix}
    #   x_1 + x_2 + z_1 & -x_1 & -x_2 & 0 & -z_1 \\
    #   -x_1 & x_1+x_3+z_2 & 0 & -x_3 & -z_3 \\
    #   -x_2 & 0 & x_2 + x_4 + z_2 & -x_4 & -z_2 \\
    #   0 & -x_3 & -x_4 & x_3+x_4+z_4 & -z_4 \\
    #   - z_1 & -z_3 & -z_2 & -z_4 & z_1+z_2+z_3+z_4
    # \end{pmatrix}\,.
    # \f}
    # \f$\mathcal{W}\f$ is thus
    # # \f{align*}
    # \mathcal{W} =& \det\left(L\left(\hat{G}/\text{Vertex}_5\right)\right) \\
    #   =& \begin{vmatrix}
    #   x_1 + x_2 + z_1 &          -x_1 &          -x_2 &            0\\
    #            -x_1 & x_1 + x_3 + z_2 &            0 &          -x_3\\
    #            -x_2 &            0 & x_2 + x_4 + z_2 &          -x_4\\
    #              0 &          -x_3 &          -x_4 & x_3 + x_4 + z_4
    #    \end{vmatrix}\\
    #  =& x_1x_2x_3z_1 + 2x_1x_2x_3z_2 + x_1x_2x_3z_4 + x_1x_2x_4z_1 + 2x_1x_2x_4z_2 + x_1x_2x_4z_4 + x_1x_2z_1z_4 \\
    #   &+ 2x_1x_2z_2z_4 + x_1x_3x_4z_1 + 2x_1x_3x_4z_2 + x_1x_3x_4z_4 + x_1x_3z_1z_2 + x_1x_3z_2^2 + x_1x_3z_2z_4 \\
    #   &+ x_1x_4z_1z_2 + x_1x_4z_1z_4 + x_1x_4z_2^2 + x_1x_4z_2z_4 + x_1z_1z_2z_4 + x_1z_2^2z_4 + x_2x_3x_4z_1 \\
    #   &+ 2x_2x_3x_4z_2 + x_2x_3x_4z_4 + x_2x_3z_1z_2 + x_2x_3z_1z_4 + x_2x_3z_2^2 + x_2x_3z_2z_4 + x_2x_4z_1z_2 \\
    #   &+ x_2x_4z_2^2 + x_2x_4z_2z_4 + x_2z_1z_2z_4 + x_2z_2^2z_4 + 2x_3x_4z_1z_2 + x_3x_4z_1z_4 + x_3z_1z_2^2 \\
    #   &+ x_3z_1z_2z_4 + x_4z_1z_2^2 + x_4z_1z_2z_4 + z_1z_2^2z_4
    # \,.
    # \f}
    def constructGraphPolynomW(self):
        logging.debug("Calculate Laplacian of the graph...")
        laplacian = self.constructLaplacian(externalLinesConnected = True)

        laplacian.row_del(laplacian.cols-1)
        laplacian.col_del(laplacian.cols-1)

        logging.debug("Calculate W polynomial of the graph...")
        wPolynom = laplacian.det()

        return sy.expand(wPolynom)


    ## We follow eqs. (52), (53), (57), and (58) in ref. [[1]](@ref refs) to Calculate the polynomials #U and #F.
    #
    # The first step is to calculate the \f$\mathcal{W}\f$-polynomial, which we get from #constructGraphPolynomW().
    #
    # #U is then given by the equation
    # \f{align*}
    # \mathcal{U} \ = \ \mathcal{W}^{(1)}_{(j)} \cdot \prod_{i=1}^n x_i
    # \f}
    # where \f$\mathcal{W}^{(1)}_{(j)}\f$ is defined as all terms of \f$\mathcal{W}\f$ that contain only one \f$z_j\f$, where \f$j\f$ is arbitrary.
    #
    # #F, on the other hand, is defined as
    # \f{align*}
    # \mathcal{F} \ = \ \mathcal{U} \cdot \sum_{i=1}^n x_i \frac{m_i^2}{\mu^2} \ + \ \sum_{j,k=1}^n \left(\frac{p_j \cdot p_k}{\mu^2}\right) \cdot \mathcal{W}^{(2)}_{(j,k)} \cdot \prod_{i=1}^n x_i\,.
    # \f}
    # \f$m_i\f$ and \f$p_i\f$ are the mass and the momentum of Line \f$i\f$, \f$\mu\f$ is the renormalization scale and
    # \f$\mathcal{W}^{(2)}_{(j,k)}\f$ are all terms of \f$\mathcal{W}\f$ which contain exact two \f$z\f$'s, namely \f$z_j\f$ and \f$z_k\f$.
    #
    # For our [example diagram](@ref box-topo), we thus get
    # \f{align*}
    # \mathcal{U} \ =& \ x_1 + x_2 + x_3 + x_4\,,\\
    # \mathcal{F} \ =& \ \left(\frac{m^2}{\mu^2}\right)x_1^2 + \left(\frac{m^2}{\mu^2}\right)x_1x_2 + \left(\frac{m^2}{\mu^2}\right)x_1x_3 + 2\left(\frac{m^2}{\mu^2}\right)x_1x_4 + \left(\frac{m^2}{\mu^2}\right)x_2x_4 + \left(\frac{m^2}{\mu^2}\right)x_3x_4 + \left(\frac{m^2}{\mu^2}\right)x_4^2 \\
    # &- q_1^2x_1x_2 - q_1^2x_1x_4 - q_1^2x_2x_3 - q_1^2x_3x_4 - 2q_1q_2x_1x_4 - 2q_1q_2x_3x_4 \\
    # &- 2q_1q_3x_2x_3 - 2q_1q_3x_3x_4 - q_2^2x_1x_4 - q_2^2x_2x_4 - q_2^2x_3x_4 - 2q_2q_3x_3x_4 \\
    # &- q_3^2x_1x_3 - q_3^2x_2x_3 - q_3^2x_3x_4\,.
    # \f}
    # \return
    # * #U as sympy expression
    # * #F as sympy expression
    def constructSymanzikPolynomials(self):
        w = self.constructGraphPolynomW()

        logging.debug("Calculate U and F...")

        # U can be extracted from the parts of W with linear power of zi's
        # Thus, normalize the power counting of the zi's together with usage of a power counting parameter z
        for zi in self.externalEdgeCoeffs:
            w = w.subs(sy.sympify(zi), sy.sympify("(" + zi + " * z)"))
        # W = W(x1, x2,..., xn) = W_0 + W_1 + W_2 + W_3 + ... where W_n only contains zi's to the power n
        w = w.expand()

        z = sy.Symbol("z")
        z1 = sy.Symbol("z1")

        # now extract only the z^1 and z^2 terms of W (i.e. W_1 and W_2)
        # for U we need only the coefficient terms of an arbitrary zi of W_1
        w1 = "+".join([str(term) for term in w.as_ordered_terms() if term.as_coeff_exponent(z1)[1] == 1])
        w1 = sy.sympify(w1)
        w1 = w1.subs([(z, 1), (z1,1)])
        if len(self.externalEdgeCoeffs) > 1:
            w1 = w1.subs([(sy.sympify(zi), 0) for zi in self.externalEdgeCoeffs[1:]])

        # for F, we need all of W_2
        w2 = " +".join([str(term) for term in w.as_ordered_terms() if term.as_coeff_exponent(z)[1] == 2])
        w2 = sy.sympify(w2)
        w2 = w2.subs(z, 1)

        # U = x1*x2*...*xn * w1(1/x1, 1/x2,..., 1/xn), with w1 being the coefficent of (z1 + z2 + ... + zn) of W_1
        prodXi = sy.sympify("*".join([x for x in self.internalEdgeCoeffs]))
        xiInversion = [(sy.sympify(x), sy.sympify(x)**(-1)) for x in self.internalEdgeCoeffs]
        self.U = prodXi * w1.subs(xiInversion)
        self.U = self.U.expand()

        # F = F0 + U * sum_i^n xi * mi^2/mu^2
        # with F0 = x1*x2*...*xn * W_2(1/x1, 1/x2,..., 1/xn, zi->qi)
        xiInversion.extend([(sy.sympify(zi), sy.sympify(re.sub(r'z', r'q', zi))) for zi in self.externalEdgeCoeffs])
        F0 = prodXi * w2.subs(xiInversion)

        # if all lines are massless F = F0
        if self.lineMasses == {}:
            self.F = F0
        else:
            self.F = F0 + self.U * sy.sympify("+".join(["x" + str(i) + "*" + m + "**2" for (i,m) in self.lineMasses.items()]))

        self.F = self.F.expand()

        return self.U, self.F


    ## Use momentum replacement rules (#externalMomentaBoundaries) and momentum conservation equation to simplify #F
    def simplifyExternalMomentaInF(self):
        # get external momentum conservation sum
        momSum = ""

        for l in self.diagram.topologicalLineList:
            if l.external:
                if l.end <= 0:
                    momSum += "+(" + l.momentum + ")"
                else:
                    momSum += "-(" + l.momentum + ")"

        momSum = sy.sympify(momSum).expand()

        # use externalMomentaBoundaries to replace momenta in F and the sum
        repl = [(sy.sympify(q), sy.sympify(F"({qRepl})")) for q, qRepl in self.externalMomentaBoundaries.items()]

        self.F = self.F.subs(repl).expand()
        momSum = momSum.subs(repl).expand()

        # replace one external momentum in favor of the others
        if momSum != 0:
            # extract momenta from the free symbols of momSum. Note: it is assumed external momenta are only replaced by a single variable!
            momenta = sorted(list(momSum.free_symbols), key=lambda x: str(x))
            # warn user if only external momentum is set to zero
            if len(momenta) == 1:
                logging.warning(F"Momentum {momenta[0]} is zero due to momentum conservation! Continue topology minimiaztion using zero external momenta.")
            # remove the last external momentum in F
            self.F = self.F.subs([(momenta[-1], sy.solve(momSum, momenta[-1])[0])])



    ## Here we use Pak's algorithm of ref. [[2]](@ref refs) to rename the graph weights \f$x_1,\dots,x_n\f$ in #U and #F such that #U times #F has a maximal lexicographic ordering.
    # If in a intermediate step, two renamings lead to the same maximized ordering, we save both similar line names in #lineSymmetries.
    # The method #applyPaksAlgorithm() is used to order #U, #F and #permutations.
    #
    # For this, we first construct a matrix describing each monomial of \f$\mathcal{U} \cdot \mathcal{F}\f$ in terms of powers of the \f$x_i\f$.
    # Then we switch the columns (corresponding to an individual \f$x_i\f$) until the matrix is lexicographic maximal ordered.
    # For this step, we use #maximizeOrdering().
    #
    # The ordering is saved in #permutations, where when we start with `[1,2,3,4]` and end up with `[2,3,1,4]`, it means that the replacements
    # \f{equation*}
    # x_1 \rightarrow x_2, \qquad x_2 \rightarrow x_3, \qquad x_3 \rightarrow x_1, \qquad x_4 \rightarrow x_4\,.
    # \f}
    # were applied to #U, #F and #lineMasses.
    #
    # The underlying TopologyAnalyzer::diagram is not affected by this ordering process.
    def canonicallyOrderSymanzikPolynomials(self):
        self.U, self.F, newPermutations, self.lineSymmetries = self.applyPaksAlgorithm(self.U, self.F, self.permutations)

        # permutate line masses
        newLineMasses = {}
        for i in range(len(newPermutations)):
            if self.permutations[i] in self.lineMasses:
                newLineMasses[newPermutations[i]] = self.lineMasses[self.permutations[i]]
        self.lineMasses = newLineMasses

        # now the new permutations are applied
        self.permutations = newPermutations


    ## Apply Pak's algorithm of ref. [[2]](@ref refs) to rename the variables \f$x_1,\dots,x_n\f$ of \p U and \p F such that \p U times \p F has a maximal lexicographic ordering.
    # @param U First Symanzik polynomial to take into account for the ordering
    # @param F Second Symanzik polynomial to take into account for the ordering
    # @param permutations List of current renaming of the polynomial variables \f$x_1,\dots,x_n\f$, same as #permutations
    # \return Tuple (U, F, permutations) -same as input- with canonically renamed polynomial variables
    @staticmethod
    def applyPaksAlgorithm(U, F, permutations):
        # for the canonical ordering of U*F, we use Pak's algorithm described in [[1]](@ref refs)
        UF = (U * F).expand()

        # [x1, x2, x3,...]
        coeffNames = [sy.Symbol("x" + str(i)) for i in range(1,len(permutations)+1)]

        # In the case expand() is not sufficient to cancel the denominators of UF (as it always should),
        # simplify() must be applied, which is slower than just expanding.
        # Thus, try to construct the polynomial from U*F. If it fails, simplify the expression and try again.
        try:
            UF = sy.Poly(UF, coeffNames).args[0]
        except:
            UF = UF.simplify()
            UF = sy.Poly(UF, coeffNames).args[0]

        # embed the polynomial in a matrix that includes the powers of the coefficients of each monomial of U*F.
        # The zero'th column contains the coefficients of the mononials
        m = []
        powerDicts = [t.as_powers_dict() for t in UF.as_ordered_terms()]
        for powDict in powerDicts:
            line = [0] * (len(permutations)+1)
            line[0] = 1

            for key, val in powDict.items():
                if key in coeffNames:
                    line[coeffNames.index(key)+1] = val
                else:
                    line[0] = line[0] *  key**val

            m.append(line)

        # sort the matrix beforehand from right to left, meaning that the last column is ordered with the least priority and the first with the most
        for i in range(len(m[0])-1, 0, -1):
            m.sort(key = lambda x: x[i])

        # exchange columns find term with maximal lexicographic ordering
        _, newPermutations, lineSymmetries = TopologyAnalyzer.maximizeOrdering(matrix = sy.Matrix(m), permutations = permutations.copy(), lineSymmetries = [])
        # orderedMatrix, newPermutations, lineSymmetries = TopologyAnalyzer.maximizeOrdering(matrix = sy.Matrix(m), permutations = permutations.copy(), lineSymmetries = [])
        # print("Matrix of {} after Ordering : ".format(self.diagram.number), orderedMatrix._format_str())

        # rename variables of Symanzik polynomials
        U = U.subs([(sy.Symbol("x" + str(permutations[i])), sy.Symbol("PLACEHOLDERX" + str(newPermutations[i]))) for i in range(len(newPermutations))])
        U = U.subs([(sy.Symbol("PLACEHOLDERX" + str(i)), sy.Symbol("x" + str(i))) for i in range(1,1+len(newPermutations))])
        U = U.expand()
        F = F.subs([(sy.Symbol("x" + str(permutations[i])), sy.Symbol("PLACEHOLDERX" + str(newPermutations[i]))) for i in range(len(newPermutations))])
        F = F.subs([(sy.Symbol("PLACEHOLDERX" + str(i)), sy.Symbol("x" + str(i))) for i in range(1,1+len(newPermutations))])
        F = F.expand()

        return U, F, newPermutations, lineSymmetries


    ## This recursive function maximizes the ordering of a monomial matrix, as described in #canonicallyOrderSymanzikPolynomials().
    #
    # Calling the function with \p col will cause in a recursion until all columns < \p col of \p matrix are ordered lexicographically maximal.
    #
    # @param matrix Sympy.Matrix with columns defined as powers of \f$x_i\f$, except the first one which are the prefactors. The rows describe different monomials.
    # @param List of ordered line indices, that describe the renaming of the line weights \f$x_i\f$. See #permutations
    # @param col The actual column number we are about to maximize.
    # @param lineSymmetries The #lineSymmetries we found so far
    # \return
    # * Sympy.Matrix with lexicographic maximized column \p col (with respect to the previously ordered columns < \p col)
    # * The actual \p permutations list
    # * The actual \p lineSymmetries we found so far
    @staticmethod
    def maximizeOrdering(matrix, permutations, col = 1, lineSymmetries = []):
        matrices = []
        # the number of lines (or x_i's)
        orderDimension = matrix.cols - 1
        # copy matrix numLines times, where x1 is exchanged by xi
        for i in range(col, orderDimension+1):
            mc = matrix.copy()
            mc.col_swap(col,i)
            mc = mc.tolist()

            # sort by the current and all columns before (priorizing the most lefter ones, except the first column which has the least priority)
            # mc.sort(key = lambda x: str(x[0]))
            for j in range(col+1,0,-1):
                if col < len(mc):
                    mc.sort(key = lambda x: x[j])

            matrices.append([i, sy.Matrix(mc)])

        # compare the i'th column and return the one which is the lexicographically largest one
        matrices.sort(key = lambda x: x[1].col(col).tolist())

        # matrix of maximal lexicographic order until column col
        m0 = matrices[0][1]

        # if two vectors have the same lexicographic weight the lines are equivalent, i.e.they are symmetric
        for m in matrices[1:]:
            mi = m[1]

            if mi.col(col) != m0.col(col):
                break
            else:
                # found symmetry
                sym = [matrices[0][0], m[0]]
                if sym not in lineSymmetries:
                    lineSymmetries.append(sym)

        # apply permutation to the permutation list. i.e. Rename xi as xj and vice versa by changing the position of i and j in permutations
        new = permutations.index(matrices[0][0])
        old = permutations.index(col)
        permutations[new], permutations[old] = permutations[old], permutations[new]

        # termination condition: all columns have been checked
        if col >= orderDimension-1:
            return matrices[0][1], permutations, lineSymmetries
        else:
            # recursive call which orders next column
            return TopologyAnalyzer.maximizeOrdering(matrices[0][1], permutations, col+1, lineSymmetries)

    ## Here we follow ref. [[6]](@ref refs) to determine if a integral is scaleless by investigating the convex hull of the product of U and F polynomials
    # @param U First Symanzik polynomial
    # @param F Second Symanzik polynomial
    # @param listOfMomenta list of line momenta
    # @param kinRep list of kinematic replacements
    # \return Boolean indicating that the corresponding Feynman integral is scaleless
    @staticmethod
    def isScaleless(U, F, listOfMomenta, kinRep):
        Nf = len(listOfMomenta)

        # Multiply the polynomials
        UF = U*F

        # Try to translate the relation to sympy and apply the kinematic relations one by one
        for lhs,rhs in kinRep.items():
            try:
                lhs = lhs.replace(".","*")
                lhs = sy.sympify(lhs)
            except:
                logging.warning(F"{lhs} in the kinematic relation cannot be interpreted by sympy!")
                exit(1)
            try:
                rhs = rhs.replace(".","*")
                rhs = sy.sympify(rhs)
            except:
                logging.warning(F"{rhs} in the kinematic relation cannot be interpreted by sympy!")
                exit(1)
            UF = UF.subs(lhs,rhs)

        # Get list of Feynman parameters
        fpars = [x  for x in list(UF.free_symbols) if str(x)[0] == "x"]

        if len(fpars) == 0:
            return True

        # Convert into sympy polynomial in the Feynman parameters, filtering out masses and momenta
        UFpoly = sy.Poly(UF, fpars)

        # Get the degrees of the monomials
        degrees = UFpoly.monoms()

        # Subtract tuples of degrees from first tuple
        subtractedDegrees = [tuple(map(lambda i, j: i - j, degrees[0], m)) for m in degrees[1:]]

        # Build matrix out of them
        M = sy.Matrix(subtractedDegrees)

        # Transpose and compute rank
        rank = (M.T).rank()

        # If rank < Nf - 1 the integral is scaleless
        if rank < Nf - 1:
            return True
        else:
            return False


    ## This method removes a bunch of Line's from #diagram and alternates #U and #F accordingly to the deletion.
    #
    # We first call #contractLines() to change Diagram::topologicalVertexList and Diagram::topologicalLineList.
    #
    # We remove the Line's corresponding to the indices in #permutations and #lineMasses, which means when we remove the indices <code>cutLines = [3,1]</code> from <code>permutations = [5,4,1,3,2]</code>,
    # we set x1 and x3 to zero.
    #
    # To get back to a normal form with lines indexed by x1,x2,...,xN, we rename the last index to the first index we deleted.
    # For our example this means:
    #
    # <code>cutLines = [3,1] -> (deletion) ->  permutations = [5,4,0,0,2] -> [5,4,2] -> (renaming: x5->x3, x4->x1) -> [3,1,2] </code>
    #
    # (of course #lineMasses is also adjusted properly)
    #
    # These changes are saved in #cutReplacements .
    # @param cutLines List of ordered Line indices i where xi corresponds to a Line l (in general l.number != i).
    # The indices of each line a given by #canonicallyOrderSymanzikPolynomials() and are thus the values of #permutations.
    # I.e. <code>permutations = [3,4,2,1], cutLines = [[2]](@ref refs) ->  permutations = [3,4,0,1] -> [3,2,1]</code>
    def deleteLines(self, cutLines):
        # Change the number of lines, the line names in the polynomials and the line mass list.
        # Additionally, rename the last edges to the cut ones, i.e. for x1,x2,x3,x4 cut x1 and rename x4 to x1
        self.cutReplacements = {len(self.diagram.internalMomenta)-n : i for n,i in enumerate(sorted(cutLines, reverse=True))} # -> {4:1}

        # delete lines from topologicalVertexList and topologicalLineList and contract vertices that are connected by those lines
        self.diagram.contractLines([self.permutations.index(cutLineIndex)+1 for cutLineIndex in cutLines])

        # save the line numbers of the cut lines (if we cut x3 from [2,3,1,4] it is line 2(which used to be called x2))
        self.cuts = [self.permutations.index(cut)+1 for cut in cutLines]

        # remove cut lines
        self.U = self.U.subs(
                [(sy.Symbol("x" + str(i)), 0) for i in cutLines]
            )
        self.U = self.U.subs(
                [(sy.Symbol("x" + str(xLarge)),sy.Symbol("x" + str(xCut))) for xLarge, xCut in self.cutReplacements.items()]
            )
        self.F = self.F.subs(
                [(sy.Symbol("x" + str(i)), 0) for i in cutLines]
            )
        self.F = self.F.subs(
                [(sy.Symbol("x" + str(xLarge)),sy.Symbol("x" + str(xCut))) for xLarge, xCut in self.cutReplacements.items()]
            )

        # the permutations list changes also
        # [4,3,1,2] -> cut x1 -> [4,3,0,2] -> rename x4 to x1 -> [1,3,0,2] -> remove cut line completely -> [1,3,2]
        for xLarge, xCut in self.cutReplacements.items():
            i1 = self.permutations.index(xLarge) # overflowing index from end
            i2 = self.permutations.index(xCut) # index of cut
            self.permutations[i1] = self.permutations[i2]
            # remove the cut lines from the permutation list
            del self.permutations[i2]

        # change line masses dict in a same manner
        for xLarge, xCut in self.cutReplacements.items():
            if xLarge in self.lineMasses and xLarge != xCut:
                self.lineMasses[xCut] = self.lineMasses[xLarge]
            elif xCut in self.lineMasses:
                del self.lineMasses[xCut]


    ## Rename multiple symbol strings in sympy expressions at the same time
    #
    # @param expr Sympy expression
    # @param listOfRenames List of symbols as strings we want to rename in \p expr.
    # Like <code>[["x", "y"], ["y, "z"]]</code> -> replace x by y and y by z at the same time, i.e. without interference of both renamings.
    # \return \p expr with renamed symbols
    @staticmethod
    def symbolMultiRename(expr, listOfRenames = [[]]):
        # this ancillary function swaps symbols in the given expression according to a swap list
        expr = expr.subs(
            [ (sy.Symbol(rename[0]), sy.Symbol("RENAMEPLACEHOLDER" + str(rename[1]))) for rename in listOfRenames ]
        )
        expr = expr.subs(
            [ (sy.Symbol("RENAMEPLACEHOLDER" + str(rename[1])), sy.Symbol(rename[1])) for rename in listOfRenames ]
        )

        return expr


    ## Re-insert the lines into #permutations that we cut via #deleteLines().
    # We use the information of #cuts to fill the removed indices of #permutations with zeros.
    # The #permutations field is not changed by this method.
    #
    # E.g. for `cuts = [2,4]` (i.e. removed Lines with indices 2 and 4) and `permutations = [2,1,3]`, we get `[2,0,1,0,3]`.
    #
    # \return List similar to #permutations with inserted zeros at previously removed lines
    def getUncutPermutations(self):
        uncutPermutations = self.permutations.copy()
        # insert a zero where we removed a line
        for cut in sorted(self.cuts):
            uncutPermutations.insert(cut-1, 0)

        return uncutPermutations


    ## Find all bridges of a topology defined by a list of Line's.
    #
    # This method only evokes topoAnalyzer.BridgeFinder::findBridges().
    # @param lines List of diagram.Line instances, that correspond to a topology (more precise to a diagram.Diagram::topologicalLineList)
    # \return List of line indices of bridges
    def findBridges(self, lines):
        bridgeFinder = BridgeFinder()
        return bridgeFinder.findBridges(lines)


    ## Similar to #constructSymanzikPolynomials(), this function uses a different approach to calculate #U and #F.
    #
    # This is a recursive implementation of the algorithm described in eq. (65) of [[1]](@ref refs).
    # The basic formulae for this approach are given by
    # \f{align*}
    # \mathcal{U}(G) &= \mathcal{U}(G/\text{Line}_k) + x_k \cdot \mathcal{U}(G-\text{Line}_k)\\
    # \mathcal{F}(G) &= \mathcal{F}(G/\text{Line}_k) + x_k \cdot \mathcal{F}(G-\text{Line}_k)\,,
    # \f}
    # where \f$G/\text{Line}\f$ describes the contraction and \f$G-\text{Line}\f$ describes the subtraction of a diagram.Line,
    # which is no self-loop and no bridge of the graph \f$G\f$.
    # This leads to a recursion (manifest in calls of #recursiveUF()) that stops at diagrams which consists only of self-loops and bridges.
    #
    # This method is -at the moment- multiple orders of magnitude faster than #constructSymanzikPolynomials(),
    # which is why this is the preferred choice in topoMinimizer.TopoMinimizer::analyzeDiagram().
    # \return
    # * #U as sympy expression
    # * #F as sympy expression
    def constructSymanzikPolynomials2(self):
        lines = self.diagram.topologicalLineList

        logging.debug("Start recursive determination of U and F...")
        self.U, F0 = self.recursiveUF(lines)

        self.F = F0
        if [e for e in lines if e.mass != "" and not e.external] != []:
            self.F += self.U * sy.sympify("+".join(["x" + str(e.number) + "*" + e.mass + "**2" for e in lines if e.mass != "" and not e.external]))

        self.U = self.U.expand()
        self.F = self.F.expand()

        return self.U, self.F


    ## Main call of #constructSymanzikPolynomials2(). Recursive contraction and deletion of diagram.Line instances until only self-loops and bridges are left.
    #
    # Bridges are found with #findBridges().
    #
    # If the final lines describe a tree graph, we need to find a momentum routing of the external lines through the remaining graph for the terminal form of #F.
    # As described by eq- (66) of [[1]](@ref refs). This is done via #momentumFlowForF().
    #
    # @param lines List of diagram.Line's that are changed gradually during the recursion until they reach the final form.
    # \return
    # * #U as sympy expression as defined in eq. (66) of [[1]](@ref refs)
    # * F0 as defined in eq. (66) of [[1]](@ref refs)
    def recursiveUF(self, lines):
        # termination condition: all edges are bridges or self-loops
        bridgeFinder = BridgeFinder()
        pivotEdge = -1
        for l in lines:
            if not l.external \
              and not l.number in bridgeFinder.findBridges(lines) \
              and not l.isSelfLoop():
                pivotEdge = l
                break

        # the bridgeFinder is no longer needed
        del bridgeFinder

        # terminal form of U and F0 is given in Eqs. (66) and (67) of [[1]](@ref refs)
        if pivotEdge == -1:
            selfLoops = ["x" + str(l.number) for l in lines if l.isSelfLoop()]
            # tree graph
            if len(selfLoops) == 0:
                return 1, self.momentumFlowForF(lines)
            else:
                return sy.sympify("*".join(selfLoops)), \
                        sy.sympify("*".join(selfLoops)) * self.momentumFlowForF([l for l in lines if not l.isSelfLoop()])

        # contract the pivot edge
        edgeContractedList = copy.deepcopy(lines)
        delIndex = None
        for e in edgeContractedList:
            if e.number == pivotEdge.number:
                delIndex = edgeContractedList.index(e)
            # contract to start vertex
            if pivotEdge.end == e.start:
                e.start = pivotEdge.start
            if pivotEdge.end == e.end:
                e.end = pivotEdge.start

        if delIndex != None:
            del edgeContractedList[delIndex]

        # remove the pivot edge
        edgeRemovedList = copy.deepcopy(lines)
        for e in edgeRemovedList:
            if e.number == pivotEdge.number:
                delIndex = edgeRemovedList.index(e)
        del edgeRemovedList[delIndex]

        # U(G) = U(G/e_i) + x_i * U(G-e_i)
        uContracted, fContracted = self.recursiveUF(edgeContractedList)
        uRemoved, fRemoved = self.recursiveUF(edgeRemovedList)
        x =  sy.sympify("x" + str(pivotEdge.number))

        return uContracted + x * uRemoved, fContracted + x * fRemoved


    ## For F we need the momentum squared flowing through all bridges of the graph.
    # Since this is only needed for tree graphs, the momentum is a linear combination of external momenta.
    #
    # This momentum routing can be easily found with usage of momentumAssigner.MomentumAssigner.
    # More specifically, we call momentumAssigner.MomentumAssigner.createMomentumMatrix() to build a momentum matrix of the tree.
    # Afterwards, momentumAssigner.MomentumAssigner.extractMomentumDictFromEchelonMatrix() is used to get the final expression from the \p momentumMatrix.
    #
    # @param lines List of diagram.Line instances that describe a tree diagram, with self-loops removed!
    # \return The tree part of F defined in eq. (67) of [[1]](@ref refs) as sympy expression
    def momentumFlowForF(self, lines):
        edgeNumbers = [e.number for e in lines if not e.external]
        # if graph consists only of self-loops, F0 equals zero
        if len(edgeNumbers) == 0:
            return 0

        # create a reduced version of the full topologicalVertexList by the the self-loops and the cut lines of the current edge list
        newTopoVertices = {}
        # save the momenta of the remaining lines to find their routing
        internalMomenta = []
        externalMomenta = []
        for e in lines:
            p = re.sub(r'\-*', r'', e.momentum)
            # find vertices connected to external lines
            if e.external:
                externalMomenta.append(p)
                innerVertex = max(e.start, e.end)
                if innerVertex not in newTopoVertices.keys():
                    newTopoVertices[innerVertex] = Vertex()
                    newTopoVertices[innerVertex].number = innerVertex
                newTopoVertices[innerVertex].lines.append(e)

            # and to internal ones
            else:
                internalMomenta.append(p)
                for innerVertex in [e.start, e.end]:
                    if innerVertex not in newTopoVertices.keys():
                        newTopoVertices[innerVertex] = Vertex()
                        newTopoVertices[innerVertex].number = innerVertex
                    newTopoVertices[innerVertex].lines.append(e)

        # delete duplicates and create momentum matrix to get the momentum routing
        internalMomenta = list(set(internalMomenta))
        externalMomenta = list(set(externalMomenta))
        externalMomenta.sort(reverse = True)
        momentumMatrix = MomentumAssigner.createMomentumMatrix(list(newTopoVertices.values()), internalMomenta+externalMomenta)

        # bring to echelon form
        momentumMatrix = sy.Matrix(momentumMatrix).rref()[0]
        # extract momenta
        momentumDict = MomentumAssigner.extractMomentumDictFromEchelonMatrix(momentumMatrix, internalMomenta+externalMomenta)

        # now sum over all x_i * p_i**2 for every edge index i
        res = ""
        for i in edgeNumbers:
            res += "-(" + momentumDict["p" + str(i)] + ")**2 * x" + str(i)

        return sy.sympify(res)


    ## Find the Nickel-index as defined according to [[3]](@ref refs).
    #
    # The Nickel-index provides a canonical name of an arbitrary graph.
    # Its notation , the ``Nickel-notation'', is the following (see eq. (1) of  [[3]](@ref refs)):
    # ```
    # "e" for external lines, vertex index for vertices connected to vertex 0 |
    #  "e" for external lines, vertex index for vertices connected to vertex 1, without 0 |
    #  "e" for external lines, vertex index for vertices connected to vertex 2, without 0,1 | ...
    # ```
    # If multiple lines connect two vertices, the vertex index just appears twice in the column of the other vertex.
    # The numeration of the vertex indices for the Nickel-notation follow a specific numeration (alpha-numeric).
    #
    # We get for our [example diagram](@ref box-topo) the string `e12|e3|e3|e`.
    #
    # Since the vertex numbering is arbitrary, we can find multiple such strings (#Vertices! many).
    # The one that is lexicographic minimal is the so called Nickel-index.
    #
    # To differentiate between different kind of Line's, we add a second string to the main string,
    # defined as the string before, only where the vertex number is replaced my the mass of the corresponding line and the entries are separated by a "_". Massless lines get an empty string "". An exception are made for external lines, they are represented by their momentum that flows into the vertex.
    # Furthermore, replacements according to #externalMomentaBoundaries are applied.
    #
    # For example, we then get for our box: `e12|e3|e3|e : q1_M1_|q2_|q3_M1|q4`
    #
    # This is a wrapper function for the computation of the Nickel-Index using a C++ backend, implemented in Cython.
    #
    # @param massesList Ordered List of names of occuring masses. Must be the same for all comparing diagrams.
    #
    # \return The Nickel-Index as a String
    def findNickelIndex(self, massesList):
        listOfLineNumbers = []
        # Translate every Line of the diagram to a list of the form [vertex 1, vertex 2, mass number, line number]
        # For external lines, second vertex number and the line number are the negative number of the external momentum, i.e. -4 for q4
        lines = []
        for line in self.diagram.topologicalLineList:
            # External lines
            if line.external:
                externalIndex = -int(re.sub(r'-*q', r'', line.momentum))
                internalIndex = max(line.start, line.end)
                assert type(externalIndex) is int, F"External index {externalIndex} is not a valid!"
                lines.append([internalIndex, externalIndex, 0, externalIndex])
            # Internal lines
            else:
                if line.mass == "":
                    massIndex = 0
                else:
                    # Take the mass index from the definition in the config file
                    massIndex = massesList.index(line.mass)+1
                assert line.number != 0, F"Lines are not properly numbered for diagram {self.diagram.name}"
                lines.append([line.start, line.end, massIndex, line.number, 1])
                listOfLineNumbers.append(line.number)

        listOfLineNumbers = sorted(list(set(listOfLineNumbers)))
        # convert to good line numbers 1,...,n
        self.goodLineNumbers = [[listOfLineNumbers[i], i+1] for i in range(len(listOfLineNumbers))]
        for l in lines:
            ln = l[3]
            if ln > 0:
                lntmp = ln
                for gnp in self.goodLineNumbers:
                    if gnp[0] == ln:
                        lntmp = gnp[1]
                l[3] = lntmp


        # Call the Cython implementation of the Nickel-Index finder with the newly constructed list
        self.nickelIndex, self.nickelVertexPermutations, self.nickelOrderedMomenta, self.nickelOrderedExternalMomenta = nickel.findNickelIndex(lines)
        # Re-substitute mass indices by actual mass names
        self.nickelIndex = self.nickelIndex.decode("utf-8")
        for i,m in enumerate(massesList):
            self.nickelIndex = re.sub(F"M{i+1}" + r'(_|\||$)', m + r'\1', self.nickelIndex)

        # Rename momenta according to self.externalMomentaBoundaries
        for key,val in self.externalMomentaBoundaries.items():
            self.nickelIndex = re.sub(key, val, self.nickelIndex)

        return self.nickelIndex


## This ancillary class finds bridges of edge lists.
#
# After initializing an instance, the #bridges can be found via #findBridges as a list of Line::number's.
# It is based on [[4]](@ref refs)
class BridgeFinder:
    ## Constructor
    def __init__(self):
        ## List of already visited vertex indices
        self.visited = []
        self.tin = {}
        self.low = {}
        ## Time index, that keeps track of the number of steps taken so far
        self.timer = 0
        ## Dict of all Vertex::number with adjascent Line's.
        # It is of the form <code>{1: [line1, line2,...], ...}</code>
        self.vertices = {}
        ## List of found bridges as Line::number's
        self.bridges = []


    ## This is the main method described in [[4]](@ref refs)
    # @param lines List of Line's of the topology of which we want to know where the bridges are
    # \return List of Line::number's corresponding to bridges
    def findBridges(self, lines):
        # build vertexList
        for e in lines:
            # discard self-loops and external lines
            if e.isSelfLoop() or e.external:
                continue
            for v in [e.start, e.end]:
                # add edge to existing vertex
                if v in self.vertices.keys():
                    self.vertices[v].append(e)
                # or create new one if it doesn't exist already
                else:
                    self.vertices[v] = [e]

        if self.vertices == {}:
            return []

        # start recursion
        self.dfs(list(self.vertices.keys())[0], Line())
        return self.bridges


    ## A straightforward implementation of the recursive "depth-first search" algorithm fully described in [[5]](@ref refs)
    def dfs(self, vertexNumber, parentEdge):
        # standard depth first search
        self.timer += 1
        self.visited.append(vertexNumber)
        self.tin[vertexNumber] = self.timer
        self.low[vertexNumber] = self.timer

        for e in self.vertices[vertexNumber]:
            if e.number == parentEdge.number:
                continue
            to = e.to(vertexNumber)
            if to in self.visited:
                self.low[vertexNumber] = min(self.low[vertexNumber], self.tin[to])
            else:
                self.dfs(to, e)
                self.low[vertexNumber] = min(self.low[vertexNumber], self.low[to])
                if self.low[to] > self.tin[vertexNumber]:
                    self.bridges.append(e.number)
