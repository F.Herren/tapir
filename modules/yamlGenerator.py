## @package yamlGenerator
# Here, we provide the necessary class modules.yamlGenerator.YamlGenerator to produce YAML files from modules.diagram.Diagram or modules.integralFamily.IntegralFamily instances.


from modules.ediaGenerator import EdiaGenerator
import logging


## Class used to create YAML files from modules.diagram.Diagram instances and to write them to a corresponding file.
class YamlGenerator:
    ## Constructor
    # @param conf modules.config.Conf instance that describes the meta data of current diagrams
    def __init__(self, conf, verbose=False):
        ## modules.config.Conf instance that describes the meta data of current diagrams
        self.conf = conf
        ## Verbose flag to print progress during file reading.
        self.verbose = verbose


    ## Translate a modules.diagram.Diagram instance into a string in YAML format
    # This method is the inversion of modules.yamlReader.YamlReader::yamlToDiagram()
    # @param diagram modules.diagram.Diagram instance we want to translate
    # \return String representing the input diagram in YAML
    def diagramToYaml(self, diagram):
        # Define the spacing between the object levels
        spacer="  "

        # Diagram.internalMomenta
        internalLines = []
        for mom in diagram.internalMomenta:
            internalLines.append("{" + F"momentum: {mom[0]}, vertices: {[mom[1], mom[2]]}, incomingParticle: {mom[3]}, outgoingParticle: {mom[4]}, mass: {mom[7]}" + "}")
        internalLines = F"\n{3*spacer}- ".join(internalLines)

        # Diagram.externalMomenta
        externalLines = []
        for mom in diagram.externalMomenta:
            externalLines.append("{" + F"incomingMomentum: {mom[0]}, vertex: {mom[1]}, incomingParticle: {mom[2]}" + "}"
            )
        externalLines = F"\n{3*spacer}- ".join(externalLines)

        # Combine YAML string
        diagramYaml = F"""{spacer}- diagramNumber: {diagram.diagramNumber}
{2*spacer}loops: {diagram.numLoops}
{2*spacer}preFactor: {diagram.preFactor}
{2*spacer}externalLines:
{3*spacer}- {externalLines}
{2*spacer}internalLines:
{3*spacer}- {internalLines}
"""
        return diagramYaml


    ## Translate the topological structure of a modules.diagram.Diagram instance into a string in YAML format
    # This method is the inversion of modules.yamlReader.YamlReader::yamlToTopology()
    # @param diagram modules.diagram.Diagram instance we want to translate
    # \return String representing the topology of the input diagram in YAML
    def topologyToYaml(self, diagram):
        # Define the spacing between the object levels
        spacer="  "

        # Diagram.internalMomenta
        internalLines = []
        for mom in diagram.internalMomenta:
            internalLines.append("{" + F"momentum: {mom[0]}, vertices: {[mom[1], mom[2]]}, mass: {mom[7]}" + "}")
        internalLines = F"\n{3*spacer}- ".join(internalLines)

        # Diagram.externalMomenta
        externalLines = []
        for mom in diagram.externalMomenta:
            externalLines.append("{" + F"incomingMomentum: {mom[0]}, vertex: {mom[1]}" + "}"
            )
        externalLines = F"\n{3*spacer}- ".join(externalLines)

        # Combine YAML string
        topologyYaml = F"""{spacer}- name: {diagram.name}
{2*spacer}loops: {diagram.numLoops}
{2*spacer}externalLines:
{3*spacer}- {externalLines}
{2*spacer}internalLines:
{3*spacer}- {internalLines}
"""
        # Add nickel index if known
        if diagram.nickelIndex:
            topologyYaml += F"{2*spacer}nickelIndex: {diagram.nickelIndex}"
        return topologyYaml


    ## Export a list of modules.diagram.Diagram instances to a YAML file
    # @param outputFileName The file name of the output destination
    # @param diagrams List of modules.diagram.Diagram instances which shall be exported
    # @param onlyTopologies If set to true the topology information of #topologyToDict() is written instead of #diagramToDict()
    def writeDiagrams(self, outputFileName, diagrams, onlyTopology=False):
        # Specify the overall YAML key
        if onlyTopology:
            diagramsKey = "topologies"
        else:
            diagramsKey = "diagrams"

        # Generate output file
        EdiaGenerator.generatePath(outputFileName)

        # Create individual YAML entries for each diagram or topology
        if onlyTopology:
            yamlDiagramList = [self.topologyToYaml(d) for d in diagrams]
        else:
            yamlDiagramList = [self.diagramToYaml(d) for d in diagrams]

        # Write entries to output file. Either in a single one...
        if self.conf.blockSize < 1:
            try:
                with open(outputFileName,'w') as yamlFile:
                    yamlFile.write(diagramsKey + ":\n" + "\n".join(yamlDiagramList))
            except FileNotFoundError:
                logging.error("Not able to write " + outputFileName)
                exit(1)
        # ... or to multiple files according to config.blockSize
        else:
            listOfEntryLists = [yamlDiagramList[i:i+self.conf.blockSize] for i in range(0, len(yamlDiagramList), self.conf.blockSize)]
            for i in range(0,len(listOfEntryLists)):
                try:
                    with open(outputFileName + "." + str(i+1),'w') as yamlFile:
                        yamlFile.write(diagramsKey + ":\n" + "\n".join(listOfEntryLists[i]))
                except FileNotFoundError:
                    logging.error("Not able to write " + outputFileName + "." + str(i+1))
                    exit(1)


    ## Export the topological structure of modules.diagram.Diagram instances to a YAML file
    # This method is wrapper around #writeDiagrams()
    # @param outputFileName The file name of the output destination
    # @param diagrams List of modules.diagram.Diagram instances which shall be exported
    def writeTopologies(self, outputFileName, diagrams):
        return self.writeDiagrams(outputFileName, diagrams, onlyTopology=True)


    ## Translate a analytic integral family to a YAML interpretable string
    # This method can translate standard propagators, i.e. 1/(M^2 - p^2), and eikonal propagators, i.e. 1/(k.q), in a YAML string which can be interpreted by e.g. Kira.
    # For example:
    #    - name: "d3l1"
    #     loop_momenta: [k3, k5, k6]
    #     propagators:
    #       - [ "-k6^2", 0]
    #       - [ "-k5^2", 0]
    #       - [ "-k3^2 + m3^2", 0]
    #       - [ "-(k6 - k5)^2", 0]
    #       - [ "-(k5 + q1 - k6)^2 + m3^2", 0]
    #       - [ "-(k3 - k5 + k6 - q1)^2", 0]
    #       - [ "-k3*k6", 0]
    #       - [ "-k3*k5", 0]
    #       - [ "-k5*q1", 0]
    #
    # @param integralFamily modules.integralFamily.IntegralFamily instance we want to translate
    # \return YAML string representation of the given integral family
    def integralFamilyToYaml(self, integralFamily):
        # Define the spacing between the object levels
        spacer="  "

        # Build everything except the propagators
        yamlString = F"""{spacer}- name: "{integralFamily.topoName}"
{spacer}  loop_momenta: [{", ".join(integralFamily.loopMomenta)}]
{spacer}  propagators:\n"""

        # Add propagators
        yamlString = yamlString + "\n".join([F"""{3*spacer}- ["{prop}", 0]""" for prop in integralFamily.analyticTopologyExpression])
        yamlString = yamlString.replace('**','^')

        return yamlString


    ## Export a list of modules.integralFamily.IntegralFamily instances to a YAML file
    # This method calls #integralFamilyToYaml() to translate every integral family into a YAML string and writes it to a file.
    # For the input three lists of the same length should be provided, where the i'th entry of each list describes the i'th integral family.
    # @param outputFileName The file name of the output destination
    # @param integralFamilies List of modules.integralFamily.IntegralFamily instances which serve as output
    def writeIntegralFamilies(self, outputFileName, integralFamilies):
        try:
            with open(outputFileName,'w') as yamlFile:
                yamlFile.write("integralfamilies:\n")
                # Print individual integral families one after another
                for family in integralFamilies:
                    yamlFile.write( self.integralFamilyToYaml(family) + "\n" )

        except FileNotFoundError:
            logging.error("Not able to write " + outputFileName)
            exit(1)
