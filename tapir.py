#!/usr/bin/env python3

"""
    tapir is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tapir is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tapir.  If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = "3.3" # Note: this number has to be changed in tapir.doxy as well!
__authors__ = "Marvin Gerlach, Florian Herren, Martin Lang"


import os
import logging
import argparse
import traceback

from modules.config import Conf
from modules.qgrafReader import QgrafReader
from modules.topselReader import TopselReader
from modules.ediaGenerator import EdiaGenerator
from modules.diaGenerator import DiaGenerator
from modules.topoFileGenerator import TopoFileGenerator
from modules.topoMinimizer import TopoMinimizer
from modules.externalLineFilter import ExternalLineFilter
from modules.sigmaLineFilter import SigmaLineFilter
from modules.diagramFilter import DiagramFilter
from modules.diagramDrawer import DiagramDrawer
from modules.bridgeRemover import BridgeRemover
from modules.feynmanRulesReader import FeynmanRulesReader
from modules.UFOReader import UFOReader
from modules.duplicateLineRemover import DuplicateLineRemover
from modules.yamlGenerator import YamlGenerator
from modules.partialFractionDecomposer import PartialFractionDecomposer
from modules.yamlReader import YamlReader


tapirString = r'''
                      @@@@@@@@@@@@@@@
                %@@@@@@@@@@@@@@@@@@@@@@@@@#               *
    *        @@@@@@@@@*             #@@@@@@@@@
          @@@@@@@*                       #@@@@@@@
        @@@@@@          *                    @@@@@@
      @@@@@@                         @@@@      @@@@@@
*    @@@@@                       @@@@@@@@@@@     @@@@@
   #@@@@           @@         @@@@@@@@@@@@@@@@    .@@@@.
  &@@@@             @@@@@ @@@@@@@@@@@@@@@@@@@@@@    @@@@#
 .@@@@               @@@@@@@@@@@@@@@@@@@@@@@@@@@@    @@@@
 @@@@                  @@@@@@@@@@@@@@@@@@@@@@@@@@@    @@@@
@@@@,                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@   %@@@%
@@@@       *         @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#   @@@@
@@@@                %@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@
@@@%               .@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@*   @@@@
@@@%              .@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@      @@@@
@@@@              @@@@@@@@@@@@@@@@@@@@@@@@@@@@   *     @@@@
@@@@             @@@@@@@@@@@@@@@@@@@@@@@@@@            @@@@
@@@@,           @@@@@@@@@@@@@@@@@@@@@@                %@@@&
 @@@@           @@@@@@@@@@                            @@@@
 .@@@@  *       @@@@@@@                              @@@@
  @@@@@         @@@@@@                 *            @@@@#
   #@@@@        @@@@@@                            .@@@@.
     @@@@@       *@@@@                           @@@@@
 *    _________     _       _______  _____  _______
     |  _   _  |   / \  *  |_   __ \|_   _||_   __ \  *
     |_/ | | \_|  / _ \      | |__) | | |    | |__) |
         | |     / ___ \     |  ___/  | |    |  __ /
    *   _| |_  _/ /   \ \_  _| |_   *_| |_  _| |  \ \_
*      |_____||____| |____||_____|  |_____||____| |___|
       ######@#####@@#####@######@@@######@#####@@####@
'''


"""
This is the head script of TAPIR.
All subroutines are called from here.
The program starts with a __main__() call at the end of the current file
"""



## Controlled program termination with hint to program usage
def legalExit(parser=None):
    print()
    if parser:
        parser.print_usage()
    exit(1)



## Wrapper function for the complete program execution
def __main__():
    parser = argparse.ArgumentParser(prog="tapir")
    subparsers = parser.add_subparsers(dest="commands")
    parser.add_argument("-v", "--version", help="Show version number and exit",action="version", version="TAPIR version " + __version__ + " by " + __authors__)
    parser.add_argument("-s", "--silent", help="Run quietly and print only the most necessary information",action="store_true", default=False)
    parser.add_argument("-c","--conf", type=str, metavar="CONF_FILE", help="Use CONF_FILE as .config file. If not specified, 'tapir.conf' is taken by default", default="tapir.conf")
    parser.add_argument("-k", "--kernels", type=int, metavar="N", nargs="?", const=0, default=1, help="Define that N cores shall be used. If no number is given, tapir will use all available cores.")
    parser.add_argument("-q","--qlist", type=str, metavar="QLIST_FILE", help="Use QLIST_FILE as .qlist input. This is the output of QGRAF using the style file 'qgraf-tapir.sty'")
    parser.add_argument("-Q","--qlist-out", type=str, metavar="QLIST_FILE", help="Output the remaining diagrams after diagram filtering to QLIST_FILE. This only works with diagram input.")
    parser.add_argument("-do","--diaout", type=str, metavar="DIA_FILE", help="Output .dia file as DIA_FILE (analytic diagram information)")
    parser.add_argument("-eo","--ediaout", type=str, metavar="EDIA_FILE", help="Output .edia file as EDIA_FILE (topological diagram information)")
    parser.add_argument("-t", "--topselout", type=str, metavar="TOPSEL_FILE", help="Generate topsel entries from the diagrams that are left after all filters and write them to TOPSEL_FILE. This can be used as an input for exp")
    parser.add_argument("-dyo", "--diagram-yaml-out", type=str, metavar="YAML_DIA_FILE", help="Output the remaining diagrams after diagram filtering to YAML_DIA_FILE")
    parser.add_argument("-tyo", "--topology-yaml-out", type=str, metavar="YAML_TOPO_FILE", help="Output the remaining topologies at the end of the topology minimization and manipulations to YAML_TOPO_FILE")
    parser.add_argument("-dyi", "--diagram-yaml-in", type=str, metavar="YAML_DIA_FILE", help="Read the diagrams of YAML_DIA_FILE and use them as diagram input")
    parser.add_argument("-tyi", "--topology-yaml-in", type=str, metavar="YAML_TOPO_FILE", help="Read the topologies of YAML_TOPO_FILE and use them as topology input. Note: .(e)dia files cannot be generated with this option")
    parser.add_argument("-i", "--topselin", type=str, metavar="TOPSEL_FILE", help="Read the topsel file TOPSEL_FILE instead of a qlist file as input. Note: .(e)dia files cannot be generated with this option")
    parser.add_argument("-o", "--ordertopsel", action="store_true", help="Order topsel entries by number of propagators (together with -t)")
    parser.add_argument("-f", "--topologyfolder", type=str, metavar="DIR", help="Generate topology files and write them to DIR. Existing topology files in DIR are overwritten.")
    parser.add_argument("-m", "--minimize", type=str, metavar="TOPSEL_FILE", nargs="?", const="", help="Minimize diagrams using Nickel-Index comparison before generating topsel or topology files. This is the fastest minimization method. If TOPSEL_FILE is provided, match the found topologies with the ones from the given topsel file and create new ones, if no matching could be found")
    parser.add_argument("-M", "--minimize-fine", type=str, metavar="TOPSEL_FILE", nargs="?", const="", help="Minimize diagrams using Pak's algorithm and Nickel-Index comparison before generating topsel or topology files. This is slower than the '-m' minimization but finds all graph symmetries. If TOPSEL_FILE is provided, match the found topologies with the ones from the given topsel file and create new ones, if no matching could be found")
    parser.add_argument("-pf", "--partfrac", action="store_true", help="Toggle partial fraction decomposition at the end of topology file generation.", default=False)
    parser.add_argument("-pm", "--partfrac-minimize", type=str, metavar="TOPOLOGY_LIST", nargs="?", const="", help="Toggle partial fraction decomposition at the end of topology file generation and minimize the resulting analytic topologies. If FIRE_TOPOLOGY_LIST is specified as a mathematica or yaml topology list file, the newly found topologies are reduced by taking the given topologies into account")
    parser.add_argument("-da", "--diagramart", type=str, metavar="LATEX_FILE", nargs="?", const="diagrams.tex", help="Generate TikZ-Feynman representation of all Feynman diagrams from the input and write them to LATEX_FILE. It can be compiled using lualatex")
    parser.add_argument("-ta", "--topologyart", type=str, metavar="LATEX_FILE", nargs="?", const="topologies.tex", help="Generate TikZ-Feynman representation of all topologies that are present and write them to LATEX_FILE. It can be compiled using lualatex")
    parser.add_argument("-ra", "--repart", type=str, metavar="LATEX_FILE", nargs="?", const="representatives.tex", help="Generate TikZ-Feynman representation of representative Feynman diagrams with unique topologies and write them to LATEX_FILE. It can be compiled using lualatex")
    parser.add_argument("-q2e", "--q2e", action="store_true", help="This flag triggers the .(e)dia file generation to be compatible with q2e, newer features like 4 fermion vertices are not supported with this option")

    # UFOReader subparser
    ufo_parser = subparsers.add_parser("ufo")
    ufo_parser.add_argument("-s", "--silent", help="Run quietly and print only the most necessary information",action="store_true", default=False)
    ufo_parser.add_argument("-c", "--conf", type=str, metavar="UFOREADER_CONF_FILE", help="Use UFOREADER_CONF_FILE as .config file. Can be omitted if both '--ufo-indir' and '--ufo-outdir' are given instead.")
    ufo_parser.add_argument("-ui", "--ufo-indir", type=str, metavar="UFOREADER_INPUT_DIR", help="Use UFOREADER_INPUT_DIR as the path to the directory containing the UFO input files.")
    ufo_parser.add_argument("-uo", "--ufo-outdir", type=str, metavar="UFOREADER_OUTPUT_DIR", help="Use UFOREADER_OUTPUT_DIR as the path where the files produced by the UFOReader are to be written to.")
    ufo_parser.add_argument("-une", "--ufo-no-export", action="store_true", help="This flag prevents the writing of output files from the UFOReader.")

    # Partial fraction decomposition subparser
    pf_parser = subparsers.add_parser("partfrac")
    pf_parser.add_argument("-s", "--silent", help="Run quietly and print only the most necessary information",action="store_true", default=False)
    pf_parser.add_argument("-i", "--topologylistin", type=str, metavar="TOPOLOGY_LIST", help="Load topology list file TOPOLOGY_LIST. This can either be a Mathematica or YAML topology list file.")
    pf_parser.add_argument("-f", "--topologyfolder", type=str, metavar="DIR", help="Generate topology files which include the partial fraction decomposition and write them to DIR. Existing topology files in DIR are overwritten.")
    pf_parser.add_argument("-pm", "--partfrac-minimize", type=str, metavar="FIRE_TOPOLOGY_LIST", nargs="?", const="", help="Minimize the resulting analytic topologies after partial fraction decomposition. If FIRE_TOPOLOGY_LIST is specified, reduce the amount newly found topologies by taking the given FIRE topologies into account")
    pf_parser.add_argument("-k", "--kernels", type=int, metavar="N", nargs="?", const=0, default=1, help="Define that N cores shall be used. If no number is given, tapir will use all available cores.")

    args = parser.parse_args()

    # configure logging options. Uncomment the first and comment the second line to see debug messages
    # logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
    logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.WARNING)

    # colorize logging output
    logging.addLevelName(logging.WARNING, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
    logging.addLevelName(logging.ERROR, "\033[1;41m%s\033[1;0m" % logging.getLevelName(logging.ERROR))

    # print ASCII art on startup
    if not args.silent:
        print(tapirString)
    logging.info("TAPIR started")

    # allow not having a config file when the only output is a topology drawing
    if args.conf == "tapir.conf" and not os.access(args.conf,os.R_OK) and args.topologyart != None and args.diaout == None and args.ediaout == None and args.topologyfolder == None:
        args.conf = None
    # allow not having a config file when the partfrac subcommand is called
    # (as it does not support config files anyway, yet)
    if args.commands == "partfrac":
        args.conf = None

    # check for existence and read permission of config file
    if args.conf != None and not os.access(args.conf,os.R_OK):
        logging.error(F"No configuration file {args.conf} found!")
        legalExit(parser)

    # read config file
    config = Conf(args.conf, args, args.q2e, args.conf == None)

    # Check for corrupting command line arguments
    # No input was defined
    if [args.commands, args.topselin, args.qlist, args.diagram_yaml_in, args.topology_yaml_in].count(None) == 5:
        logging.error("No diagram (qlist, YAML), topology (topsel, YAML) and no UFO module as input defined!")
        legalExit(parser)
    # Using multiply input sources is prohibited
    if [args.commands, args.topselin, args.qlist, args.diagram_yaml_in, args.topology_yaml_in].count(None) != 4:
        logging.error("Cannot load two inputs simultaneously!")
        legalExit(parser)

    # Check mandatory commands for partfrac submodule
    if args.commands == "partfrac":
        # Check if the output path is given
        if args.topologyfolder == None:
            logging.error("No output topology folder defined!")
            legalExit(parser)
        # Check if a topology list file was provided
        if args.topologylistin == None:
            logging.error("No input topology list file defined!")
            legalExit(parser)

    # standalone partial fractioning mode
    # after running through, exit
    if args.commands == "partfrac":
        # parse fire topology list and partial fraction
        partialFractionDecomposer = PartialFractionDecomposer(conf=config, partFracFile=args.partfrac_minimize, verbose=not args.silent, kernels=args.kernels)
        partialFractionDecomposer.standaloneDecomposition(
            topologyListFile=args.topologylistin,
            topologyFolder=args.topologyfolder,
            mathematicaListFileName="topologyList.m",
            yamlListFile="topologyList.yaml",
            minimize=args.partfrac_minimize!=None
        )
        return

    # Using only topology input prohibits the usage of diagram routines
    if args.topselin != None or args.topology_yaml_in != None:
        # user tries to get dia file from topsel file (not explicitely doable due to unknown particle definitions)
        if args.diaout != None:
            logging.error("Cannot construct dia file from a topology input!")
            legalExit(parser)
        # user tries to get qlist output file from topsel file
        if args.qlist_out != None:
            logging.error("Cannot construct qlist output file from a topology input!")
            legalExit(parser)
        # user tries to draw a diagram from topsel file
        if (args.diagramart != None or args.repart != None):
            logging.error("Cannot draw diagrams from a topology input! Try instead \"--topologyart\"")
            legalExit(parser)
        # user tries to apply diagram filters to input from topsel file
        if (len(config.filters) > 0):
            logging.error("Cannot apply diagram filters " + ", ".join([f[0] for f in config.filters]) + " to a topology input!")
            legalExit(parser)

    # The legacy q2e mode requires the specification of dia and edia output file
    if args.q2e:
        # Invalid qlist input
        if args.qlist == None:
            logging.error("No qlist file defined! This is required in q2e mode.")
            legalExit(parser)
        # Invalid dia output
        if args.diaout == None:
            logging.error("No output dia file defined! This is required in q2e mode.")
            legalExit(parser)
        # Invalid edia output
        if args.ediaout == None:
            logging.error("No output edia file defined! This is required in q2e mode.")
            legalExit(parser)

    # Check for existing files
    # qlist file
    if args.qlist != None and not os.access(args.qlist, os.R_OK):
        logging.error("File "+args.qlist+" not found!")
        legalExit(parser)
    # topsel input file
    if args.topselin != None and not os.access(args.topselin, os.R_OK):
        logging.error("File "+args.topselin+" not found!")
        legalExit(parser)
    # YAML diagram input file
    if args.diagram_yaml_in != None and not os.access(args.diagram_yaml_in, os.R_OK):
        logging.error("File "+args.diagram_yaml_in+" not found!")
        legalExit(parser)
    # YAML topology input file
    if args.topology_yaml_in != None and not os.access(args.topology_yaml_in, os.R_OK):
        logging.error("File "+args.topology_yaml_in+" not found!")
        legalExit(parser)
    # topsel update file
    if args.minimize != None and args.minimize != "" and not os.access(args.minimize, os.R_OK):
        logging.error("File "+args.minimize+" not found!")
        legalExit(parser)
    # qlist-out must have a diagram input
    if args.qlist_out != None and args.qlist == None and args.diagram_yaml_in == None:
        logging.error("Cannot output a qlist file without an diagram input!")
        legalExit(parser)
    # YAML diagram output must have a diagram input
    if args.diagram_yaml_out != None and args.qlist == None and args.diagram_yaml_in == None:
        logging.error("Cannot output a YAML diagram file without an diagram input!")
        legalExit(parser)


    # Handle inputs
    # Parse qlist file
    if args.qlist != None:
        diagrams = QgrafReader(config, verbose=not args.silent, kernels=args.kernels).getDiagrams(args.qlist)
    # Parse YAML diagram file
    elif args.diagram_yaml_in != None:
        diagrams = YamlReader(config, verbose=not args.silent, kernels=args.kernels).readDiagrams(args.diagram_yaml_in)
    # Generate Feynman rules
    elif args.commands == "ufo":
        if (args.conf or (args.ufo_indir and args.ufo_outdir)) and not args.ufo_no_export:
            # Generate .lag file
            feynmanRulesReader = UFOReader(config, verbose=not args.silent)
            feynmanRulesReader.getPropagators()
            feynmanRulesReader.getVertices()
            feynmanRulesReader.exportLagFile()
            feynmanRulesReader.exportCouplingsFile()
            feynmanRules = feynmanRulesReader.returnRules()
            feynmanRules.writeq2eFiles()
    # Parse topsel file
    elif args.topselin != None:
        topselReader = TopselReader(config)
        diagrams = topselReader.getDiagrams(args.topselin)
    # Parse YAML topology file
    elif args.topology_yaml_in != None:
        diagrams = YamlReader(config, verbose=not args.silent, kernels=args.kernels).readTopologies(args.topology_yaml_in)
    # Exit if nothing else is applied
    else:
        logging.error("No diagram (qlist, YAML), topology (topsel, YAML) and no UFO module as input defined!")
        legalExit(parser)


    # Apply diagram filters
    if args.qlist or args.diagram_yaml_in:
        diagrams = DiagramFilter(config, verbose=not args.silent).filter(diagrams)


    # Write qlist output file after the diagram filtering
    if args.qlist_out != None:
        QgrafReader(config, verbose=not args.silent, kernels=args.kernels).writeDiagrams(args.qlist_out, diagrams)

    # Write YAML diagram output file after the diagram filtering
    if args.diagram_yaml_out != None:
        YamlGenerator(config).writeDiagrams(args.diagram_yaml_out, diagrams)


    # draw feynman diagrams
    if args.diagramart != None:
        diagramDrawer = DiagramDrawer(config, args.diagramart)
        diagramDrawer.drawDiagrams(diagrams)


    # save the current topological line lists to draw it after minimization
    if args.repart != None:
        for d in diagrams:
            d.cacheTopologicalLineList()


    # diaGenarator may change the number of diagrams, the old diagrams list is used for topology analysis
    if args.qlist or args.topselin or args.diagram_yaml_in or args.topology_yaml_in:
        diaDiagrams = diagrams

    # generate .dia output file
    if args.diaout != None:
        if not args.commands == "ufo":
            feynmanRulesReader = FeynmanRulesReader(config)
        elif args.ufo_no_export:
            feynmanRulesReader = UFOReader(config, verbose=not args.silent)
            feynmanRulesReader.getPropagators()
            feynmanRulesReader.getVertices()
        feynmanRules = feynmanRulesReader.returnRules()
        diaGenerator = DiaGenerator(config, feynmanRules, verbose=not args.silent, kernels=args.kernels)
        diaDiagrams = diaGenerator.generateEntriesAndExtendDiagrams(diagrams)
        diaGenerator.writeDia(args.diaout)


    # remove lines of sigma particles that do not carry momentum and hence have no meaning in an edia file
    if config.ediaNoSigma:
        diagrams = SigmaLineFilter(config, topselIn=args.topselin != None, verbose=not args.silent, kernels=args.kernels).filter(diagrams)

    # create ediaGenerator instance if a topsel or a edia file shall be created
    if args.ediaout != None or args.topselout != None:
        # remove external momenta if set to zero in the options
        diaDiagrams = ExternalLineFilter(config, verbose=not args.silent, kernels=args.kernels).filter(diaDiagrams)
        ediaGenerator = EdiaGenerator(config, verbose=not args.silent)
        ediaGenerator.generateEntries(diaDiagrams)


    # generate .edia output file
    if args.ediaout != None:
        ediaGenerator.writeEdia(args.ediaout)


    #########################################################################################################
    # From here on, the diagrams list corresponds to topologies, of which we think as mutable scalar objects.
    #########################################################################################################
    # Thus stop propgram if no topology operations are applied
    if args.topologyfolder == None \
      and args.topologyart == None \
      and args.repart == None \
      and args.topselout == None \
      and args.topology_yaml_out == None \
      and args.minimize == None \
      and args.minimize_fine == None \
      and args.topologyfolder == None:
        return

    topologies = diagrams

    # The following filters are destructive, i.e. they change the entries of "topologies".

    # remove lines of sigma particles that do not carry momentum and hence have no meaning in a topology
    topologies = SigmaLineFilter(config, topselIn=args.topselin != None, verbose=not args.silent, kernels=args.kernels).filter(topologies)

    # remove external momenta if set to zero in the options
    topologies = ExternalLineFilter(config, verbose=not args.silent, kernels=args.kernels).filter(topologies)

    # remove the bridges from the topologies to be compatible with exp mapping
    topologies = BridgeRemover(config, verbose=not args.silent, kernels=args.kernels).filter(topologies)

    # remove lines carrying the same momentum and mass
    topologies = DuplicateLineRemover(config, verbose=not args.silent, kernels=args.kernels).filter(topologies)

    # run different minimization routines, depending on whether args.minimize or args.minimize_fine is given
    if args.minimize != None or args.minimize_fine != None:
        # minimize the slow way
        if args.minimize_fine != None:
            fine = True
            fineArg = args.minimize_fine
        # minimize the fast way
        else:
            fine = False
            fineArg = args.minimize

        topselDiagrams = []
        # minimize with respect to already known topologies given via topsel file
        if fineArg != "":
            # parse topsel file
            topselReader = TopselReader(config)
            topselDiagrams = topselReader.getDiagrams(fineArg)

        topologies = TopoMinimizer(config, fine=fine, verbose=not args.silent, kernels=args.kernels).filter(topologies, topselDiagrams)


    # draw topologies
    if args.topologyart != None:
        diagramDrawer = DiagramDrawer(config, args.topologyart)
        diagramDrawer.drawTopologies(topologies)


    # draw representative diagrams with unique topologies. Use cached line lists to recover the diagram information
    if args.repart != None:
        if args.minimize == None and args.minimize_fine == None:
            minTopologies = TopoMinimizer(config, fine=False, verbose=not args.silent, kernels=args.kernels).filter(topologies)
        else:
            minTopologies = topologies
        diagramDrawer = DiagramDrawer(config, latexFileName=args.repart)
        diagramDrawer.drawDiagrams(minTopologies, useCachedTopologyLineList=True)


    # generate topsel output file
    if args.topselout != None:
        ediaGenerator.generateEntries(topologies)
        ediaGenerator.writeTopsel(args.topselout, args.ordertopsel)


    # generate YAML topology file
    if args.topology_yaml_out != None:
        YamlGenerator(config).writeTopologies(args.topology_yaml_out, topologies)


    # toggle partial fraction decomposition
    if args.partfrac_minimize != None:
        args.partfrac = True

    # generate FORM readable topology files
    if args.topologyfolder != None:
        topoFileGenerator = TopoFileGenerator(config, verbose=not args.silent, kernels=args.kernels)
        topoFileGenerator.generateTopoFiles(args.topologyfolder, "topologyList.m", "topologyList.yaml", topologies, args.partfrac, args.partfrac_minimize)



# The actual program starts here with exceptions properly handled
try:
    __main__()
# SystemExit is thrown from #legalExit(), KeyboardInterrupt comes from Ctrl+C while program execution
except (SystemExit, KeyboardInterrupt):
    print()
    exit(1)
except:
    logging.error(traceback.format_exc())
    exit(1)
