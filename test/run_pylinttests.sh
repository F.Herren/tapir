#!/bin/bash
set -e
set -v
source python3/bin/activate

cd modules

# For the beginning, check only for errors, e.g.
# reference before assignment, etc.
pylint --errors-only .

echo -e "\n\e[1;32m[PASS]\e[0;32m pylint checks succeeded!\e[m\n"
