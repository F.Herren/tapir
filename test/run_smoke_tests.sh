#!/usr/bin/env bash

# Quit on error
set -e
# Print executed commands
set -v

./tapir -s -q2e -q test/smokeTestFiles/qlist. -c test/smokeTestFiles/db1penguins.conf -do test/smokeTestFiles/tapir/db1penguins.dia -eo test/smokeTestFiles/tapir/db1penguins.edia
cat test/smokeTestFiles/tapir/db1penguins.dia
cat test/smokeTestFiles/tapir/db1penguins.edia

# tapir basic q2e-like operations with arguments passed via config file
./tapir -s -c test/smokeTestFiles/db1penguins.conf.basic
cat test/smokeTestFiles/tapir/db1penguins.dia.2
cat test/smokeTestFiles/tapir/db1penguins.edia.2

# topsel generation
./tapir -s -q test/smokeTestFiles/qlist. -c test/smokeTestFiles/db1penguins.conf -t test/smokeTestFiles/tapir/db1penguins.topsel
cat test/smokeTestFiles/tapir/db1penguins.topsel

# yaml topology output file generation from yaml diagram input. Also generate the same topsel file from before and output a qlist file
./tapir -s -dyi test/smokeTestFiles/diagramsIn.yaml -c test/smokeTestFiles/db1penguins.conf -dyo test/smokeTestFiles/tapir/topologiesOut.yaml -t test/smokeTestFiles/tapir/db1penguins2.topsel -Q test/smokeTestFiles/tapir/diagramsOut.qlist
cat test/smokeTestFiles/tapir/topologiesOut.yaml
cat test/smokeTestFiles/tapir/diagramsOut.qlist

# Compare both generated topsel files
diff -wB test/smokeTestFiles/tapir/db1penguins.topsel test/smokeTestFiles/tapir/db1penguins2.topsel

# topology file generation with partial fraction decomposition
./tapir -s -q test/smokeTestFiles/qlist. -c test/smokeTestFiles/db1penguins.conf -f test/smokeTestFiles/tapir/topologyFiles -pm
[ "$(ls test/smokeTestFiles/tapir/topologyFiles | wc -l | xargs)" == "4" ]
cat test/smokeTestFiles/tapir/topologyFiles/topologyList.m
cat test/smokeTestFiles/tapir/topologyFiles/d1l1

# topology filtering: See which momenta are present in the output files 
./tapir -s -c test/smokeTestFiles/filtertest.conf
cat test/smokeTestFiles/filtertest/out.edia | grep 'p5'
! cat test/smokeTestFiles/filtertest/out.edia | grep 'p6'
cat test/smokeTestFiles/filtertest/out.dia | grep 'p5'
! cat test/smokeTestFiles/filtertest/out.dia | grep 'p6'
cat test/smokeTestFiles/filtertest/out.topsel | grep 'p3'
! cat test/smokeTestFiles/filtertest/out.topsel | grep 'p4'
cat test/smokeTestFiles/filtertest/out.topsel | grep 'q3'
! cat test/smokeTestFiles/filtertest/out.topsel | grep 'q4'
! cat test/smokeTestFiles/filtertest/out.topsel | grep 'q5'
cat test/smokeTestFiles/filtertest/topologies/d1l1 | grep 'q3'
! cat test/smokeTestFiles/filtertest/topologies/d1l1 | grep 'q4'
! cat test/smokeTestFiles/filtertest/topologies/d1l1 | grep 'q5'
cat test/smokeTestFiles/filtertest/topologies/d1l1 | grep 'p3'
! cat test/smokeTestFiles/filtertest/topologies/d1l1 | grep 'p4'

# topsel file input
./tapir -s -i test/smokeTestFiles/testInputLongOUT.topsel -c test/smokeTestFiles/db1penguins.conf -f test/smokeTestFiles/tapir/topologyFilesFromTopsel
[ "$(ls test/smokeTestFiles/tapir/topologyFilesFromTopsel | wc -l | xargs)" == "18" ]

# single topsel input and output comparison
./tapir -s -i test/smokeTestFiles/testInputSingleIN.topsel -c test/smokeTestFiles/db1penguins.conf -f test/smokeTestFiles/tapir/topologyFilesFromSingleTopsel -t test/smokeTestFiles/testInputSingleOUT.topsel -M
[ "$(ls test/smokeTestFiles/tapir/topologyFilesFromSingleTopsel | wc -l | xargs)" == "3" ]
diff -wB test/smokeTestFiles/testInputSingleIN.topsel test/smokeTestFiles/testInputSingleOUT.topsel

# generate edia file from topsel input
./tapir -s -i test/smokeTestFiles/testInputLongOUT.topsel -c test/smokeTestFiles/db1penguins.conf -eo test/smokeTestFiles/tapir/db1penguins.edia
cat test/smokeTestFiles/tapir/db1penguins.edia

# read in a topsel file and try to minimize them with respect to an already known topsel file using two cores
./tapir -s -k 2 -i test/smokeTestFiles/testInputLongOUT.topsel -c test/smokeTestFiles/db1penguins.conf -m test/smokeTestFiles/testInputSingleIN.topsel -t test/smokeTestFiles/testMergeTopsel.topsel
cat test/smokeTestFiles/testMergeTopsel.topsel

# read in a topsel file and try to minimize them with respect to an already known topsel file using two cores, config file version
./tapir -s -c test/smokeTestFiles/db1penguins.conf.min
cat test/smokeTestFiles/testMergeTopsel.topsel.2

# Also a mapping file should be generated according to the config file
cat test/smokeTestFiles/mapping.inc
cat test/smokeTestFiles/mapping.yaml

# test q2e.blocksize in legacy mode
./tapir -s -q2e -q test/qlist.q2eCompare -c test/blocksizeTest.conf -do test/smokeTestFiles/tapirdia/blocksizeTest.dia -eo test/smokeTestFiles/tapiredia/blocksizeTest.edia
[ "$(ls test/smokeTestFiles/tapirdia | wc -l | xargs)" == "10" ]
[ "$(ls test/smokeTestFiles/tapiredia | wc -l | xargs)" == "10" ]

# draw diagrams
./tapir -s -q test/smokeTestFiles/qlist. -c test/smokeTestFiles/db1penguins.conf -da test/smokeTestFiles/tapir/diagrams.tex
cat test/smokeTestFiles/tapir/diagrams.tex

# draw diagrams with options in config file
./tapir -s -c test/smokeTestFiles/db1penguins.conf.draw
cat test/smokeTestFiles/tapir/dia.tex

# draw topologies
./tapir -s -q test/smokeTestFiles/qlist. -ta test/smokeTestFiles/tapir/topologies.tex
cat test/smokeTestFiles/tapir/topologies.tex

# generate dia and edia files with overlapping feynman rules
./tapir -s -q example/4fermi/qlist.2 -c example/4fermi/4fermi.conf -do example/4fermi/4fermitest.dia -eo example/4fermi/4fermitest.edia
[ "$(cat example/4fermi/4fermitest.edia | wc -l | xargs)" == "28" ]

# YAML config reader
./tapir -s -c test/smokeTestFiles/gg3l/gg3l.conf
./tapir -s -c test/smokeTestFiles/gg3l/gg3l.yaml
diff -wB test/smokeTestFiles/tapir/gg3l/gg3l.dia test/smokeTestFiles/tapir/gg3l/gg3lYAML.dia
diff -wB test/smokeTestFiles/tapir/gg3l/gg3l.edia test/smokeTestFiles/tapir/gg3l/gg3lYAML.edia
diff -wB test/smokeTestFiles/tapir/gg3l/topsel.out test/smokeTestFiles/tapir/gg3l/topselYAML.out

# YAML diagram reader
./tapir -s -c example/selfenergies/masslessQCD/masslessQCD_SE1l.yaml -dyo test/smokeTestFiles/tapir/masslessQSE1l_dia.yaml -do test/smokeTestFiles/tapir/masslessQSE1l.dia
diff -wB example/selfenergies/masslessQCD/1lQSE.dia test/smokeTestFiles/tapir/masslessQSE1l.dia

# UFOReader
./tapir ufo -s -ui example/UFO/SM/Standard_Model_UFO/ -uo test/smokeTestFiles/tapir/SMUFO1/
./tapir ufo -s -c test/smokeTestFiles/ufo.conf
./tapir ufo -s -c test/smokeTestFiles/ufo.yaml
diff -wB test/smokeTestFiles/tapir/SMUFO1/UFO.vrtx test/smokeTestFiles/tapir/SMUFO2/UFO.vrtx
diff -wB test/smokeTestFiles/tapir/SMUFO1/UFO.vrtx test/smokeTestFiles/tapir/SMUFOYAML/UFO.vrtx

# Partial fraction decomposition sub-module
./tapir partfrac -s -i test/smokeTestFiles/testsomode.m -f test/smokeTestFiles/tapir/partfractest
[ "$(ls test/smokeTestFiles/tapir/partfractest | wc -l | xargs)" == "5" ]
[ "$(ls test/smokeTestFiles/tapir/partfractest/topologyList.m | wc -l | xargs)" == "1" ]

# Partial fraction decomposition sub-module from yaml input with preset minimization
./tapir partfrac -s -i test/smokeTestFiles/testsomode.yaml -f test/smokeTestFiles/tapir/partfractest2 -pm test/smokeTestFiles/knownTopoList.m -k 2
[ "$(ls test/smokeTestFiles/tapir/partfractest2 | wc -l | xargs)" == "5" ]
[ "$(ls test/smokeTestFiles/tapir/partfractest2/topologyList.m | wc -l | xargs)" == "1" ]

# No diagram renumbering with several qlist input files and application of filters
# Split the diagrams
awk '/^{$/ { delim++ } { file = sprintf("test/smokeTestFiles/tapir/qlist.gg1l.part%s.txt", int(delim / 6) + 1); print > file; }' < test/smokeTestFiles/qlist.gg.1
# Generate individual config files, deleting unnecessary stuff and adjusting paths
cp test/smokeTestFiles/gg3l/gg3l.conf test/smokeTestFiles/tapir/tapir_gg1l_split1.conf
cp test/smokeTestFiles/gg3l/gg3l.conf test/smokeTestFiles/tapir/tapir_gg1l_split2.conf
grep -v -e "kernels" -e "topologyfolder" -e "topologyart" -e "diaout" -e "ediaout" -e "topselout" -e "qlist" -e "diagramart" test/smokeTestFiles/tapir/tapir_gg1l_split1.conf > test/smokeTestFiles/tapir/tapir_gg1l_split1.conf_tmp && mv test/smokeTestFiles/tapir/tapir_gg1l_split1.con{f_tmp,f}
grep -v -e "kernels" -e "topologyfolder" -e "topologyart" -e "diaout" -e "ediaout" -e "topselout" -e "qlist" -e "diagramart" test/smokeTestFiles/tapir/tapir_gg1l_split2.conf > test/smokeTestFiles/tapir/tapir_gg1l_split2.conf_tmp && mv test/smokeTestFiles/tapir/tapir_gg1l_split2.con{f_tmp,f}
echo -e "\n* tapir.filter_no_rename_diagrams" >> test/smokeTestFiles/tapir/tapir_gg1l_split1.conf
echo -e "\n* tapir.filter_no_rename_diagrams" >> test/smokeTestFiles/tapir/tapir_gg1l_split2.conf
# Remove diagrams 3 and 8
echo -e "\n* tapir.filter vertex : true : : fC,fc,g : 0,0" >> test/smokeTestFiles/tapir/tapir_gg1l_split1.conf
echo -e "\n* tapir.filter vertex : true : : g,g,sigma : 0,0" >> test/smokeTestFiles/tapir/tapir_gg1l_split1.conf
echo -e "\n* tapir.filter vertex : true : : fC,fc,g : 0,0" >> test/smokeTestFiles/tapir/tapir_gg1l_split2.conf
echo -e "\n* tapir.filter vertex : true : : g,g,sigma : 0,0" >> test/smokeTestFiles/tapir/tapir_gg1l_split2.conf
./tapir -s -c test/smokeTestFiles/tapir/tapir_gg1l_split1.conf -q test/smokeTestFiles/tapir/qlist.gg1l.part1.txt -do test/smokeTestFiles/tapir/norename1.dia -eo test/smokeTestFiles/tapir/norename1.edia -t test/smokeTestFiles/tapir/norename1.topsel
./tapir -s -c test/smokeTestFiles/tapir/tapir_gg1l_split2.conf -q test/smokeTestFiles/tapir/qlist.gg1l.part2.txt -do test/smokeTestFiles/tapir/norename2.dia -eo test/smokeTestFiles/tapir/norename2.edia -t test/smokeTestFiles/tapir/norename2.topsel
# norename1.dia should start with 1, end with 5, norename2.dia should start with 6, end with 9 (even if diagrams 3 and 8 are removed)
[ "$(cat test/smokeTestFiles/tapir/norename1.dia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "4" ] && [ "$(cat test/smokeTestFiles/tapir/norename1.dia | grep -o "d1l3" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.dia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.dia | grep -o "d1l8" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.dia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.dia | grep -o "d1l6" | sort | uniq | wc -l | xargs)" == "1" ]
[ "$(cat test/smokeTestFiles/tapir/norename1.edia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "4" ] && [ "$(cat test/smokeTestFiles/tapir/norename1.edia | grep -o "d1l3" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.edia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.edia | grep -o "d1l8" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.edia | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.edia | grep -o "d1l6" | sort | uniq | wc -l | xargs)" == "1" ]
[ "$(cat test/smokeTestFiles/tapir/norename1.topsel | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "4" ] && [ "$(cat test/smokeTestFiles/tapir/norename1.topsel | grep -o "d1l3" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.topsel | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.topsel | grep -o "d1l8" | wc -l | xargs)" == "0" ]
[ "$(cat test/smokeTestFiles/tapir/norename2.topsel | grep -o "d1l[0-9]*" | sort | uniq | wc -l | xargs)" == "3" ] && [ "$(cat test/smokeTestFiles/tapir/norename2.topsel | grep -o "d1l6" | sort | uniq | wc -l | xargs)" == "1" ]

# Clear generated test directory
rm -rf test/smokeTestFiles/tapir test/smokeTestFiles/testInputSingleOUT.topsel example/4fermi/4fermitest.dia example/4fermi/4fermitest.edia test/smokeTestFiles/tapirdia test/smokeTestFiles/tapiredia test/smokeTestFiles/mapping.inc test/smokeTestFiles/mapping.yaml test/smokeTestFiles/testsomodeout1 test/smokeTestFiles/testsomodeout2

echo -e "\n\e[1;32m[PASS]\e[0;32m Smoke tests succeeded!\e[m\n"
