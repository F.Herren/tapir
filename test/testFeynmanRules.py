import unittest
import logging
from modules.diagram import Diagram, Vertex, Line
from modules.config import Conf 
from modules.feynmanRules import FeynmanRules
from modules.feynmanRulesReader import FeynmanRulesReader
import sympy as sy
import re
import copy


class testFeynmanRules(unittest.TestCase):
    def test_ReadFeynmanRules(self):
        config = Conf("test/4fermi.conf", False)
        feynmanRulesReader = FeynmanRulesReader(config)

        # Propagators
        self.assertTrue({('fb', 'fB'): ['*fprop(<momentum>,<spinor_index_vertex_1>,<spinor_index_vertex_2>)'],}.items() <= feynmanRulesReader.feynmanRules.propLorentz.items())
        self.assertTrue({('fs', 'fS'): ['*fprop(<momentum>,<spinor_index_vertex_1>,<spinor_index_vertex_2>)'],}.items() <= feynmanRulesReader.feynmanRules.propLorentz.items())
        self.assertTrue({('g', 'g'): ['*Dg1(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>)', '*Dg2(<lorentz_index_vertex_1>,<lorentz_index_vertex_2>,<momentum>)']}.items() <= feynmanRulesReader.feynmanRules.propLorentz.items())

        self.assertTrue({('fb', 'fB'): ['*d(<fundamental_index_vertex_1>,<fundamental_index_vertex_2>)']}.items() <= feynmanRulesReader.feynmanRules.propQCD.items())
        self.assertTrue({('fs', 'fS'): ['*d(<fundamental_index_vertex_1>,<fundamental_index_vertex_2>)']}.items() <= feynmanRulesReader.feynmanRules.propQCD.items())
        self.assertTrue({('g', 'g'): ['*prop(<colour_index_vertex_1>,<colour_index_vertex_2>)', '*prop(<colour_index_vertex_1>,<colour_index_vertex_2>)']}.items() <= feynmanRulesReader.feynmanRules.propQCD.items())
       
        self.assertTrue({('fb', 'fB'): ['']}.items() <= feynmanRulesReader.feynmanRules.propQED.items())
        self.assertTrue({('fs', 'fS'): ['']}.items() <= feynmanRulesReader.feynmanRules.propQED.items())
        self.assertTrue({('g', 'g'): ['', '']}.items() <= feynmanRulesReader.feynmanRules.propQED.items())

        self.assertTrue({('fb', 'fB'): ['']}.items() <= feynmanRulesReader.feynmanRules.propEW.items())
        self.assertTrue({('fs', 'fS'): ['']}.items() <= feynmanRulesReader.feynmanRules.propEW.items())
        self.assertTrue({('g', 'g'): ['', '']}.items() <= feynmanRulesReader.feynmanRules.propEW.items())

        # Vertices
        self.assertTrue({('fb', 'fs', 'fB', 'fS'): ['*C1*fvertV(<local_index_rho>,<spinor_index_particle_1>,<spinor_index_particle_2>)*fvertV(<local_index_rho>,<spinor_index_particle_3>,<spinor_index_particle_4>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxLorentz.items())
        self.assertTrue({('fb', 'fB', 'g'): ['*C1*fvertV(<lorentz_index_particle_3>,<spinor_index_particle_1>,<spinor_index_particle_2>)', '*C2*fvertV(<lorentz_index_particle_3>,<spinor_index_particle_1>,<spinor_index_particle_2>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxLorentz.items())
        self.assertTrue({('fs', 'fS', 'g'): ['*fvertV(<lorentz_index_particle_3>,<spinor_index_particle_1>,<spinor_index_particle_2>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxLorentz.items())

        self.assertTrue({('fb', 'fs', 'fB', 'fS'): ['*GM(<local_index_jj>,<fundamental_index_particle_1>,<fundamental_index_particle_2>)*GM(<local_index_jj>,<fundamental_index_particle_3>,<fundamental_index_particle_4>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxQCD.items())
        self.assertTrue({('fb', 'fB', 'g'): ['*GM(<colour_index_particle_3>,<fundamental_index_particle_1>,<fundamental_index_particle_2>)', '*GM(<colour_index_particle_3>,<fundamental_index_particle_1>,<fundamental_index_particle_2>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxQCD.items())
        self.assertTrue({('fs', 'fS', 'g'): ['*GM(<colour_index_particle_3>,<fundamental_index_particle_1>,<fundamental_index_particle_2>)']}.items() <= feynmanRulesReader.feynmanRules.vrtxQCD.items())

        self.assertTrue({('fb', 'fs', 'fB', 'fS'): ['']}.items() <= feynmanRulesReader.feynmanRules.vrtxQED.items())
        self.assertTrue({('fb', 'fB', 'g'): ['', '']}.items() <= feynmanRulesReader.feynmanRules.vrtxQED.items())
        self.assertTrue({('fs', 'fS', 'g'): ['']}.items() <= feynmanRulesReader.feynmanRules.vrtxQED.items())

        self.assertTrue({('fb', 'fs', 'fB', 'fS'): ['']}.items() <= feynmanRulesReader.feynmanRules.vrtxEW.items())
        self.assertTrue({('fb', 'fB', 'g'): ['', '']}.items() <= feynmanRulesReader.feynmanRules.vrtxEW.items())
        self.assertTrue({('fs', 'fS', 'g'): ['']}.items() <= feynmanRulesReader.feynmanRules.vrtxEW.items())

        self.assertDictEqual(feynmanRulesReader.getAntiParticleDict(), {'fb': 'fB', 'fB': 'fb', 'fs': 'fS', 'fS': 'fs', 'g': 'g', 'fmu': 'fMu', 'fMu': 'fmu', 'fe1': 'fE1', 'fE1': 'fe1'})