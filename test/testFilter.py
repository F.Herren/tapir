from modules.externalLineFilter import ExternalLineFilter
from modules.diagramFilter import DiagramFilter
import unittest
import logging
from modules.diagram import Diagram, Vertex, Line
from modules.config import Conf 
from modules.topoMinimizer import TopoMinimizer
from modules.topoAnalyzer import TopologyAnalyzer, BridgeFinder
from modules.bridgeRemover import BridgeRemover
from modules.duplicateLineRemover import DuplicateLineRemover
from modules.sigmaLineFilter import SigmaLineFilter
from modules.cutFilter import CutFilter
from modules.vertexFilter import VertexFilter
import sympy as sy
import re
import copy
import itertools as it


class testFilter(unittest.TestCase):
    def test_BridgeRemover(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
        conf = Conf("test/qqqq.conf", False)
        conf.topoIgnoreBridges = True
        conf.externalMomenta = {"q1": "q", "q2": "-q"}

        d = Diagram(conf)
        d.internalMomenta = [["p1", 1, 4, "fq", "fQ", 0,0, ""], ["p2", 6, 6, "W", "W", 0,0, "M1"], ["p3", 2, 6, "W", "W", 0,0, "M1"], ["p4", 3, 2, "fq", "fQ", 0,0, ""],  ["p5", 2, 3, "fq", "fQ", 0,0, ""],  ["p6", 4, 3, "W", "W", 0,0, "M1"],  ["p7", 5, 4, "fq", "fQ", 0,0, ""]]
        d.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 5, "fq", 0]]
        d.numLoops = 2
        d.diagramNumber = 1
        d.createTopologicalVertexAndLineLists()

        d = BridgeRemover(conf).filter([d])[0]

        # We switched off momentum renaming after the filter, so that only the vertices are relabelled...
        self.assertListEqual(d.internalMomenta, [["p2", 2, 2, "W", "W", 0,0, "M1"], ["p4", 1, 2, "fq", "fQ", 0,0, ""],  ["p5", 2, 1, "fq", "fQ", 0,0, ""]])
        self.assertListEqual(d.externalMomenta, [["-q1", 1, "fq", 0], ["-q2", 1, "fq", 0]])


    def test_SigmaLineFilter(self):
        conf = Conf("test/qqqq.conf", False)
        conf.sigmaParticles = ["s1", "S2"]
        conf.externalMomenta = {"q1": "q", "q2": "-q"}

        d = Diagram(conf)
        d.internalMomenta = [["p1", 1, 2, "s1", "S1", 0,0, ""], ["p2", 2, 3, "fq", "fQ", 0,0, "M1"], ["p3", 3, 4, "s2", "S2", 0,0, ""], ["p4", 3, 2, "fq", "fQ", 0,0, "M1"]]
        d.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fq", 0],["-q3", 3, "fq", 0], ["-q4", 4, "fq", 0]]
        d.numLoops = 1
        d.diagramNumber = 1
        d.createTopologicalVertexAndLineLists()

        d = SigmaLineFilter(conf, topselIn=False).filter([d])[0]

        # We switched off momentum renaming after the filter, so that only the vertices are relabelled...
        self.assertListEqual(d.internalMomenta, [["p2", 1, 2, "fq", "fQ", 0,0, "M1"], ["p4", 2, 1, "fq", "fQ", 0,0, "M1"]])
        self.assertListEqual(d.externalMomenta, [["-q1", 1, "fq", 0], ["-q2", 1, "fq", 0],["-q3", 2, "fq", 0], ["-q4", 2, "fq", 0]])


    def test_DiagramLineContraction(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
        conf = Conf("test/qqqq.conf", False)
        # conf.externalMomenta = {"q1": "q", "q2": "-q"}
        d = Diagram(conf)
        d.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, ""], ["p2", 1, 3, "W", "W", 0,0, "M1"], ["p3", 4, 2, "W", "W", 0,0, "M1"], ["p4", 3, 4, "fq", "fQ", 0,0, ""], ["p5", 2, 3, "fq", "fQ", 0,0, ""]]
        d.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 4, "fq", 0]]
        d.numLoops = 2
        d.diagramNumber = 1
        d.createTopologicalVertexAndLineLists()

        d.contractLines([1,4])

        # We switched off momentum renaming after the filter, so that only the vertices are relabelled...
        self.assertListEqual(d.internalMomenta, [["p2", 1, 2, "W", "W", 0,0, "M1"], ["p3", 2, 1, "W", "W", 0,0, "M1"], ["p5", 1, 2, "fq", "fQ", 0,0, ""],])
        self.assertListEqual(d.externalMomenta, [["-q1", 1, "fq", 0], ["-q2", 2, "fq", 0]])


    def test_ExternalLineFilter_UnremovedLine(self):
        box = Diagram()
        box.internalMomenta = [["p1", 1, 2, "W", "W", 0,0, "M1"], ["p2", 3, 4, "W", "W", 0,0, "M1"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        box.diagramNumber = 1
        box.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.externalMomenta = {"q1" : "0", "q4" : 0}

        extFilter = ExternalLineFilter(conf)

        boxCopy = extFilter.filter([box])[0]

        self.assertEqual(boxCopy, box)
        self.assertListEqual(sorted([p[0] for p in boxCopy.externalMomenta]), sorted(["-q2", "-q3"]))
        self.assertEqual(len([1 for l in boxCopy.topologicalLineList if not l.external]), 4)
        self.assertEqual(len([1 for l in boxCopy.topologicalLineList if l.external]), 2)


    # def test_ExternalLineFilter_RemovedInternalLine(self):
    #     box = Diagram()
    #     box.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, ""], ["p2", 3, 4, "fq", "fQ", 0,0, ""], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
    #     box.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
    #     box.diagramNumber = 1
    #     box.createTopologicalVertexAndLineLists()

    #     conf = Conf("test/qqqq.conf", False)
    #     conf.externalMomenta = {"q1" : "0", "q4" : 0}

    #     extFilter = ExternalLineFilter(conf)

    #     boxCopy = extFilter.filter([box])[0]

    #     self.assertNotEqual(boxCopy, box)
    #     self.assertListEqual(sorted([p[0] for p in boxCopy.externalMomenta]), sorted(["-q2", "-q3"]))
    #     self.assertEqual(len([1 for l in boxCopy.topologicalLineList if not l.external]), 2)
    #     self.assertEqual(len([1 for l in boxCopy.topologicalLineList if l.external]), 2)


    def test_lineCutTreePartitioning(self):
        l1 = Line(1,-1,1)
        l1.external = True

        l2 = Line(2,-2,2)
        l2.external = True

        l3 = Line(3,1,3)
        l4 = Line(4,3,2)
        l5 = Line(5,3,4)
        l6 = Line(6,5,4)

        diagramFilter = DiagramFilter(Conf("test/qqqq.conf", False))

        partitions = diagramFilter.lineCutTreePartitioning([l1,l2,l3,l4,l5,l6],l5)

        self.assertListEqual(partitions, [{4, 5}, {1, 2, 3, -2, -1}])


    def initializeDiagrams(self):
    # normal box diagram
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, "M1"], ["p2", 3, 4, "g", "g", 0,0, "M1"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        d1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        # self energy diagram without mixing
        d2 = Diagram()
        d2.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 1, "g", "g", 0,0, ""]]
        d2.externalMomenta = [["-q1", 1, "ft1", 0], ["-q2", 2, "fT1", 0]]
        d2.diagramNumber = 2
        d2.diagramID = d2.diagramNumber
        d2.createTopologicalVertexAndLineLists()

        # self energy diagram with mixing
        d3 = Diagram()
        d3.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 1, "g", "g", 0,0, ""]]
        d3.externalMomenta = [["-q1", 1, "ft1", 0], ["-q2", 2, "fT2", 0]]
        d3.diagramNumber = 3
        d3.diagramID = d3.diagramNumber
        d3.createTopologicalVertexAndLineLists()

        # 4 point function with external line mixing due to tadpole
        d4 = Diagram()
        d4.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 3, "fq", "fQ", 0,0, "M1"], ["p3", 3, 4, "sigma", "sigma", 0,0, "M1"], ["p4", 4, 4, "ft2", "fT2", 0,0, ""]]
        d4.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fQ", 0], ["-q3", 2,  "fq", 0], ["-q4", 3, "fT1", 0]]
        d4.diagramNumber = 4
        d4.diagramID = d4.diagramNumber
        d4.createTopologicalVertexAndLineLists()

        # 4 point function with tadpole but without mixing
        d5 = Diagram()
        d5.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 3, "fq", "fQ", 0,0, "M1"], ["p3", 3, 4, "sigma", "sigma", 0,0, "M1"], ["p4", 4, 4, "ft2", "fT2", 0,0, ""]]
        d5.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fQ", 0], ["-q3", 2,  "fq", 0], ["-q4", 3, "fQ", 0]]
        d5.diagramNumber = 5
        d5.diagramID = d5.diagramNumber
        d5.createTopologicalVertexAndLineLists()

        # two point function with and without mixing self energy 
        d6 = Diagram()
        d6.internalMomenta = [["p1", 1, 3, "ft1", "fT1", 0,0, ""], ["p2", 1, 3, "sigma", "sigma", 0,0, "M1"], ["p3", 3, 4, "ft2", "fT2", 0,0, ""], ["p4", 4, 2, "ft2", "fT2", 0,0, ""], ["p5", 2, 4, "sigma", "sigma", 0,0, "M1"]]
        d6.externalMomenta = [["-q1", 1, "ft1", 0], ["-q2", 2, "fT2", 0]]
        d6.diagramNumber = 6
        d6.diagramID = d6.diagramNumber
        d6.createTopologicalVertexAndLineLists()

        # 4 point function with external line mixing and self energy afterwards
        d7 = Diagram()
        d7.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 3, "fq", "fQ", 0,0, "M1"], ["p3", 2, 3, "g", "g", 0,0, "M1"], ["p4", 3, 4, "fq", "fQ", 0,0, "M1"], ["p5", 4, 5, "sigma", "sigma", 0,0, "M1"], ["p6", 5, 5, "ft2", "fT2", 0,0, ""]]
        d7.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fQ", 0], ["-q3", 1,  "fq", 0], ["-q4", 4, "fT1", 0]]
        d7.diagramNumber = 7
        d7.diagramID = d7.diagramNumber
        d7.createTopologicalVertexAndLineLists()

        # 4 point function with external self energy and line mixing and afterwards
        d8 = Diagram()
        d8.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 4, 3, "ft1", "fT1", 0,0, "M1"], ["p3", 4, 3, "g", "g", 0,0, "M1"], ["p4", 3, 2, "ft1", "fT1", 0,0, "M1"], ["p5", 2, 5, "sigma", "sigma", 0,0, "M1"], ["p6", 5, 5, "ft2", "fT2", 0,0, ""]]
        d8.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "fQ", 0], ["-q3", 1,  "fq", 0], ["-q4", 4, "fT1", 0]]
        d8.diagramNumber = 8
        d8.diagramID = d8.diagramNumber
        d8.createTopologicalVertexAndLineLists()

        # 4 point function with one mixing of internal lines
        d9 = Diagram()
        d9.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 4, "ft1", "fT1", 0,0, "M1"], ["p3", 2, 3, "sigma", "sigma", 0,0, "M1"], ["p4", 3, 3, "ft2", "fT2", 0,0, ""]]
        d9.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "g", 0], ["-q3", 4,  "g", 0], ["-q4", 4, "fT1", 0]]
        d9.diagramNumber = 9
        d9.diagramID = d9.diagramNumber
        d9.createTopologicalVertexAndLineLists()

        # 4 point function with one self-energy of internal lines
        d10 = Diagram()
        d10.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 4, "fq", "fQ", 0,0, "M1"], ["p3", 2, 3, "sigma", "sigma", 0,0, "M1"], ["p4", 3, 3, "ft2", "fT2", 0,0, ""]]
        d10.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "g", 0], ["-q3", 4,  "g", 0], ["-q4", 4, "fQ", 0]]
        d10.diagramNumber = 10
        d10.diagramID = d10.diagramNumber
        d10.createTopologicalVertexAndLineLists()

        # 4 point function with one self-energy and one mixing of internal lines
        d11 = Diagram()
        d11.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 5, "ft1", "fT1", 0,0, "M1"], ["p3", 2, 3, "sigma", "sigma", 0,0, "M1"], ["p4", 3, 3, "ft2", "fT2", 0,0, ""], ["p5", 5, 6, "sigma", "sigma", 0,0, "M1"], ["p6", 6, 6, "ft2", "fT2", 0,0, ""], ["p7", 5, 4, "ft1", "fT1", 0,0, "M1"]]
        d11.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 1, "g", 0], ["-q3", 4,  "g", 0], ["-q4", 4, "fT1", 0]]
        d11.diagramNumber = 11
        d11.diagramID = d11.diagramNumber
        d11.createTopologicalVertexAndLineLists()

        return [d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11]

    def test_selfEnergyBridgeMixFilter_Mixing(self):
        diagrams = self.initializeDiagrams()

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["self_energy_bridge_mixing", "true"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1, 2, 3, 4, 5, 6, 7])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [3, 4, 6, 7, 8, 9, 11])

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["self_energy_bridge_mixing", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1, 2, 3, 4])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [1,2,5,10])


    def test_selfEnergyBridgeMixFilter_NonMixing(self):
        diagrams = self.initializeDiagrams()

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["self_energy_bridge", "true"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4,5,6,7])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [2,5,6,7,8,10,11])

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["self_energy_bridge", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [1,3,4,9])


    def test_selfEnergyBridgeMixFilter_Mixing_External(self):
        diagrams = self.initializeDiagrams()

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["external_self_energy_bridge_mixing", "true"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [3,4,6,7])

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["external_self_energy_bridge_mixing", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4,5,6,7])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [1,2,5,8,9,10,11])


    def test_selfEnergyBridgeMixFilter_NonMixing_External(self):
        diagrams = self.initializeDiagrams()

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["external_self_energy_bridge", "true"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [2,5,6,8])

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["external_self_energy_bridge", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)

        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4,5,6,7])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [1,3,4,7,9,10,11])


    def test_multipleFilters(self):
        diagrams = self.initializeDiagrams()

        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["self_energy_bridge", "true"], ["self_energy_bridge_mixing", "true"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3,4])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [6,7,8,11])

        conf.filters = [["self_energy_bridge", "true"], ["self_energy_bridge_mixing", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2,3])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [2,5,10])

        conf.filters = [["external_self_energy_bridge_mixing", "true"], ["self_energy_bridge", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1,2])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [3,4])

        conf.filters = [["external_self_energy_bridge_mixing", "true"], ["self_energy_bridge", "true"], ["external_self_energy_bridge", "false"]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [7])


    def test_duplicateLineRemover(self):
        # 2-point function box
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 3, "fq", "fQ", 0,0, "M1"], ["p3", 3, 4, "fq", "fQ", 0,0, "M1"], ["p4", 4, 1, "ft1", "fT1", 0,0, ""]]
        d1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0]]
        d1.diagramNumber = 1
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.topoRemoveDuplicateLines = True
        
        d1F = DuplicateLineRemover(conf).filter([d1])[0]

        self.assertEqual(len(d1F.internalMomenta), 3)
        self.assertListEqual(d1F.externalMomenta, [['-q1', 1, 'g', 0], ['-q2', 2, 'g', 0]])
        self.assertListEqual(d1F.internalMomenta, [['p1', 1, 2, 'fq', 'fQ', 0, 0, 'M1'], ['p2', 2, 3, 'fq', 'fQ', 0, 0, 'M1'], ['p3', 3, 1, 'ft1', 'fT1', 0, 0, '']])


    def test_parseCutFilterOptions(self):
        conf = Conf("test/qqqq.conf", False)
        filterOptions = CutFilter(conf).parseFilterOptions([ [ "true", "q2", "h1,h2", "1,1"], ["false" , "", "",""], ["1" , "q1" , "ft" , "0,0, 1,14"], [ "f", "q2", "h1,h2", "0,2,4,15"], [ "f", "q2", "h1,h2", "0,2, 2,4, 7,8, 6,10, 9,11, 12,12"] ], ["q1", "-q2"])
        self.assertListEqual(filterOptions, [[True, ["q2"], ["h1","h2"], [[1,1]]], [True, ["q1"], [], [[0,0]]], [True, ["q1"], ["ft"], [[0,0],[1,14]]], [True, ["q2"], ["h1","h2"], [[3,3],[16,1000]]], [True, ["q2"], ["h1","h2"], [[5,5],[13,1000]]]])


    def test_parseCutFilterOptions2(self):
        conf = Conf("test/qqqq.conf", False)
        filterOptions = CutFilter(conf).parseFilterOptions([ [ "true", "q1,q2", "h", "2,2"], ["1" , "q1,q2" , "g,fq,c" , "0,2"], ["1", "q1,q2", "", "2,4" ]], ["q1", "q2", "-q3", "-q4"])
        self.assertListEqual(filterOptions, [[True, ["q1","q2"], ["h"], [[2,2]]], [True, ["q1", "q2"], ["g", "fq", "c"], [[0,2]]], [True, ["q1","q2"], [], [[2,4]]]])

    def test_parseVertexFilterOptions(self):
        conf = Conf("test/qqqq.conf", False)
        filterOptions = VertexFilter(conf).parseFilterOptions([ [ "true", "q2", "h1,h2,h3", "1,1"], ["1" , "q1" , "ft,fT,h1" , "0,0, 1,14"], [ "f", "q2", "h1,h2,h3;h1,h2,h4", "0,2,4,15"], [ "f", "q2", "h1,h2,h3", "0,2, 2,4, 7,8, 6,10, 9,11, 12,12"] ])
        # Note: the second element of each sublist (the external momenta) are ignored anyway for the vertexFilter
        self.assertListEqual(filterOptions, [[True, "q2", [["h1","h2","h3"]], [[1,1]]], [True, "q1", [["ft","fT","h1"]], [[0,0],[1,14]]], [True, "q2", [["h1","h2","h3"], ["h1","h2","h4"]], [[3,3],[16,1000]]], [True, "q2", [["h1","h2","h3"]], [[5,5],[13,1000]]]])

    def test_parseVertexFilterOptions2(self):
        conf = Conf("test/qqqq.conf", False)
        filterOptions = VertexFilter(conf).parseFilterOptions([ [ "true", "q1,q2", "h,W,Z", "2,2"], ["1" , "q1,q2" , "g,fq,c" , "0,2"] ])
        self.assertListEqual(filterOptions, [[True, "q1,q2", [["h","W","Z"]], [[2,2]]], [True, "q1,q2", [["g", "fq", "c"]], [[0,2]]]])

    def test_createMultipleEdgeClusters(self):
        # 2-point function with two mutli edges
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "fq", "fQ", 0,0, "M1"], ["p2", 2, 1, "fq", "fQ", 0,0, "M1"], ["p3", 2, 3, "fq", "fQ", 0,0, "M1"], ["p4", 1, 3, "ft1", "fT1", 0,0, ""], ["p5", 1, 2, "fq", "fQ", 0,0, "M1"], ["p6", 3, 1, "fq", "fQ", 0,0, "M1"]]
        d1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0]]
        d1.diagramNumber = 1
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        self.assertDictEqual(CutFilter(conf).createMultipleEdgeClusters(d1.topologicalLineList), {1:[2,5], 2:[1,5], 5:[1,2], 4:[6], 6:[4]})


    def test_CutFilterDiagramMatchesParticleNumberConstraints(self):
        # 4-point function box
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 3, 4, "g", "g", 0,0, ""], ["p3", 3, 1, "fq", "fQ", 0,0, "M1"], ["p4", 4, 2, "fq", "fQ", 0,0, "M1"]]
        d1.externalMomenta = [["q1", 1, "fq", 0], ["q2", 3, "fQ", 0], ["-q3", 2,  "fq", 0], ["-q4", 4, "fQ", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        filterOptions = [[True, ["q1","q2"], ["fq"], [[1,2]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))

        filterOptions = [[True, ["q1","q2"], ["fq"], [[0,0],[2,5]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))

        filterOptions = [[True, ["q1","q2"], ["ft"], [[0,0],[2,5]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))

        filterOptions = [[True, ["q1","q2"], ["ft"], [[1,1],[2,5]]]]
        self.assertFalse(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))

        filterOptions = [[True, ["q1","q2"], ["fq"], [[5,7]]]]
        self.assertFalse(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))
        
        
    def test_CutFilterDiagramMatchesParticleNumberConstraints2(self):
        # 2-loop di-Higgs PS diagram
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 5, "h", "h", 0,0, "M2"], ["p3", 5, 4, "h", "h", 0,0, "M2"], ["p4", 5, 4, "h", "h", 0,0, "M2"], ["p5", 4, 6, "g", "g", 0,0, ""], ["p6", 6, 3, "g", "g", 0,0, ""], ["p7", 3, 7, "fq", "fQ", 0,0, ""],\
        ["p8", 7, 1, "fq", "fQ", 0,0, ""], ["p9", 6, 7, "g", "g", 0,0, ""]]
        d1.externalMomenta = [["q1", 1, "fq", 0], ["q2", 2, "g", 0], ["-q3", 3,  "fQ", 0], ["-q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        filterOptions = [[True, ["q1","q2"], ["h"], [[2,2]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))

        filterOptions = [[True, ["q1","q2"], [], [[3,4]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))
        
        filterOptions = [[True, ["q1","q2"], ["fq"], [[3,3]]]]
        self.assertFalse(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))
        
        filterOptions = [[True, ["q1","q2"], ["g", "fq"], [[1,2]]]]
        self.assertTrue(CutFilter(conf).diagramMatchesParticleNumberConstraints(d1.topologicalLineList, filterOptions))


    def test_cutLinesMatchFilterConstraints(self):
        conf = Conf("test/qqqq.conf", False)

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[1,1]]]]
        self.assertFalse(CutFilter(conf).cutLinesMatchFilterConstraints([["h","h"], ["h","h"]], filterOptions))

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[1,1]]], [True, ["q1","q2"], ["h"], [[0,5]]]]
        self.assertTrue(CutFilter(conf).cutLinesMatchFilterConstraints([["h","h"], ["h","h"], ["fq","fQ"]], filterOptions))

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[0,0],[2,10]]], [True, ["q1","q2"], ["h"], [[0,5]]]]
        self.assertFalse(CutFilter(conf).cutLinesMatchFilterConstraints([["h","h"], ["h","h"], ["fq","fQ"]], filterOptions))

        filterOptions = [[True, ["q1","q2"], [], [[0,3]]]]
        self.assertTrue(CutFilter(conf).cutLinesMatchFilterConstraints([["h","h"], ["h","h"], ["fq","fQ"]], filterOptions))

        filterOptions = [[True, ["q1","q2"], [], [[0,3]]]]
        self.assertFalse(CutFilter(conf).cutLinesMatchFilterConstraints([["h","h"], ["h","h"], ["fq","fQ"], ["fq","fQ"]], filterOptions))
        

    def test_invertIntervals(self):
        conf = Conf("test/qqqq.conf", False)
        self.assertListEqual( CutFilter(conf).invertIntervals([[1,3]], 10), [[0,0], [4,10]] )
        self.assertListEqual( CutFilter(conf).invertIntervals([[0,0], [7,10]], 10), [[1,6]] )
        self.assertListEqual( CutFilter(conf).invertIntervals([[2,2], [3,4], [7,9]], 10), [[0,1],[5,6], [10,10]] )
        self.assertListEqual( CutFilter(conf).invertIntervals([], 10), [[0,10]] )
        self.assertListEqual( CutFilter(conf).invertIntervals([[0,5],[6,10]], 10), [] )


    def test_getTypeNLines(self):
        # 2-loop di-Higgs PS diagram
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 5, "h", "h", 0,0, "M2"], ["p3", 5, 4, "h", "h", 0,0, "M2"], ["p4", 5, 4, "h", "h", 0,0, "M2"], ["p5", 4, 6, "g", "g", 0,0, ""], ["p6", 6, 3, "g", "g", 0,0, ""], ["p7", 3, 7, "fq", "fQ", 0,0, ""],\
        ["p8", 7, 1, "fq", "fQ", 0,0, ""], ["p9", 6, 7, "g", "g", 0,0, ""]]
        d1.externalMomenta = [["q1", 1, "fq", 0], ["q2", 2, "g", 0], ["-q3", 3,  "fQ", 0], ["-q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[1,1]]]]
        self.assertSetEqual(CutFilter(conf).getTypeNLines(d1.topologicalLineList, filterOptions), {-1,-2,-3,-4})

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[0,0]]]]
        self.assertSetEqual(CutFilter(conf).getTypeNLines(d1.topologicalLineList, filterOptions), {-1,-2,-3,-4,1,5,6,7,8,9})

        filterOptions = [[True, ["q1","q2"], ["h"], [[0,0]]]]
        self.assertSetEqual(CutFilter(conf).getTypeNLines(d1.topologicalLineList, filterOptions), {-1,-2,-3,-4,2,3,4})

        filterOptions = [[True, ["q1","q2"], ["h"], [[0,1]]]]
        self.assertSetEqual(CutFilter(conf).getTypeNLines(d1.topologicalLineList, filterOptions), {-1,-2,-3,-4})

        filterOptions = [[True, ["q1","q2"], ["h"], [[1,1]]]]
        self.assertSetEqual(CutFilter(conf).getTypeNLines(d1.topologicalLineList, filterOptions), {-1,-2,-3,-4})


    def test_getTypeMLines(self):
        # 2-loop di-Higgs PS diagram
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, ""], ["p2", 2, 5, "h", "h", 0,0, "M2"], ["p3", 5, 4, "h", "h", 0,0, "M2"], ["p4", 5, 4, "h", "h", 0,0, "M2"], ["p5", 4, 6, "g", "g", 0,0, ""], ["p6", 6, 3, "g", "g", 0,0, ""], ["p7", 3, 7, "fq", "fQ", 0,0, ""],\
        ["p8", 7, 1, "fq", "fQ", 0,0, ""], ["p9", 6, 7, "g", "g", 0,0, ""]]
        d1.externalMomenta = [["q1", 1, "fq", 0], ["q2", 2, "g", 0], ["-q3", 3,  "fQ", 0], ["-q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[1,1]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), set())

        filterOptions = [[True, ["q1","q2"], ["g","fq"], [[6,8]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), {1,5,6,7,8,9})

        filterOptions = [[True, ["q1","q2"], ["h"], [[1,5]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), set())

        filterOptions = [[True, ["q1","q2"], ["h"], [[3,4]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), {2,3,4})

        filterOptions = [[True, ["q1","q2"], [], [[8,9]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), set())

        filterOptions = [[True, ["q1","q2"], [], [[9,10]]]]
        self.assertSetEqual(CutFilter(conf).getTypeMLines(d1.topologicalLineList, filterOptions), {1,2,3,4,5,6,7,8,9})


    def test_cutFilter1lBox(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)
        
        # 2-point function box
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 2, "g", "g", 0,0, "M1"], ["p2", 3, 4, "g", "g", 0,0, "M1"], ["p3", 3, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 2, "fq", "fQ", 0,0, ""]]
        d1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        d2 = Diagram()
        d2.internalMomenta = [["p1", 1, 3, "g", "g", 0,0, "M1"], ["p2", 2, 4, "g", "g", 0,0, "M1"], ["p3", 2, 1, "fq", "fQ", 0,0, ""], ["p4", 4, 3, "fq", "fQ", 0,0, ""]]
        d2.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 3, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 4, "g", 0]]
        d2.diagramNumber = 2
        d2.diagramID = d2.diagramNumber
        d2.createTopologicalVertexAndLineLists()

        diagrams = [d1, d2]

        conf = Conf("test/qqqq.conf", False)
        # conf.filters = [  [ "cuts",[ "true", "q1,q2", "h1,h2", "1,1"], ["false" , "", "",""], ["1" , "q1,q2" , "ft" , "0,0, 1,14"] ]  ]
        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "2,2"] ]  ]

        filteredDiagrams = DiagramFilter(conf).filter(diagrams)

        self.assertListEqual(filteredDiagrams, [d2])


    def test_findNumberOfConnectedComponents(self):
        self.assertEqual(CutFilter.findNumberOfConnectedComponents([ [1,2], [2,3], [2,3], [4,1], [4,2] ]), 1)
        self.assertEqual(CutFilter.findNumberOfConnectedComponents([ [1,2], [5,3], [5,3], [4,1], [4,2] ]), 2)
        self.assertEqual(CutFilter.findNumberOfConnectedComponents([ [1,2], [2,3], [3,1], [4,5], [5,4], [6,7] ]), 3)
        self.assertEqual(CutFilter.findNumberOfConnectedComponents([ [1,8], [2,7], [3,7], [4,5], [9,10], [12,10] ]), 4)


    def test_cutFilter2lBox(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 2-point function box
        d1 = Diagram()
        d1.internalMomenta = [["p1", 1, 5, "g", "g", 0,0, ""], ["p2", 1, 2, "fq", "fQ", 0,0, "M1"], ["p3", 2, 6, "fQ", "fq", 0,0, "M1"], ["p4", 6, 5, "fq", "fQ", 0,0, "M1"], ["p5", 5, 3, "fq", "fQ", 0,0, "M1"], ["p6", 3, 4, "fq", "fQ", 0,0, "M1"], ["p7", 4, 6, "g", "g", 0,0, ""]]
        d1.externalMomenta = [["-q1", 1, "fq", 0], ["-q2", 2, "g", 0], ["-q3", 3,  "g", 0], ["-q4", 4, "fQ", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "5,5"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "4,4"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "3,3"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [d1])

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "2,2"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "fq", "1,1"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [d1])



    def test_cutFilter3lDiagram(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 2-point function box
        d1 = Diagram()
        d1.internalMomenta = [["p1",4,1,"g","g",0,0,""], ["p2",5,2,"g","g",0,0,""], ["p3",3,3,"fT","ft",0,0,""], ["p4",6,3,"huc","huc",0,0,""], ["p5",7,4,"fT","ft",0,0,""], ["p6",4,8,"fT","ft",0,0,""], ["p7",5,7,"fT","ft",0,0,""], ["p8",8,5,"fT","ft",0,0,""], ["p9",7,6,"h","h",0,0,""], ["p10",8,6,"h","h",0,0,""]]
        d1.externalMomenta = [["-q1", 1, "g", 0], ["-q2", 1, "g", 0], ["-q3", 2,  "g", 0], ["-q4", 2, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "h", "2,2"], ["true", "q1,q2", "ft,g,c,fq,huc", "0,0"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])
        
    def test_cutFilter4lDiagram1(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 4-point function
        d1 = Diagram()
        d1.internalMomenta = [["p1",1,2,"ft","fT",0,0,""], ["p2",2,5,"ft","fT",0,0,""], ["p3",5,6,"ft","fT",0,0,""], ["p4",6,7,"ft","fT",0,0,""], ["p5",7,1,"ft","fT",0,0,""], ["p6",3,4,"ft","fT",0,0,""], ["p7",4,8,"ft","fT",0,0,""], ["p8",8,9,"ft","fT",0,0,""], ["p9",9,10,"ft","fT",0,0,""],["p10",10,3,"ft","fT",0,0,""], ["p11",9,6,"h","h",0,0,""], ["p12",5,8,"h","h",0,0,""], ["p13",7,10,"g","g",0,0,""]]
        d1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 3,  "g", 0], ["q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "h", "2,2"], ["true", "q1,q2", "ft,g,c,fq,huc", "0,0"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])
        
    def test_cutFilter4lDiagram2(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 4-point function
        d1 = Diagram()
        d1.internalMomenta = [["p1",1,2,"ft","fT",0,0,""], ["p2",2,5,"ft","fT",0,0,""], ["p3",5,6,"ft","fT",0,0,""], ["p4",6,7,"ft","fT",0,0,""], ["p5",7,1,"ft","fT",0,0,""], ["p6",3,4,"ft","fT",0,0,""], ["p7",4,8,"ft","fT",0,0,""], ["p8",8,9,"ft","fT",0,0,""], ["p9",9,10,"ft","fT",0,0,""],["p10",10,3,"ft","fT",0,0,""], ["p11",9,6,"h","h",0,0,""], ["p12",5,8,"h","h",0,0,""], ["p13",7,10,"g","g",0,0,""]]
        d1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 3,  "g", 0], ["q4", 4, "g", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q2", "h", "2,2"], ["true", "q1,q2", "ft,huc", "0,0"], ["true", "q1,q2", "g,c,fq", "1,99"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [d1])
        
    def test_cutFiltertchannel1(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 4-point function
        d1 = Diagram()
        d1.internalMomenta = [["p1",1,5,"ft","fT",0,0,""], ["p2",5,6,"ft","fT",0,0,""], ["p3",6,3,"ft","fT",0,0,""], ["p4",3,1,"ft","fT",0,0,""], ["p5",2,4,"ft","fT",0,0,""], ["p6",4,8,"ft","fT",0,0,""], ["p7",8,7,"ft","fT",0,0,""], ["p8",7,2,"ft","fT",0,0,""], ["p9",5,7,"g","g",0,0,""], ["p10",6,8,"g","g",0,0,""]]
        d1.externalMomenta = [["q1", 1, "g", 0], ["q2", 3, "g", 0], ["q3", 2,  "h", 0], ["q4", 4, "h", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q3", "g", "2,2"], ["true", "q1,q3", "ft", "0,0"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])
        
    def test_cutFiltertchannel2(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 4-point function
        d1 = Diagram()
        d1.internalMomenta = [["p1",1,5,"ft","fT",0,0,""], ["p2",5,6,"ft","fT",0,0,""], ["p3",6,3,"ft","fT",0,0,""], ["p4",3,1,"ft","fT",0,0,""], ["p5",2,4,"ft","fT",0,0,""], ["p6",4,8,"ft","fT",0,0,""], ["p7",8,7,"ft","fT",0,0,""], ["p8",7,2,"ft","fT",0,0,""], ["p9",5,7,"g","g",0,0,""], ["p10",6,8,"g","g",0,0,""]]
        d1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 3,  "h", 0], ["q4", 4, "h", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q3", "g", "2,2"], ["true", "q1,q3", "ft", "0,0"] ]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [d1])
        
    def test_cutFiltertchannel3(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)

        # 4-point function
        d1 = Diagram()
        d1.internalMomenta = [["p1",1,5,"ft","fT",0,0,""], ["p2",5,6,"ft","fT",0,0,""], ["p3",6,3,"ft","fT",0,0,""], ["p4",3,1,"ft","fT",0,0,""], ["p5",2,4,"ft","fT",0,0,""], ["p6",4,8,"ft","fT",0,0,""], ["p7",8,7,"ft","fT",0,0,""], ["p8",7,2,"ft","fT",0,0,""], ["p9",5,7,"g","g",0,0,""], ["p10",6,8,"g","g",0,0,""]]
        d1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 3,  "h", 0], ["q4", 4, "h", 0]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1,q3", "g", "2,2"], ["true", "q1,q3", "ft", "0,0"],[ "true", "q1,q2", "ft", "4,4"]]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [d1])
        
    def test_cutFilterIsolatedVertex(self):
        logging.basicConfig(format='%(levelname)s:%(module)s: %(message)s', level=logging.DEBUG)   
        
        # 2-point function
        d1 = Diagram()
        d1.internalMomenta = [['p1', 3, 1, 'fQ', 'fq', 5, 6, ''], ['p2', 1, 4, 'fQ', 'fq', 7, 8, ''], ['p3', 5, 2, 'fQ', 'fq', 9, 10, ''], ['p4', 2, 6, 'fQ', 'fq', 11, 12, ''], ['p5', 4, 3, 'fQ', 'fq', 13, 14, ''], ['p6', 5, 3, 'g', 'g', 15, 16, ''], ['p7', 7, 4, 'g', 'g', 17, 18, ''], ['p8', 6, 5, 'fQ', 'fq', 19, 20, ''], ['p9', 8, 6, 'g', 'g', 21, 22, ''], ['p10', 8, 7, 'fB', 'fb', 23, 24, 'M1'], ['p11', 7, 8, 'fB', 'fb', 25, 26, 'M1']]
        d1.externalMomenta = [["q1", 1, "a", 1], ["q2", 2, "a", 2]]
        d1.diagramNumber = 1
        d1.diagramID = d1.diagramNumber
        d1.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)

        conf.filters = [  [ "cuts",[ "true", "q1", "fq", "2,2"], ["true", "q1", "fb", "2,2"], ["true", "q1", "g", "1,1" ]]  ]
        self.assertListEqual(DiagramFilter(conf).filter([d1]), [])
        
    def test_ScalelessIntFilter(self):
        # Example 23 from Jens Hoff's PhD Thesis with a 2-loop tadpole
        prop1 = Diagram()
        prop1.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "g", "g", 0, 0, ""],["p5", 3, 4, "g", "g", 0, 0, ""],["p6", 4, 2, "g", "g", 0, 0, ""]]
        prop1.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop1.diagramNumber = 1
        prop1.diagramID = 1
        prop1.name = "prop1"
        prop1.createTopologicalVertexAndLineLists()
        
        prop2 = Diagram()
        prop2.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 1, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "g", "g", 0, 0, ""],["p5", 4, 3, "g", "g", 0, 0, ""],["p6", 4, 2, "g", "g", 0, 0, ""]]
        prop2.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop2.diagramNumber = 2
        prop2.diagramID = 2
        prop2.name = "prop2"
        prop2.createTopologicalVertexAndLineLists()
        
        prop3 = Diagram()
        prop3.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "fT1", "ft1", 0, 0, "M1"],["p5", 3, 4, "ft1", "fT1", 0, 0, "M1"],["p6", 4, 2, "g", "g", 0, 0, ""]]
        prop3.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0]]
        prop3.diagramNumber = 3
        prop3.diagramID = 3
        prop3.name = "prop3"
        prop3.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.filters = [["scaleless", "true"]]
        dialist = [prop1, prop2, prop3]

        filteredDias = DiagramFilter(conf).filter(dialist)
        self.assertEqual(len(filteredDias), 1)
        self.assertEqual(filteredDias[0].diagramID, 1)
        
        conf.filters = [["scaleless", "false"]]

        filteredDias2 = DiagramFilter(conf).filter(dialist)
        self.assertEqual(len(filteredDias2), 2)
        self.assertEqual(filteredDias2[0].diagramID, 2)
        self.assertEqual(filteredDias2[1].diagramID, 3)
        
    def test_ScalelessLegFilter(self):
        ggh1 = Diagram()
        ggh1.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "g", "g", 0, 0, ""],["p5", 4, 5, "g", "g", 0, 0, ""],["p6", 5, 3, "g", "g", 0, 0, ""]]
        ggh1.externalMomenta = [["q1", 1, "g", 0], ["q2", 4, "g", 0], ["q3", 5, "h", 0]]
        ggh1.diagramNumber = 1
        ggh1.diagramID = 1
        ggh1.name = "ggh1"
        ggh1.createTopologicalVertexAndLineLists()

        ggh2 = Diagram()
        ggh2.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 3, "g", "g", 0, 0, ""],["p3", 3, 4, "g", "g", 0, 0, ""],["p4", 4, 1, "g", "g", 0, 0, ""],["p5", 4, 2, "g", "g", 0, 0, ""],["p6", 3, 5, "g", "g", 0, 0, ""]]
        ggh2.externalMomenta = [["q1", 1, "g", 0], ["q2", 5, "g", 0], ["q3", 5, "h", 0]]
        ggh2.diagramNumber = 2
        ggh2.diagramID = 2
        ggh2.name = "ggh2"
        ggh2.createTopologicalVertexAndLineLists()
        
        ggh3 = Diagram()
        ggh3.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 3, "g", "g", 0, 0, ""],["p3", 3, 4, "g", "g", 0, 0, ""],["p4", 4, 5, "g", "g", 0, 0, ""],["p5", 5, 1, "g", "g", 0, 0, ""],["p6", 3, 5, "g", "g", 0, 0, ""]]
        ggh3.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 4, "h", 0]]
        ggh3.diagramNumber = 3
        ggh3.diagramID = 3
        ggh3.name = "ggh3"
        ggh3.createTopologicalVertexAndLineLists()
        
        ggh4 = Diagram()
        ggh4.internalMomenta = [["p1", 1, 2, "fT1", "ft1", 0, 0, "M1"],["p2", 2, 1, "fT1", "ft1", 0, 0, "M1"],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 4, "g", "g", 0, 0, ""],["p5", 4, 5, "g", "g", 0, 0, ""],["p6", 5, 3, "g", "g", 0, 0, ""]]
        ggh4.externalMomenta = [["q1", 1, "g", 0], ["q2", 4, "g", 0], ["q3", 5, "h", 0]]
        ggh4.diagramNumber = 4
        ggh4.diagramID = 4
        ggh4.name = "ggh4"
        ggh4.createTopologicalVertexAndLineLists()
        
        ggh5 = Diagram()
        ggh5.internalMomenta = [["p1", 1, 2, "g", "g", 0, 0, ""],["p2", 2, 1, "g", "g", 0, 0, ""],["p3", 2, 3, "g", "g", 0, 0, ""],["p4", 3, 2, "g", "g", 0, 0, ""]]
        ggh5.externalMomenta = [["q1", 1, "g", 0], ["q2", 1, "g", 0], ["q3", 3, "h", 0]]
        ggh5.diagramNumber = 5
        ggh5.diagramID = 5
        ggh5.name = "ggh5"
        ggh5.createTopologicalVertexAndLineLists()
        
        ggh6 = Diagram()
        ggh6.internalMomenta = [["p1", 1, 2, "fT1", "ft1", 0, 0, "M1"],["p2", 2, 3, "fT1", "ft1", 0, 0, "M1"],["p3", 3, 4, "fT1", "ft1", 0, 0, "M1"],["p4", 4, 5, "fT1", "ft1", 0, 0, "M1"],["p5", 5, 1, "fT1", "ft1", 0, 0, "M1"],["p6", 3, 5, "g", "g", 0, 0, ""]]
        ggh6.externalMomenta = [["q1", 1, "g", 0], ["q2", 2, "g", 0], ["q3", 4, "h", 0]]
        ggh6.diagramNumber = 6
        ggh6.diagramID = 6
        ggh6.name = "ggh6"
        ggh6.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf", False)
        conf.kinematicRelations["q1.q1"] = "0"
        conf.kinematicRelations["q2.q2"] = "0"
        conf.kinematicRelations["q1.q2"] = "s/2"
        dialist = [ggh1,ggh2,ggh3,ggh4,ggh5,ggh6]

        conf.filters = [["scaleless", "false"]]
        filteredDias = DiagramFilter(conf).filter(dialist)
        self.assertEqual(len(filteredDias), 4)
        
        conf.filters = [["scaleless", "true"]]
        filteredDias2 = DiagramFilter(conf).filter(dialist)
        self.assertEqual(len(filteredDias2), 2)
        
        conf.filters = [["scaleless", "false"]]
        conf.externalMomenta["q2"] = "q1"
        filteredDias3 = DiagramFilter(conf).filter(dialist)
        self.assertEqual(len(filteredDias3), 1)

    def test_vertexFilterDia8(self):
        diagrams = self.initializeDiagrams()
        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["vertex", ["true", "", "fq,fq,fQ,fq", "1,1"]], ["vertex", ["true", "", "sigma,fQ,fT1", "1,1"]], ["vertex", ["true", "", "g,ft1,fT1", "2,2"]]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [8])

    def test_vertexFilterDia11(self):
        diagrams = self.initializeDiagrams()
        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["vertex", ["false", "", "sigma,ft2,fT2", "0,1"]]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [11])

    def test_vertexAndCutFilterDia7(self):
        diagrams = self.initializeDiagrams()
        conf = Conf("test/qqqq.conf", False)
        conf.propFile = "test/qcd.prop"
        conf.filters = [["vertex", ["true", "", "sigma,ft2,fT2", "1,1"]], ["cuts", ["true", "q1,q2,q3", "fq,g", "2,2"]]]
        diagramFilter = DiagramFilter(conf)
        filteredDiagrams = diagramFilter.filter(diagrams)
        self.assertListEqual([d.diagramNumber for d in filteredDiagrams], [1])
        self.assertListEqual([d.diagramID for d in filteredDiagrams], [7])
