from modules.topoFileGenerator import TopoFileGenerator
from modules.qgrafReader import QgrafReader
from modules.diagram import Diagram
from modules.config import Conf
from modules.momentumAssigner import MomentumAssigner
from modules.partialFractionDecomposer import PartialFractionDecomposer
from modules.mathematicaTopoListGenerator import MathematicaTopoListGenerator
from modules.externalLineFilter import ExternalLineFilter
from modules.integralFamily import IntegralFamily
from sympy import Matrix
import sympy as sy
import unittest
import re
import os
import logging


class testTopoFileGenerator(unittest.TestCase):

    def test_topoStringWithoutExternalMomentumRelations(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,False)

        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "fT1", "ft1", 2,3, "M1"], ["p3", 3,1, "g", "g", 3,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "* Using Minkowski momenta"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3 = p2-q1-q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p2-q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = p1.p1/2-p3.p3/2+q1.q1/2+q1.q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q2 = -p1.p1/2+p2.p2/2+q2.q2/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = -1/s2m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m1^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p3.p3^n4? = (-1)^n1 * (-1)^n3 * (-1)^n4 * d1l0(n0,n1,n2,n3,n4);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoStringWithoutExternalMomentumRelationsExtraSort(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.topoExtraSort = True
        self.assertEqual(config.euclideanMomenta,False)
        self.assertEqual(config.topoExtraSort,True)


        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "fT1", "ft1", 2,3, "M1"], ["p3", 3,1, "g", "g", 3,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p3 = p2-q1-q2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p2-q2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = p1.p1/2-p3.p3/2+q1.q1/2+q1.q2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q2 = -p1.p1/2+p2.p2/2+q2.q2/2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = -1/s2m1+M1^2; .sort"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m1^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p3.p3^n4? = (-1)^n1 * (-1)^n3 * (-1)^n4 * d1l0(n0,n1,n2,n3,n4); .sort"), topoString, "This may differ if the scalar integral composition has changed")
        
    def test_topoStringWithoutExternalMomentumRelationsExtraACCU(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.topoExtraSort = True
        config.topoSortFunc = "ACCU()"
        self.assertEqual(config.euclideanMomenta,False)
        self.assertEqual(config.topoExtraSort,True)
        self.assertEqual(config.topoSortFunc,"ACCU()")


        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "fT1", "ft1", 2,3, "M1"], ["p3", 3,1, "g", "g", 3,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p3 = p2-q1-q2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p2-q2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = p1.p1/2-p3.p3/2+q1.q1/2+q1.q2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q2 = -p1.p1/2+p2.p2/2+q2.q2/2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = -1/s2m1+M1^2; #call ACCU()"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m1^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p3.p3^n4? = (-1)^n1 * (-1)^n3 * (-1)^n4 * d1l0(n0,n1,n2,n3,n4); #call ACCU()"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoStringWithoutExternalMomentumRelationsEuclidean(self):
        config = Conf("test/qqqqeuclidean.conf",False)

        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,True)

        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "fT1", "ft1", 2,3, "M1"], ["p3", 3,1, "g", "g", 3,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "d1l0:{M1^2+p2^2"), topoString)
        self.assertIn(re.sub(r'\s', r'', "*Using Euclidean momenta"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3 = p2-q1-q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p2-q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = p1.p1/2-p3.p3/2+q1.q1/2+q1.q2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q2 = -p1.p1/2+p2.p2/2+q2.q2/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = 1/s1m1-M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = 1/s2m1-M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m1^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p3.p3^n4? = d1l0(n0,n1,n2,n3,n4);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoStringWithExternalMomentumRelations(self):
        config = Conf("test/qqqq.conf",False)

        config.externalMomenta = {"q2":"q", "q3":"q"}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,False)

        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "g", "g", 2,3, ""], ["p3", 3,1, "fT1", "ft1", 3,1, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "* Using Minkowski momenta"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id q2 = q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id q3 = q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2 = p3-q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p3-2*q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.q = -p1.p1/4+p3.p3/4+q.q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = p1.p1/2+p3.p3/2-q.q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = -1/s3m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s3m1^n0? * 1/p3.p3^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p2.p2^n4? = (-1)^n1 * (-1)^n3 * (-1)^n4 * d1l0(n0,n1,n2,n3,n4);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoStringWithExternalMomentumRelationsEuclidean(self):
        config = Conf("test/qqqqeuclidean.conf",False)

        config.externalMomenta = {"q2":"q", "q3":"q"}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,True)

        # simple massive three-point diagram with one loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "fT1", 2], ["q3", 3, "fT1", 3]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 2,3, "g", "g", 2,3, ""], ["p3", 3,1, "fT1", "ft1", 3,1, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()
        
        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "* Using Euclidean momenta"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id q2 = q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id q3 = q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2 = p3-q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = p3-2*q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.q = -p1.p1/4+p3.p3/4+q.q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = p1.p1/2+p3.p3/2-q.q;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = 1/s1m1-M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = 1/s3m1-M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s3m1^n0? * 1/p3.p3^n1? * s1m1^n2? * 1/p1.p1^n3? * 1/p2.p2^n4? = d1l0(n0,n1,n2,n3,n4);"), topoString, "This may differ if the scalar integral composition has changed")
        

    def test_topoFileGeneratorOnMasslessSelfLoopDiagram(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,False)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 1, "g", 1]]
        diagram.internalMomenta = [["p1", 1,1, "g", "g", 1,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)
        self.assertTrue(re.sub(r' ', r'', "id 1/p1.p1^n0? * 1/p1.q1^n1? = (-1)^n0 * (-1)^n1 * d2l42(n0, n1);") in topoString)


    def test_topoFileGeneratorOnMasslessSelfLoopDiagramEuclidean(self):
        config = Conf("test/qqqqeuclidean.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,True)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 1, "g", 1]]
        diagram.internalMomenta = [["p1", 1,1, "g", "g", 1,1, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)
        self.assertTrue(re.sub(r' ', r'', "id 1/p1.p1^n0? * 1/p1.q1^n1? = d2l42(n0, n1);") in topoString)


    def test_topoFileGeneratorOnMassiveSelfLoopDiagram(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,False)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 1, "g", 1]]
        diagram.internalMomenta = [["p1", 1,1, "fT1", "ft1", 1,1, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)
        self.assertIn(re.sub(r' ', r'', "id s1m1^n0? * 1/p1.p1^n1? * 1/p1.q1^n2? = (-1)^n1 * (-1)^n2 * d2l42(n0, n1, n2);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorOnMassiveSelfLoopDiagramEuclidean(self):
        config = Conf("test/qqqqeuclidean.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        self.assertEqual(config.euclideanMomenta,True)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 1, "g", 1]]
        diagram.internalMomenta = [["p1", 1,1, "fT1", "ft1", 1,1, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)
        self.assertIn(re.sub(r' ', r'', "id s1m1^n0? * 1/p1.p1^n1? * 1/p1.q1^n2? = d2l42(n0, n1, n2);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorOnTwoMassProblem(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.mass = {"ft1" : "M1", "g" : "M2"}
        self.assertEqual(config.euclideanMomenta,False)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 1,2, "g", "g", 1,2, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p1 = -p2+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = -p1.p1/2+p2.p2/2+q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = -1/s2m2+M2^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m2^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? = (-1)^n1*(-1)^n3*d1l42(n0,n1,n2,n3);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorOnTwoMassProblemEuclidean(self):
        config = Conf("test/qqqqeuclidean.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.mass = {"ft1" : "M1", "g" : "M2"}
        self.assertEqual(config.euclideanMomenta,True)

        # simple two-point diagram with one self-loop
        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 1,2, "g", "g", 1,2, ""]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p1 = -p2+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.q1 = -p1.p1/2+p2.p2/2+q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = 1/s1m1-M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2 = 1/s2m2-M2^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s2m2^n0? * 1/p2.p2^n1? * s1m1^n2? * 1/p1.p1^n3? = d1l42(n0,n1,n2,n3);"), topoString)


    def test_topoFileGeneratorOnStrangeDiagramMultipleMassProblem(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.mass = {"ft1" : "M1", "w1" : "M2", "w2" : "M4"}
        self.assertEqual(config.euclideanMomenta,False)

        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 1,3, "g", "g", 1,3, ""], ["p3", 3,2, "w1", "w1", 3,2, "M2"], ["p4", 2, 1, "w2", "w2", 2, 1, "M4"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p2 = p3;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = -p3+p4+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2^n0? = (p3.p3)^n0;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.q1 = p1.p1/2-p3.p3/2+p3.p4+p3.q1-p4.p4/2-q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = -1/s3m2+M2^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.p4 = -1/s4m4+M4^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s4m4^n0? * 1/p4.p4^n1? * s3m2^n2? * 1/p3.p3^n3? * s1m1^n4? * 1/p1.p1^n5? * 1/p3.p4^n6? * 1/p3.q1^n7? = (-1)^n1 * (-1)^n3 * (-1)^n5 * (-1)^n6 * (-1)^n7 * d1l42(n0,n1,n2,n3,n4,n5,n6,n7);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorWithoutExtendAnalyticalTopos(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = False
        config.mass = {"ft1" : "M1", "w1" : "M2", "w2" : "M4"}
        self.assertEqual(config.euclideanMomenta,False)

        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 1,3, "g", "g", 1,3, ""], ["p3", 3,2, "w1", "w1", 3,2, "M2"], ["p4", 2, 1, "w2", "w2", 2, 1, "M4"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p2 = p3;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = -p3+p4+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2^n0? = (p3.p3)^n0;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.q1 = p1.p1/2-p3.p3/2+p3.p4+p3.q1-p4.p4/2-q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = -1/s3m2+M2^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.p4 = -1/s4m4+M4^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s4m4^n0? * s3m2^n1? * 1/p3.p3^n2? * s1m1^n3? * 1/p3.p4^n4? * 1/p3.q1^n5? = (-1)^n2 * (-1)^n4 * (-1)^n5 * d1l42(n0,n1,n2,n3,n4,n5);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorCompleteMomentumProducts(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.topoCompleteMomentumProducts = True
        config.mass = {"ft1" : "M1", "w1" : "M2", "w2" : "M4"}
        self.assertEqual(config.euclideanMomenta,False)

        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "g", 1], ["q2", 2, "g", 2]]
        diagram.internalMomenta = [["p1", 1,2, "fT1", "ft1", 1,2, "M1"], ["p2", 1,3, "g", "g", 1,3, ""], ["p3", 3,2, "w1", "w1", 3,2, "M2"], ["p4", 2, 1, "w2", "w2", 2, 1, "M4"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)

        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\n', r'', topoString)
        topoString = re.sub(r' ', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p2 = p3;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1 = -p3+p4+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p2.p2^n0? = (p3.p3)^n0;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.q1 = p1.p1/2-p3.p3/2+p3.p4+p3.q1-p4.p4/2-q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p4 = (P1.P1 - p3.p3 - p4.p4)/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.q1 = (P2.P2 - p3.p3 - q1.q1)/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = -1/s3m2+M2^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p4.p4 = -1/s4m4+M4^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id s4m4^n0? * s3m2^n1? * 1/p3.p3^n2? * s1m1^n3? * 1/P1.P1^n4? * 1/P2.P2^n5? = (-1)^n2 * (-1)^n4 * (-1)^n5 * d1l42(n0,n1,n2,n3,n4,n5);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_topoFileGeneratorOnExternalBridgeDiagram(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.mass = {"ft1" : "M1"}
        self.assertEqual(config.euclideanMomenta,False)

        diagram = Diagram()
        diagram.numLoops = 1
        diagram.diagramNumber = 42
        diagram.externalMomenta = [["q1", 1, "ft1", 1], ["q2", 2, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,3, "fT1", "ft1", 1,3, "M1"], ["p2", 3,1, "g", "g", 3,1, ""], ["p3", 3,2, "fT1", "ft1", 3,2, "M1"]]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "id p2 = p1 - q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3 = q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.q1 = p1.p1/2 - p2.p2/2 + q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = q1.q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p1.p1 = -1/s1m1 + M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "id p3.p3 = -1/s3m1 + M1^2;"), topoString)

        comparisonString = "id s1m1^n0? * 1/p1.p1^n1? * s3m1^n2? * 1/p3.p3^n3? * 1/p2.p2^n4? = (-1)^n1 * (-1)^n3 * (-1)^n4 * d1l42(n0,n1,n2,n3,n4);"
        comparisonString = re.sub(r'\s', r'', comparisonString)
        self.assertIn(comparisonString, topoString, "This may differ if the scalar integral composition has changed")



    def test_topoFileGeneratorOnTwoLoopTwoMassProblem(self):
        config = Conf("test/qqqq.conf",False)
        config.externalMomenta = {}
        config.extendAnalyticalTopos = True
        config.mass = {"fb" : "M1", "g" : "M3"}
        self.assertEqual(config.euclideanMomenta,False)

        diagram = Diagram()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.externalMomenta = [['q1', 1, 'fb', 1], ['q4', 3, 'fb', 4]]
        diagram.internalMomenta = [['p1', 4, 1, 'W1F', 'w1F', 9, 10, ''],
                ['p2', 3, 2, 'g', 'g', 11, 12, 'M3'],
                ['p3', 5, 2, 'fS', 'fs', 13, 14, ''],
                ['p4', 3, 6, 'fB', 'fb', 15, 16, 'M1'],
                ['p5', 4, 5, 'fC', 'fc', 17, 18, ''],
                ['p6', 6, 4, 'fC', 'fc', 19, 20, ''],
                ['p7', 6, 5, 'W1', 'w1', 21, 22, '']]
        diagram.setName()
        diagram.createTopologicalVertexAndLineLists()

        topoFileGenerator = TopoFileGenerator(config)

        mA = MomentumAssigner(config, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations() 

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        topoString = re.sub(r'\s', r'', topoString)

        self.assertIn(re.sub(r'\s', r'', "idp7=p4-p5+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp6=p5-q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp3=p4+q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp1=-q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp2=-p4-q1;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp3.p3^n0?=(p2.p2)^n0;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp1.p1^n0?=(q1.q1)^n0;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp4.q1=p2.p2/2-p4.p4/2-q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp5.q1=p5.p5/2-p6.p6/2+q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp4.p5=p2.p2/2+p6.p6/2-p7.p7/2-q1.q1/2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp2.p2=-1/s2m3+M3^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "idp4.p4=-1/s4m1+M1^2;"), topoString)
        self.assertIn(re.sub(r'\s', r'', "ids4m1^n0?*1/p4.p4^n1?*1/p5.p5^n2?*s2m3^n3?*1/p2.p2^n4?*1/p6.p6^n5?*1/p7.p7^n6?=(-1)^n1*(-1)^n2*(-1)^n4*(-1)^n5*(-1)^n6*d2l42(n0,n1,n2,n3,n4,n5,n6);"), topoString, "This may differ if the scalar integral composition has changed")


    def test_eikonalTopoFile(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 4, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,3, "f2", "f2", 1,3, "M2"], ["p3", 2,3, "f3", "f3", 2,3, "M3"], ["p4", 2,5, "g", "g", 2,5, ""], ["p5", 3,4, "g", "g", 3,4, ""], ["p6", 4,5, "f1", "f1", 4,5, "M1"]]
        diagram.createTopologicalVertexAndLineLists()
        diagram.numLoops = 2
        diagram.diagramNumber = 42
        diagram.setName()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1", "f2": "M2", "f3": "M3"}

        conf.topoEikonalExternal = [True]
        conf.topoEikonalInternal = True

        topoFileGenerator = TopoFileGenerator(conf)

        mA = MomentumAssigner(conf, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations()

        topoString = topoFileGenerator.getTopoString(mA)
        topoString = re.sub(r'\.sort', r'', topoString)
        # print(topoString)

        self.assertIn("multiply replace_(p4, -p6);", topoString)
        self.assertIn("1/p3.p6^n", topoString)
        self.assertIn("1/p3.q1^n", topoString)
        self.assertIn("1/p6.q1^n", topoString)
        self.assertIn("1/p1.p2^n", topoString)
        self.assertIn("1/p1.p3^n", topoString)
        self.assertIn("1/p1.p5^n", topoString)
        self.assertIn("1/p1.p6^n", topoString)
        self.assertIn("1/p2.p3^n", topoString)
        self.assertIn("1/p2.p5^n", topoString)
        self.assertIn("1/p2.p6^n", topoString)
        self.assertIn("1/p3.p5^n", topoString)
        self.assertIn("1/p5.p6^n", topoString)
        self.assertIn("1/p1.q1^n", topoString)
        self.assertIn("1/p2.q1^n", topoString)
        self.assertIn("1/p5.q1^n", topoString)

    
    def test_kinematicRelationsInTopoFile(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,2, "f1", "f1", 1,2, "M1"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1"}
        conf.kinematicRelations = {"q1.q1": "-M1^2"}

        topoFileGenerator = TopoFileGenerator(conf)

        mA = MomentumAssigner(conf, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations()

        topoString = topoFileGenerator.getTopoString(mA)

        self.assertIn("id q1.q1^n0? = (-M1^2)^n0;", topoString)


    def test_kinematicRelationsVanishingMomentumSquaredInTopoFile(self):
        diagram = Diagram()
        diagram.externalMomenta = [["q1", 1, "fT1", 1], ["q2", 2, "fT1", 2]]
        diagram.internalMomenta = [["p1", 1,2, "f1", "f1", 1,2, "M1"], ["p2", 1,2, "f1", "f1", 1,2, "M1"]]
        diagram.createTopologicalVertexAndLineLists()

        conf = Conf("test/qqqq.conf",False)
        conf.mass = {"f1": "M1"}
        conf.kinematicRelations = {"q1.q1": "0"}

        topoFileGenerator = TopoFileGenerator(conf)

        mA = MomentumAssigner(conf, diagram)
        # extract massive lines
        mA.generateLineMassesDict()
        # generate dictionary of momenta
        mA.calculateMomentumRelations()

        topoString = topoFileGenerator.getTopoString(mA)

        self.assertIn("id q1.q1 = 0;", topoString)


    def test_writeIntegralFamilies(self):
        testFileName = "test/test_MathematicaWriteIntegralFamilies.m"
        config = Conf("test/qqqq.conf",False)
        mmaTLG = MathematicaTopoListGenerator(config)
        fam1 = IntegralFamily("Int1", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"])
        fam2 = IntegralFamily("Int2", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"])
        fam3 = IntegralFamily("Int3", ['-k1*k2', '-k1^2 - 2*k1*q1 - q1^2', 'M2^2 - k2^2 + 2*k2*q1 - q1^2', '-k1^2', '-k2^2'], ["k1", "k2"])
        
        mmaTLG.writeIntegralFamilies(testFileName, [fam1, fam2, fam3])

        listFile = open(testFileName, "r")
        listFileString = "".join(listFile.readlines())
        listFile.close()
        os.remove(testFileName)

        self.assertIn('{"Int1", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)
        self.assertIn('{"Int2", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)
        self.assertIn('{"Int3", {-k1*k2, -k1^2 - 2*k1*q1 - q1^2, M2^2 - k2^2 + 2*k2*q1 - q1^2, -k1^2, -k2^2}, {k1, k2}}', listFileString)
